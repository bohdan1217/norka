<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Заказать тренинг по продажам!"); 
$APPLICATION->SetTitle("Заказать тренинг по продажам!"); 
?>



<div class="content">
	
<noindex>
<?$APPLICATION->IncludeComponent(
	"ritg:inv.feedback", 
	"custom", 
	array(
		"IBLOCK_TYPE" => "feedback",
		"IBLOCK_ID" => "17",
		"STATUS_NEW" => "Y",
		"LIST_URL" => "",
		"ADD_ELEMENTS" => "Y",
		"SEND_EMAILS" => "Y",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_ADD" => GetMessage("CALL_REQUEST_ADD"),
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "Y",
		"PROPERTY_CODES" => array(
			0 => "65",
			1 => "66",
			2 => "67",
			3 => "68",
			4 => "69",
			5 => "70",
			6 => "71",
			7 => "72",
			8 => "73",
			9 => "74",
			10 => "75",
			11 => "NAME",
			12 => "DETAIL_TEXT",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "65",
			1 => "66",
			2 => "67",
			3 => "68",
			4 => "69",
			5 => "70",
			6 => "NAME",
		),
		"CUSTOM_LABEL" => "",
		"CLASS_FOR_PROPERTY" => "",
		"EVENT_NAME" => "REQUEST_TRAINING",
		"EVENT_MESSAGE_ID" => "9",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"CUSTOM_TITLE_NAME" => "Название вашей компании",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "Пожелания, комментарии, вопросы",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"COMPONENT_TEMPLATE" => "custom",
		"EMAIL_TO" => "mail@norca.ru"
	),
	false
);?>

</noindex>
</div>






<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
