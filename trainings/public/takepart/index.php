<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Записаться на тренинг по продажам!");
$APPLICATION->SetTitle("Записаться на тренинг по продажам!");
?>

 

<div class="content">


<noindex>
<?$APPLICATION->IncludeComponent(
	"ritg:inv.feedback", 
	"custom", 
	array(
		"IBLOCK_TYPE" => "feedback",
		"IBLOCK_ID" => "21",
		"STATUS_NEW" => "Y",
		"LIST_URL" => "",
		"ADD_ELEMENTS" => "Y",
		"SEND_EMAILS" => "Y",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_ADD" => "Ваша заявка принята",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "Y",
		"PROPERTY_CODES" => array(
			0 => "89",
			1 => "90",
			2 => "91",
			3 => "92",
			4 => "93",
			5 => "94",
			6 => "95",
			7 => "NAME",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "91",
			1 => "92",
			2 => "93",
		),
		"CUSTOM_LABEL" => "",
		"CLASS_FOR_PROPERTY" => "",
		"EVENT_NAME" => "TRAINING_PARTICIPATION_REQUEST",
		"EVENT_MESSAGE_ID" => "8",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"CUSTOM_TITLE_NAME" => "Название вашей компании",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "Пожелания, комментарии, вопросы",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"COMPONENT_TEMPLATE" => "custom",
		"EMAIL_TO" => "mail@norca.ru"
	),
	false
);?>
</noindex>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>