<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

 

<div class="content">
	
<noindex>
<?$APPLICATION->IncludeComponent("ritg:inv.feedback", "custom", array(
					    "IBLOCK_TYPE" => "feedback",
					    "IBLOCK_ID" => "18",
					    "STATUS_NEW" => "Y",
					    "LIST_URL" => "",
					    "ADD_ELEMENTS" => "Y",
					    "SEND_EMAILS" => "Y", 

					    "USE_CAPTCHA" => "Y",

					    "USER_MESSAGE_ADD" => GetMessage('CALL_REQUEST_ADD'),
					    "DEFAULT_INPUT_SIZE" => "30",
					    "RESIZE_IMAGES" => "Y",

					    "PROPERTY_CODES" => array(
					       "NAME", 
					       "118",
					       "119",
					       "76",
					       "77",
					       "78",
					       "79",
					       "80",
					       "81",
					       "82",
					       "83",
					       "84",

					       "PREVIEW_TEXT",
					       "DETAIL_TEXT",
					       "86",
					    ),
					    "PROPERTY_CODES_REQUIRED" => array(
				       	   "NAME", 
					       "118",
					       "119",
					       "76",
					       "77",
					       "78",
					       "79",
					       "80",
					       "81", 
						   
					       "PREVIEW_TEXT",
					    ),

						"CUSTOM_LABEL" => array(
					     		 

					     ),


					    "CLASS_FOR_PROPERTY" => array(
					    	 
					    ),

					    "EVENT_NAME" => "REQUEST_COACHING",

					    "EVENT_MESSAGE_ID" => "11",
/*					    "EMAIL_TO" => "sale@casf.com",
*/
					    "GROUPS" => array(
					        0 => "2",
					    ),
					    "MAX_FILE_SIZE" => "0",
					    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
					    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
					    "CUSTOM_TITLE_NAME" => "Название вашей компании",
					    "CUSTOM_TITLE_TAGS" => "",
					    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
					    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
					    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
					    "CUSTOM_TITLE_PREVIEW_TEXT" => "Опишите, пожалуйста, какую задачу хотите решить с помощью коучинга",
					    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
					    "CUSTOM_TITLE_DETAIL_TEXT" => "Пожелания, комментарии, вопросы",
					    "CUSTOM_TITLE_DETAIL_PICTURE" => ""
					),
					    false
					);?>
</noindex>

</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>