<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты бизнес-тренера Дмитрия Норка | Norca"); 
$APPLICATION->SetTitle("Контакты бизнес-тренера Дмитрия Норка | Norca");
?>

<div class="content">
 

            <div class="contacts">
            <div class="contacts_inf">
                <p><b>Телефон:</b> <?$APPLICATION->IncludeFile(SITE_DIR.'include/telephone.php', array(), array());?></p>
                <p><b>Электронная почта:</b> <a class="reset_style" href="mailto:<?$APPLICATION->IncludeFile(SITE_DIR.'include/email.php', array(), array());?>"><?$APPLICATION->IncludeFile(SITE_DIR.'include/email.php', array(), array());?></a></p>
 

             <?$APPLICATION->IncludeComponent("bitrix:news.list","social",Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "press_center",
                "IBLOCK_ID" => "23",
                "NEWS_COUNT" => "20",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => Array("ID"),
                "PROPERTY_CODE" => Array("DESCRIPTION"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "Y",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "Y",
                "SET_META_DESCRIPTION" => "Y",
                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            )
    );?>
            </div> 
                    <?$APPLICATION->IncludeComponent(
	"ritg:inv.feedback", 
	"contacts", 
	array(
		"IBLOCK_TYPE" => "feedback",
		"IBLOCK_ID" => "22",
		"STATUS_NEW" => "Y",
		"LIST_URL" => "",
		"ADD_ELEMENTS" => "Y",
		"SEND_EMAILS" => "Y",
		"USE_CAPTCHA" => "Y",
		"USER_MESSAGE_ADD" => GetMessage("CALL_REQUEST_ADD"),
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "Y",
		"PROPERTY_CODES" => array(
			0 => "96",
			1 => "NAME",
			2 => "DETAIL_TEXT",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "96",
			1 => "NAME",
			2 => "DETAIL_TEXT",
		),
		"EVENT_NAME" => "SEND_QUESTION",
		"EVENT_MESSAGE_ID" => "12",
		"GROUPS" => array(
			0 => "2",
		),
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"CUSTOM_TITLE_NAME" => "Email",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "Вопрос",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"COMPONENT_TEMPLATE" => "contacts",
		"EMAIL_TO" => "mail@norca.ru"
	),
	false
);?> 


        </div>
 


 
    
 

    
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
