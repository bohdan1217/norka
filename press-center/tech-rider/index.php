<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Технический райдер по продажам!"); 
$APPLICATION->SetTitle("Технический райдер по продажам!");
?>

<section class="section-welcome-press-center">
    <div class="container">
        <h3 class="section-welcome-title-press">Технический райдер</h3>
    </div>
</section>



<section class="section-content-newss section-bg-grey">
    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
            "START_FROM" => "0",  // Номер пункта, начиная с которого будет построена навигационная цепочка
            "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        ),
            false
        );?>


        <?$APPLICATION->IncludeFile(SITE_DIR.'include/tech_rider.php', array(), array());?>


    </div>
</section>


 


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
