<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Пресс-центр");
$APPLICATION->SetTitle("Пресс-центр");
?>

<section class="section-welcome-press-center">
 <div class="container">
  <h3 class="section-welcome-title-press">Пресс-центр</h3>
  <p class="section-welcome-text-press">Сегодня, главная задача лучших продавцов заключается в  том, чтобы добиться доверия и взаимодействие с клиентом.</p>
 </div>
</section>



<section class="section-content-newss section-bg-grey">
 <div class="container">
     <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
         "START_FROM" => "0",  // Номер пункта, начиная с которого будет построена навигационная цепочка
         "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
         "SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
     ),
         false
     );?>


     <?$APPLICATION->IncludeComponent("bitrix:news.list", "press-kit", Array(
         "DISPLAY_DATE" => "N",	// Выводить дату элемента
         "DISPLAY_NAME" => "Y",	// Выводить название элемента
         "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
         "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
         "AJAX_MODE" => "N",	// Включить режим AJAX
         "IBLOCK_TYPE" => "press",	// Тип информационного блока (используется только для проверки)
         "IBLOCK_ID" => "24",	// Код информационного блока
         "NEWS_COUNT" => "1",	// Количество новостей на странице
         "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
         "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
         "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
         "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
         "FILTER_NAME" => "",	// Фильтр
         "FIELD_CODE" => array(	// Поля
             0 => "",
             1 => "ID",
             2 => "CODE",
             3 => "XML_ID",
             4 => "NAME",
             5 => "TAGS",
             6 => "SORT",
             7 => "PREVIEW_TEXT",
             8 => "PREVIEW_PICTURE",
             9 => "DETAIL_TEXT",
             10 => "DETAIL_PICTURE",
             11 => "DATE_ACTIVE_FROM",
             12 => "ACTIVE_FROM",
             13 => "DATE_ACTIVE_TO",
             14 => "ACTIVE_TO",
             15 => "SHOW_COUNTER",
             16 => "SHOW_COUNTER_START",
             17 => "IBLOCK_TYPE_ID",
             18 => "IBLOCK_ID",
             19 => "IBLOCK_CODE",
             20 => "IBLOCK_NAME",
             21 => "IBLOCK_EXTERNAL_ID",
             22 => "DATE_CREATE",
             23 => "CREATED_BY",
             24 => "CREATED_USER_NAME",
             25 => "TIMESTAMP_X",
             26 => "MODIFIED_BY",
             27 => "USER_NAME",
             28 => "",
         ),
         "PROPERTY_CODE" => array(	// Свойства
             0 => "PHOTOS",
             1 => "DESCRIPTION",
             2 => "ZIP_ARCHIVE",
         ),
         "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
         "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
         "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
         "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
         "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
         "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
         "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
         "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
         "SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
         "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
         "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
         "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
         "PARENT_SECTION" => "",	// ID раздела
         "PARENT_SECTION_CODE" => "",	// Код раздела
         "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
         "CACHE_TYPE" => "N",	// Тип кеширования
         "CACHE_TIME" => "3600",	// Время кеширования (сек.)
         "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
         "CACHE_GROUPS" => "Y",	// Учитывать права доступа
         "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
         "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
         "PAGER_TITLE" => "Новости",	// Название категорий
         "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
         "PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
         "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
         "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
         "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
         "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
         "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
         "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
         "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
         "COMPONENT_TEMPLATE" => ".default"
     ),
         false
     );?>


     <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
             "AREA_FILE_SHOW" => "file",
             "AREA_FILE_SUFFIX" => "inc",
             "EDIT_TEMPLATE" => "",
             "PATH" => SITE_TEMPLATE_PATH."/includes/tech-rider.php"
         )
     );?>






     <?$APPLICATION->IncludeComponent(
         "bitrix:news.list",
         "news_list_press_center",
         array(
             "COMPONENT_TEMPLATE" => "news_list_press_center",
             "IBLOCK_TYPE" => "services",
             "IBLOCK_ID" => "31",
             "NEWS_COUNT" => "8",
             "SORT_BY1" => "ACTIVE_FROM",
             "SORT_ORDER1" => "DESC",
             "SORT_BY2" => "SORT",
             "SORT_ORDER2" => "ASC",
             "FILTER_NAME" => "",
             "FIELD_CODE" => array(
                 0 => "ID",
                 1 => "CODE",
                 2 => "XML_ID",
                 3 => "NAME",
                 4 => "TAGS",
                 5 => "SORT",
                 6 => "PREVIEW_TEXT",
                 7 => "PREVIEW_PICTURE",
                 8 => "DETAIL_TEXT",
                 9 => "DETAIL_PICTURE",
                 10 => "DATE_ACTIVE_FROM",
                 11 => "ACTIVE_FROM",
                 12 => "DATE_ACTIVE_TO",
                 13 => "ACTIVE_TO",
                 14 => "SHOW_COUNTER",
                 15 => "SHOW_COUNTER_START",
                 16 => "IBLOCK_TYPE_ID",
                 17 => "IBLOCK_ID",
                 18 => "IBLOCK_CODE",
                 19 => "IBLOCK_NAME",
                 20 => "IBLOCK_EXTERNAL_ID",
                 21 => "DATE_CREATE",
                 22 => "CREATED_BY",
                 23 => "CREATED_USER_NAME",
                 24 => "TIMESTAMP_X",
                 25 => "MODIFIED_BY",
                 26 => "USER_NAME",
                 27 => "",
             ),
             "PROPERTY_CODE" => array(
                 0 => "",
                 1 => "",
             ),
             "CHECK_DATES" => "Y",
             "DETAIL_URL" => "",
             "AJAX_MODE" => "Y",
             "AJAX_OPTION_JUMP" => "N",
             "AJAX_OPTION_STYLE" => "Y",
             "AJAX_OPTION_HISTORY" => "N",
             "AJAX_OPTION_ADDITIONAL" => "",
             "CACHE_TYPE" => "N",
             "CACHE_TIME" => "0",
             "CACHE_FILTER" => "N",
             "CACHE_GROUPS" => "Y",
             "PREVIEW_TRUNCATE_LEN" => "",
             "ACTIVE_DATE_FORMAT" => "d.m.Y",
             "SET_TITLE" => "Y",
             "SET_BROWSER_TITLE" => "Y",
             "SET_META_KEYWORDS" => "Y",
             "SET_META_DESCRIPTION" => "Y",
             "SET_STATUS_404" => "N",
             "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
             "ADD_SECTIONS_CHAIN" => "N",
             "HIDE_LINK_WHEN_NO_DETAIL" => "N",
             "PARENT_SECTION" => "",
             "PARENT_SECTION_CODE" => "",
             "INCLUDE_SUBSECTIONS" => "Y",
             "DISPLAY_DATE" => "Y",
             "DISPLAY_NAME" => "Y",
             "DISPLAY_PICTURE" => "Y",
             "DISPLAY_PREVIEW_TEXT" => "Y",
             "PAGER_TEMPLATE" => "news_list",
             "DISPLAY_TOP_PAGER" => "N",
             "DISPLAY_BOTTOM_PAGER" => "Y",
             "PAGER_TITLE" => "Новости",
             "PAGER_SHOW_ALWAYS" => "N",
             "PAGER_DESC_NUMBERING" => "N",
             "PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
             "PAGER_SHOW_ALL" => "N"
         ),
         false
     );?>

 </div>
</section>

<section class="press-center-contact" style="color:white">
 <div class="container ">
  <ul class="row align-items-end">
   <li class="col-md-6 block_1">
    <ul>
     <li>
      <h5 class="press-center-contact-title">Телефон</h5>
      <ul class="section-indents-smd6">
          <?php
          $phone1 = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/kit/phone.php');
          $clean = preg_replace("/[^0-9]/", "",$phone1);
          ?>
       <a href="tel:<?=$clean?>">
        <li class="press-center-contact-tel">
            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/kit/phone.php"
                )
            );?>
        </li>
       </a>


          <?php
          $phone2 = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/kit/phone.php');
          $clean2 = preg_replace("/[^0-9]/", "",$phone2);
          ?>
       <a href="tel:<?=$clean2?>">
        <li class="press-center-contact-tel">
            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/kit/phone2.php"
                )
            );?>
        </li>
       </a>
      </ul>
     </li>
     <li>
      <h5 class="press-center-contact-title section-indents-am8">E-mail</h5>
      <ul class="section-indents-mds5">
          <?php
          $email = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/footer/email.php');
          ?>
       <a href="mailto:<?=$email?>">
        <li class="press-center-contact-email">
            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/header/email.php"
                )
            );?>
        </li>
       </a>

      </ul>
     </li>
     <li>
      <ul class="d-flex press-center-social">
       <a href="https://www.instagram.com/dnorka/"  target="_blank" class="a-press-center-social"><li class="inst"></li></a>
       <li><a href="http://ru-ru.facebook.com/people/Dmitriy-Norka/1211541062" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
       <li><a href="http://vkontakte.ru/id19381151?40138" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
       <li><a href="http://twitter.com/norca_r" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      </ul>
     </li>
    </ul>
   </li>


   <li class="col-md-6 block_2">
    <h4 class="text-center section-title section-title-white section-indents-md">Задать вопрос</h4>
    <form class="form-contact question_form_press_center" action="<?=SITE_TEMPLATE_PATH?>/ajax/question.php" method="POST">
     <ul class="row press-center-ul">
      <li class="col-md-12 col-lg-6 form-comments-name-wrap-blog">
       <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
       <input type="text" placeholder="Ваше имя" name="name" class="form-comments-name d-block form-comments-name-tr form-kit" required/>
      </li>
      <li class="col-md-12 col-lg-6 form-comments-email-wrap-blog">
       <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
       <input type="email" placeholder="Ваш Е-mail" name="email" class="form-comments-email form-comments-email-tr d-block form-kit" required>
      </li>
      <li class="col-md-12 form-comments-comment-wrap-blog section-indents-sm">
       <img src="<?=SITE_TEMPLATE_PATH?>/src/img/pen.png">
       <textarea placeholder="Сообщение" name="question" class="form-comments-comment form-comments-comment-tr d-block form-kit" rows="5"></textarea>
      </li>
      <li class="col-md-12 d-flex justify-content-center">
       <button class="btn-submit">ОТправить</button>
      </li>
     </ul>
    </form>
   </li>


  </ul>
 </div>
</section>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
