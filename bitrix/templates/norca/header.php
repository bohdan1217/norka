<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
	
	$curPage = $APPLICATION->GetCurPage();
function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }
    function selfURL() 
{ 
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
    $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
}  
 
?>

<!DOCTYPE html>
<html>
<head lang="en">
	   <title><?$APPLICATION->ShowTitle()?></title> 
     <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">


    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="cmsmagazine" content="d939b98a50c9db28b4a8cf3a2017dfd4" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 
    <?  
	$APPLICATION->ShowMeta("image", false, true)?>  
	
	

    <meta property="og:description" content="<?=$APPLICATION->ShowProperty("og:description", false, true);?>"/> 
    <meta property="og:url" content="<?=selfUrl();?>"/>
    <meta property="og:title" content="<?=$APPLICATION->ShowProperty("og:title", false, true)?>"/>
    <meta property="og:image" content="<?=$APPLICATION->ShowProperty("og:image", false, true)?>"/>
 
    <meta name="url" content="<?=selfUrl();?>"/>
    <meta name="title" content="<?=$APPLICATION->ShowProperty("og:title", false, true)?>"/>

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/reset.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/fonts/fonts.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.selectBox.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css?v=2"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/responsive.css"/>


    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.selectBox.js"></script>

<script type="text/javascript" encoding="utf-8">
       var _tbEmbedArgs = _tbEmbedArgs || [];
       (function () {
           var u =  "https://widget.textback.io/widget";
           _tbEmbedArgs.push(['widgetId', '60ab396e-0d2e-466d-94f7-a33c31ab1b8d']);
           _tbEmbedArgs.push(['baseUrl', u]);

           var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
           g.type = 'text/javascript';
           g.charset = "utf-8";
           g.defer = true;
           g.async = true;
           g.src = u + '/widget.js';
           s.parentNode.insertBefore(g, s);
       })();

       //_tbOnWidgetReady = function() {
       //  console.log('TextBack widget is ready.');
       //};

       //_tbOnWidgetClick = function(channel) {
       //  console.log('TextBack channel ' + channel + ' was clicked.');
       //};
</script>

<script> 
function jivo_onLoadCallback() { 
window.jivo_cstm_widget = document.createElement('div'); 
jivo_cstm_widget.setAttribute('id', 'jivo_custom_widget'); 
document.body.appendChild(jivo_cstm_widget); 
};

function jivo_onOpen() { 
document.getElementById('textback_widget').className = ''; 
};

function jivositeCallback () { 
jivo_api.open(); 
}; 
</script>
    <?$APPLICATION->ShowHead()?>
    <link rel="canonical" href="<? echo $APPLICATION->GetCurDir(); ?>" />
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js?v=1"></script>
	<meta name='yandex-verification' content='62a7cf34fdc978df' />
<meta name="viewport" content="width=1024">
<meta name="MobileOptimized" content="320"/>
<meta name="HandheldFriendly" content="true"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">	
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="width=720">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>

<?$APPLICATION->ShowPanel()?>



    <?php  
$APPLICATION->AddHeadScript('http://vk.com/js/api/share.js?90'); 
            $APPLICATION->AddHeadScript('http://platform.twitter.com/widgets.js');
            $APPLICATION->AddHeadScript('http://connect.facebook.net/ru_RU/sdk.js#xfbml=1&amp;version=v2.3');

            $APPLICATION->AddHeadScript('http://vk.com/js/api/openapi.js?116');
            
 
    ?>



<!-- SendPulse Form -->
 <style >.sp-force-hide { display: none;}.sp-form[sp-id="82468"] { display: block; background: #ffffff; padding: 25px; width: 450px; max-width: 100%; border-radius: 8px; -moz-border-radius: 8px; -webkit-border-radius: 8px; font-family: Arial, "Helvetica Neue", sans-serif; border-width: 1px; border-color: #dddddd; border-style: solid;}.sp-form-fields-wrapper { margin: 0 auto; width: 400px;}.sp-form[sp-id="82468"] .sp-form-control { background: #ffffff; border-color: #cccccc; border-style: solid; border-width: 1px; font-size: 15px; padding-left: 8.75px !important; padding-right: 8.75px !important; border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; height: 35px; width: 100%;}.sp-form[sp-id="82468"] .sp-field label { color: #444444; font-size: 13px; font-style: normal; font-weight: bold;}.sp-form[sp-id="82468"] .sp-button { border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; background-color: #0089bf; color: #ffffff; width: auto; font-weight: bold;}.sp-form[sp-id="82468"] .sp-button-container { text-align: left;}.sp-popup-outer { background: rgba(0, 0, 0, 0.5);}.sp-form[sp-id="82468"] .sp-d107f569-468d-4446-acaf-eb45d35329fb-container { text-align: left;}</style><div class="sp-form-outer sp-popup-outer sp-force-hide" style="background: rgba(0, 0, 0, 0.5);"><div id="sp-form-82468" sp-id="82468" sp-hash="3507ad8d4cf89cd49d4967eb03ee1d2b5db367bb824875415909c73146c7c635" sp-lang="ru" class="sp-form sp-form-regular sp-form-popup" sp-show-options="%7B%22amd%22%3Afalse%2C%22condition%22%3A%22onEnter%22%2C%22scrollTo%22%3A25%2C%22delay%22%3A10%2C%22repeat%22%3A3%2C%22background%22%3A%22rgba(0%2C%200%2C%200%2C%200.5)%22%2C%22position%22%3A%22bottom-right%22%2C%22hideOnMobile%22%3Afalse%2C%22urlFilter%22%3Afalse%2C%22urlFilterConditions%22%3A%5B%7B%22force%22%3A%22hide%22%2C%22clause%22%3A%22contains%22%2C%22token%22%3A%22%22%7D%5D%7D"><div class="sp-form-fields-wrapper"><button class="sp-btn-close ">&nbsp;</button><div class="sp-message"><div></div></div><div class="sp-element-container"><div class="sp-field " sp-id="sp-19be6eb4-75d0-46c0-a6d7-31f1fea6934d"><div style="font-family: Arial, &quot;Helvetica Neue&quot;, sans-serif; line-height: 1.2;"><p style="text-align: center;">&nbsp;</p><p style="text-align: center;"><span style="font-size: 20px;"><strong>Получите </strong></span><span style="font-size: 20px;"><strong>бесплатно </strong><strong>мини книгу </strong></span></p><p style="text-align: center;">&nbsp;</p></div></div><div class="sp-field full-width sp-d107f569-468d-4446-acaf-eb45d35329fb-container" sp-id="sp-d107f569-468d-4446-acaf-eb45d35329fb"><img class="sp-image " src="//login.sendpulse.com/files/emailservice/userfiles/f786b4e0582e9af373f99a3103915991743105/2%20%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20%D0%BA%D0%BD%D0%B8%D0%B3%D0%B8-%D0%BF%D0%BD%D0%B3.png"></div><div class="sp-field " sp-id="sp-23efae95-80b4-4b68-ba5e-b020dbad40e2"><div style="font-family: inherit; line-height: 1.2;"><p>Чтобы получить книгу вы можете оформить подписку через мессенджеры Fakebook, В Контакте, Telegram, WhatsApp, или через вашу электронную почту. Выберите, пожалуйста, удобный для вас способ получения книги.</p></div></div>
<div style="text-align: center">
        <style>
            tb-notification-widget a {
                color: #fff !important;
            }
            
            @media (max-width: 600px) {
                tb-notification-button {
                    display: block;
                    width: 200px;
                }
            }
            </style>
            <script src="//cdn.polyfill.io/v2/polyfill.min.js?features=default,fetch,Promise"></script>
            <script src="//unpkg.com/@textback/notification-widget@1.x/build/index.js"></script>
            <tb-notification-widget widget-id="e521ce8a-6a91-4220-9ebb-0d79419bf48a" api-path="https://api.textback.io/api">
            </tb-notification-widget>
        </div>
<div class="sp-field " sp-id="sp-9bf416b0-fb88-4655-9d10-063a4a66ad19"><label class="sp-control-label"><span >Ваше имя</span></label><input type="text" sp-type="input" name="sform[0LjQvNGP]" class="sp-form-control " placeholder="имя" sp-tips="%7B%7D"></div><div class="sp-field " sp-id="sp-d3937c73-afe4-449d-bacf-bada84f7b640"><label class="sp-control-label"><span >Email</span><strong >*</strong></label><input type="email" sp-type="email" name="sform[email]" class="sp-form-control " placeholder="username@gmail.com" sp-tips="%7B%22required%22%3A%22%D0%9E%D0%B1%D1%8F%D0%B7%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%20%D0%BF%D0%BE%D0%BB%D0%B5%22%2C%22wrong%22%3A%22%D0%9D%D0%B5%D0%B2%D0%B5%D1%80%D0%BD%D1%8B%D0%B9%20email-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%22%7D" required="required"></div><div class="sp-field " sp-id="sp-d7d6cfb9-72a9-448c-999f-85615aab3637"><label class="sp-control-label"><span >Телефон</span></label><input type="tel" sp-type="phone" name="sform[phone]" class="sp-form-control " style="text-indent:70px" placeholder=" " sp-tips="%7B%22wrong%22%3A%22%D0%9D%D0%B5%D0%B2%D0%B5%D1%80%D0%BD%D1%8B%D0%B9%20%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%20%D1%82%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD%D0%B0%22%7D"></div><div class="sp-field sp-button-container " sp-id="sp-62a08ac5-d331-4581-ac53-aebb083134c8"><button id="sp-62a08ac5-d331-4581-ac53-aebb083134c8" class="sp-button">Подписаться </button></div></div><div class="sp-link-wrapper sp-brandname__left"></div></div></div></div><script type="text/javascript" src="//static-login.sendpulse.com/apps/fc3/build/default-handler.js?1505921137568"></script> 
<!-- /SendPulse Form -->

<div class="wrapper_q">


<div class="wrapper"> 
    <header <?if($curPage == '/') echo 'class="index"'; ?>>
        <a href="/"><div class="logo"></div></a>
        <div class="right_head">
            <nav>
            <?$APPLICATION->IncludeComponent( 'bitrix:menu', 'header_menu', array(
					"ROOT_MENU_TYPE" => "top", 
				    "MAX_LEVEL" => "1", 
				    "CHILD_MENU_TYPE" => "top", 
				    "USE_EXT" => "Y",
				    "DELAY" => "N",
				    "ALLOW_MULTI_SELECT" => "N",
				    "MENU_CACHE_TYPE" => "N", 
				    "MENU_CACHE_TIME" => "3600", 
				    "MENU_CACHE_USE_GROUPS" => "Y", 
				    "MENU_CACHE_GET_VARS" => "" 
				) );?>
            </nav>
 

            <script>document.write('<ul class="social"> +7 (495) 776-82-34     +7 (495) 773-11-01   <li><a class="home" href="/"></a></li><li><a class="msg" href="mailto:<?=COption::GetOptionString('main', 'email_from', 'admin@norca.ru')?>"></a></li><li><a class="map" href="/sitemap/"></a></li><li><a class="in" target="_blank" href="https://www.linkedin.com/in/dmitrinorca"></a></li><li><a class="fb" target="_blank" href="https://www.facebook.com/profiprod.ru"></a></li><li><a class="tw" target="_blank" href="http://twitter.com/norca_ru"></a></li><li><a class="vk" target="_blank" href="http://vk.com/club24131096"></a></li><li><a class="ok" target="_blank" href="http://wg18.odnoklassniki.ru/dk?st.cmd=userMain&amp;tkn=5967"></a></li></ul>');</script> 
        </div>
    </header>
 


     <?

     if($curPage !== '/') {

         $APPLICATION->IncludeComponent( 'bitrix:menu', 'sub_menu', array(

                    "ROOT_MENU_TYPE" => "main_child", 

                    "MAX_LEVEL" => "1", 
                    "CHILD_MENU_TYPE" => "", 
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N", 
                    "MENU_CACHE_TIME" => "3600", 
                    "MENU_CACHE_USE_GROUPS" => "Y", 
                    "MENU_CACHE_GET_VARS" => "" 
                ) ); 


         $APPLICATION->IncludeComponent("bitrix:breadcrumb","custom",Array(
                "START_FROM" => "0", 
                "PATH" => "", 
                "SITE_ID" => "s1" 
              )
          );
      }?>
      

  