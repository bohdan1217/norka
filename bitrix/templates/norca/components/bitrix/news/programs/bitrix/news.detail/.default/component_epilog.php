<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?php 
/*
global $USER;
if ($USER->IsAdmin()) {echo "<pre>"; print_r($arResult); echo "</pre>";}*/

global $APPLICATION;

 
$APPLICATION->SetPageProperty("description",$arResult['IPROPERTY_VALUES']["ELEMENT_META_DESCRIPTION"] );
$APPLICATION->SetPageProperty("title", $arResult['IPROPERTY_VALUES']["ELEMENT_META_TITLE"]);
$APPLICATION->SetPageProperty("image", 'http://'.$_SERVER['SERVER_NAME'].SITE_TEMPLATE_PATH . '/img/logo.png');

$APPLICATION->SetPageProperty("og:description", $arResult['IPROPERTY_VALUES']["ELEMENT_META_DESCRIPTION"] );
$APPLICATION->SetPageProperty("og:title",$arResult['IPROPERTY_VALUES']["ELEMENT_META_TITLE"]);
$APPLICATION->SetPageProperty("og:url",  selfUrl());

$APPLICATION->SetPageProperty("og:image", 'http://'.$_SERVER['SERVER_NAME'].SITE_TEMPLATE_PATH . '/img/logo.png');