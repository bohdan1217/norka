<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
 
?>  
 
 
            <h1><?=$arResult["NAME"]?></h1>
            

            <div>
			 <?echo $arResult["DETAIL_TEXT"];?>

<?

$htmlLinks ='<div>';

foreach ($arResult["TOLEFT"] as $rg) {
	if (mb_strlen($rg["NAME"]) > 2){
		$htmlLinks .= '<a class="fleft" id="previous_page" href="'.$rg["URL"].'"> '.$rg["NAME"].'</a><br>';
	}
}

foreach ($arResult["TORIGHT"] as $rg) {
if (mb_strlen($rg["NAME"]) > 2){
		$htmlLinks .= '<a class="fright" id="previous_page" href="'.$rg["URL"].'"> '.$rg["NAME"].' </a><br>';
	 } 
}


$htmlLinks .='</div>';

$GLOBALS['htmlLinks'] = $htmlLinks;

?>

</div>


            <div class="open_program-href" >
                 <a href="/trainings/public/">Посмотреть где и когда данный тренинг будет проходить в открытом формате</a>
               </div>