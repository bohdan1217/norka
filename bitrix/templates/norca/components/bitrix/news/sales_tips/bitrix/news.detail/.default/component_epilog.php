<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?php 
/*
global $USER;
if ($USER->IsAdmin()) {echo "<pre>"; print_r($arResult); echo "</pre>";}*/

global $APPLICATION;


$desc = $arResult['IPROPERTY_VALUES']["ELEMENT_META_DESCRIPTION"];
$title = $arResult['IPROPERTY_VALUES']["ELEMENT_META_TITLE"];

if(empty($desc))
{
	$desc = $arResult['DESC'];
}
if(empty($title))
{
	$title = $arResult['TITLE'];
}
$APPLICATION->SetPageProperty("description", $desc );
$APPLICATION->SetPageProperty("title", $title);
$APPLICATION->SetPageProperty("image", 'http://'.$_SERVER['SERVER_NAME'].SITE_TEMPLATE_PATH . '/img/logo.jpg');

$APPLICATION->SetPageProperty("og:description", $desc );
$APPLICATION->SetPageProperty("og:title", $title);
$APPLICATION->SetPageProperty("og:url",  selfUrl());


$APPLICATION->SetPageProperty("og:image", 'http://'.$_SERVER['SERVER_NAME'].SITE_TEMPLATE_PATH . '/img/logo.jpg');