<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<ul>
     

   
<?foreach($arResult["ITEMS"] as $arItem):?>

   <li id="<?=$this->GetEditAreaId($arItem['ID'])?>">
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="title_t"><?echo $arItem["NAME"]?></a>
        <div class="desc_t"><?echo $arItem["PREVIEW_TEXT"]?></div>
        <div class="inf_t">
            <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">читать далее &gt;&gt;</a>
            <div class="right_inf">
                <span class="date"><?echo $arItem["ACTIVE_FROM"]?></span>
                <div class="views"><div class="icon"></div><?echo $arItem["SHOW_COUNTER"]?></div>
                <div class="comments_inf"><div class="icon"></div>  <?$APPLICATION->IncludeComponent('ritg:comments', 'counter', array( "IBLOCK_ID" => "26",  "GROUP_ID" => $arItem["ID"] )); ?>
</div>
            </div>
        </div>
    </li>



<?endforeach;?>

 
            </ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

 