<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?> 

<div class="books">
 
<?if(!empty($arResult)){?>
	<?
	$this->AddEditAction($arResult['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

?>

            <div class="item"  id="<?=$this->GetEditAreaId($arResult['ID']);?>">
                <div class="img">
                    <?php echo CFile::ShowImage($arResult["PREVIEW_PICTURE"]["SRC"], 300, 300, 'border=0', false); ?>
                </div>
                <div class="text">
                    <h3><?echo $arResult["NAME"]?></h3>
                    <div>
                    <?echo $arResult["DETAIL_TEXT"];?>
                     </div>
                </div>
                <div class="clear_float"></div>
            </div>

<? } ?>



</div>

