<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="slider_part">
	<div class="arrow-left"></div>
	<div class="arrow-right"></div>
<div class="swiper-container">

    <div class="swiper-wrapper">

	<?foreach($arResult["ITEMS"] as $arItem):?>


		<div class="swiper-slide"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
			<div>
				<a href="/dmitry-norca/clients/"><img src="<?=CFile::GetPath($arItem["~PREVIEW_PICTURE"]);?>" alt="<?=$arItem["NAME"];?>"></a>
			</div>
		</div>


	<?endforeach;?>

    </div> 
</div>


</div>
 

