jQuery(function() {
 

    var  slidesLength =  $('.slider_part .swiper-slide').length,

        mySwiper = new Swiper('.slider_part .swiper-container',{
        paginationClickable: true,
        slidesPerView: Math.min( 4,  slidesLength),
        loop: true
    });

    $('.slider_part .arrow-left').on('click', function(e){
        e.preventDefault();
        mySwiper.swipePrev();
    });
    
    $('.slider_part .arrow-right').on('click', function(e){
        e.preventDefault();
        mySwiper.swipeNext();
    });

    var  supw = !!window.innerWidth,
        reInitSwiper = function() {
            var $ww = supw ? window.innerWidth : $(window).width();
            if (mySwiper && mySwiper.params) {
                    mySwiper.params.slidesPerView = Math.min( 4,  slidesLength);
                    mySwiper.reInit();
            }
        };
    reInitSwiper();
    $(window).resize(reInitSwiper);
})


