<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?> 
 
<!-- 
               <ul>
                    <li><a href="#"><div><img src="./img/social/skype_cont.png"></div>Норка в скайпе</a></li>
                    <li><a href="#"><div><img src="./img/social/vk_cont.png"></div>Норка на vk.com</a></li>
                    <li><a href="#"><div><img src="./img/social/fb_cont.png"></div>Норка на facebook.com</a></li>
                    <li><a href="#"><div><img src="./img/social/lj_cont.png"></div>Норка в LiveJornal</a></li>
                    <li><a href="#"><div><img src="./img/social/tw_cont.png"></div>Норка в Twitter</a></li>
                    <li><a href="#"><div><img src="./img/social/ok_cont.png"></div>Норка на ok.ru</a></li>
                </ul>
 -->

<ul>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	
<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">

<?php $url = $arItem["PROPERTIES"]["URL"]["VALUE"]; ?>
	<a href="<?=$url;?>" target="_blank">
	<div><?php echo CFile::ShowImage( $arItem["PREVIEW_PICTURE"]["SRC"], 50, 50, 'border=0', false); ?></div><?echo $arItem["NAME"]?></a>
</li>
<?endforeach;?>

</ul>
  