<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?> 

 

<?php 
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH. '/js/moment.min.js'); ?>



<?php 

    $offset = 0; 


    function drawTree(  $id, $th, $arResult, $parentId ) {
 
        $arItem = $arResult["ITEMS_X"][$id];

        global $offset;
  
        if($offset >= 2)
            $offset = 1;
        else{
            $offset++;
        }

        if($parentId == 0){
            $offset = 0;
        } 
 

 
        $html = '<li  data-id="'.$id.'" '. ( $parentId ? ('data-parentId="'. $parentId .'"') : '' ) .'class="comment '. ($offset ? ('answ_'. $offset) : '' ) .  '" id="'. $th->GetEditAreaId($id). '">'
                   
                    .$th->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"))
                    .$th->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')))
                   .'<div class="wrap_comment">
                        <input type="hidden" name="id" value="'.$id.'">
                        <div class="name"><p>'.$arItem["NAME"].'</p><span data-date="'.$arItem["ACTIVE_FROM"].'" ></span></div>
                        <div class="content_com">
                            <div class="ava">
                                '. CFile::ShowImage($arItem["PREVIEW_PICTURE"]["SRC"], 300, 300, 'border=0', false).'
                            </div>
                            <div class="text">
                                '.$arItem["PREVIEW_TEXT"].'
                            </div>
                            <div class="tumbs">
                                <span class="count_dislike">-'.$arItem["PROPERTIES"]["DISLIKES"]["VALUE"].'</span>
                                <a href="#" class="dislike"></a>
                                <div class="line"></div>
                                <span class="count_like">+'. $arItem["PROPERTIES"]["LIKES"]["VALUE"].'</span>
                                <a href="#" class="like"></a>
                                <a class="answer answer_but_toggle" href="#">Ответить</a>
                                <div class="send_comment" style="display: none;">
                                    <div>
                                        <textarea name="message"></textarea>
                                    </div>
                                    <div>
                                        <input type="button" value="Отправить">
                                    </div>
                                </div>
                                <span class="share">Поделиться &gt;&gt;</span>
                                <ul class="soc_com">
                                    <li><a class="fb" href="#"></a></li>
                                    <li><a class="tw" href="#"></a></li>
                                    <li><a class="vk" href="#"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </li>';


        $tree = $arResult["COMMENTS_TREE"][$id]["CHILDS_ID"];
                 
        if( is_array( $tree ) && !empty( $tree ) ) {

                foreach( $tree as $cId  ){
                        $html.= drawTree( $cId, $th, $arResult, $id );
                }

         }

        return $html;

    }


 ?> 

<!-- 
 <div class="comments">
                <h5>Оставьте, пожалуйста, свой комментарий</h5>
                <span>3 комментрия</span>
                <p>Поделитесь этим советом со своими друзьями:</p>
                <div class="social_share">Тут будут кнопки поделится</div>
                <div class="auth_c">
                    <p>
                        Войти как пользователь
                        <a href="#" class="fb"></a>
                        <a href="#" class="tw"></a>
                        <a href="#" class="vk"></a>
                        или оставить сообщение как
                        <span>гость</span></p>
                </div>
                <form action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data" type="POST">
                    <textarea name="message"></textarea>
                    <input type="submit" value="Отправить комментарий">
                </form>
                 <ul>
                <?//foreach($arResult["COMMENTS_TREE"] as $curId => $tree) if(!$tree["NO_MAIN"]):?>
                    <?//php echo drawTree(  $curId, $this, $arResult,  0 ); ?>
                <?//endif;?>
                </ul>

 
            </div>
 -->
            <!-- KAMENT -->

 <div class="comments">
<div id="kament_comments"></div>
<script type="text/javascript">
    /* * * НАСТРОЙКА * * */
    var kament_subdomain = 'flegent';

    /* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
    (function() {
        var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
        node.src = 'http://' + kament_subdomain + '.svkament.ru/js/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(node);
    })();
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>

</div>

<script>/*
$(function(){


    moment.locale('ru');
        
    $('[data-date]').each(function(){

        var $this = $(this),
            date =$(this).data('date');
     
            if( date )
                $this.text( moment( date, "DD.MM.YYYY HH:mm:ss").fromNow() )

    });


    $('.comments')

    .on('click', '.answer_but_toggle',function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).next('.send_comment').toggle().find('textarea').focus();
    })

    .on('click', '.send_comment input[type=button]', function(e){

        e.stopPropagation();
        e.preventDefault();

        console.log( $(this).closest('.send_comment').find('textarea').val() );

    })
    .on('click', '.comment .like', function(){

    })

    .on('click', '.comment .like', function(){
        
    });*/

})

</script>