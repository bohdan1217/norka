<?php 

	$commentTree = array();
	$items = array();

	$childsIds = array();

	foreach ($arResult["ITEMS"] as $i => $mainComment ) {

		$items[$mainComment["ID"]] = $mainComment;

		unset($arResult["ITEMS"][$i]);

	}

	foreach ($items as $id => $mainComment ) {
 

		$CHILDS_ID = $mainComment["PROPERTIES"]["CHILDS_ID"]["VALUE"];

 		if( empty( $CHILDS_ID ) ) {

 			$commentTree[$id] = array();
 
 		} else {

			$childsIds = array_merge( $CHILDS_ID, $childsIds );
 			$commentTree[$id]["CHILDS_ID"] = $CHILDS_ID;
 
 		}


	}
 
	foreach ($childsIds as $id ) {
  			if( isset( $commentTree[$id] ) ) { 
				$commentTree[$id]["NO_MAIN"] = 'Y';  
  			}
	}
 

	$arResult["COMMENTS_TREE"] = $commentTree;
	$arResult["ITEMS_X"] = $items; 
 
 

?>