<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<div class="press-kit tips_i ">
           
 

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
 		<div class="screen">

				<?php foreach( $arItem["PROPERTIES"]["PHOTOS"]["VALUE"] as $photoId ): ?>
					<?php 
					$newImage = CFile::ResizeImageGet(  $photoId  , array( 'width'=>238, "height" => 340 ), BX_RESIZE_IMAGE_EXACT, true); 
						 

					?>
					<div><img src="<?=$newImage["src"]?>" alt=""></div>
				<?php endforeach; ?>


				</div>
 

            <div class="desc"><?=$arItem["PREVIEW_TEXT"];?></div>


            <a class="button_g" href="<?=CFile::GetPath( $arItem["PROPERTIES"]["ZIP_ARCHIVE"]["VALUE"] );?>"><div>СКАЧАТЬ (архив zip)</div></a>
       	



<!-- 
       	 </div> -->
</div>
<?endforeach;?>












</div>