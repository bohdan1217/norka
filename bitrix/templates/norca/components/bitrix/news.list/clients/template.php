<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="clients tips_i">

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

 <ul>

	<?foreach($arResult["ITEMS"] as $arItem):?>
 
 <li  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
                    <div style="display: table-cell;" ><div class="point"></div></div>
                    <div class="cont">
                        <p style="font-size: 19px; font-weight: bold; margin-bottom: 12px; font-style: normal;"><?=$arItem["NAME"];?></p>
                        <a  ><?=$arItem["PROPERTIES"]["URL"]["VALUE"];?></a>
                        <p><?=$arItem["DETAIL_TEXT"];?></p>
                    </div>
  </li>
	<?endforeach;?>
</ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
 

<?
?>

