<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

 

 	<div class="banner-cont">
<?foreach($arResult["ITEMS"] as $arItem){
 
	$html .= '<div class="banner-item " id="'.$this->GetEditAreaId($arItem['ID']).'">';
	
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	
	<?php 

		if(!empty($arItem["PROPERTIES"]["URL"]["VALUE"]))
		{?>

		<?php 

			$url = $arItem["PROPERTIES"]["URL"]["VALUE"];

			if(!preg_match('/https?\:\/\//', $url)) {
				$url = 'http://' . $url;
			}

		if ($url=='http://www.life-line.ru/you_can_do_it_now/it_easy')
		{
			$html .= '<noindex><a href="'.$url.'" target="_blank" rel="nofollow">';
		}
		else{
			$html .= '<a href="'.$url.'" target="_blank">';
		}

		}

			$html .= '<img src="'.$arItem["PREVIEW_PICTURE"]["SRC"].'" alt="">';
	

		if(!empty($arItem["PROPERTIES"]["URL"]["VALUE"]))
		{
			if ($url=='http://www.life-line.ru/you_can_do_it_now/it_easy')
			{
	  		$html .= '</a></noindex>';
			}
			else{
				$html .= '</a>';
			}
		}

	$html .= '</div>';
		
} ?>

	<script>
		document.write('<?=$html;?>');
	</script>

 </div>