<?php 


	$ids = array();

 
	foreach ($arResult["ITEMS"] as $i => $arItem)
	{
		
		if(!empty($arItem["PROPERTIES"]["PROGRAMS"]["VALUE"]))
			$ids[ $arItem["PROPERTIES"]["PROGRAMS"]["VALUE"] ][] = $i;

	}



	if( !empty( $ids ) )
	{


			$arPrograms = CIBlockElement::GetList(
				array(

				), 

				array(

					"ID" =>  array_keys( $ids ),
					"IBLOCK_ID" => 20,
					"PROPERTY_OPEN_TRAINING_VALUE" => "Y"

				), 

				false,
				false,

				array(

					"ID",
					"IBLOCK_ID",
					"PROPERTY_OPEN_TRAINING"

				));


			$arFiltrate = array();



			while( $arElement = $arPrograms->GetNextElement() )
			{
				
				$ob = $arElement->GetFields();

				$arFiltrate[ $ob["ID"] ] = $ids[ $ob["ID"] ]; 
			}
 


			foreach ( $arResult["ITEMS"] as $i => $arItem )
			{

				if(!empty($arItem["PROPERTIES"]["PROGRAMS"]["VALUE"])) {
					
					$id = $arItem["PROPERTIES"]["PROGRAMS"]["VALUE"];

					
					 if( !isset( $arFiltrate[$id] ) || !in_array( $i, $arFiltrate[ $id ] ) ) {
					 	unset( $arResult[ "ITEMS" ][ $i ] );
					 }

				}

			}
	}


 ?>