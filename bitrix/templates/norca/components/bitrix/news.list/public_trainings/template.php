<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?> 

 <div class="open_traning tips_i">

 <?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

            <ul>


		
<?foreach($arResult["ITEMS"] as $i=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
 
                <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <h4><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></h4>
                    <div>
                        <p><b>Город: </b><?echo $arItem["PROPERTIES"]["CITY"]["VALUE"]?></p>
                        <p><b>Программа: </b><?echo $arItem["DISPLAY_PROPERTIES"]["PROGRAMS"]["DISPLAY_VALUE"]?></b></p>
                        <p><b>Продолжительность: </b><?echo $arItem["PROPERTIES"]["PERIOD"]["VALUE"]?></p>
                        <p><b>Стоимость: </b><?echo $arItem["PROPERTIES"]["STOIMOST"]["VALUE"]?></p>
                    </div>
                    <a href="takepart/" class="button_g tr"><div>Принять участие</div></a>
                </li>
     
<?endforeach;?>

 
</ul> 

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
