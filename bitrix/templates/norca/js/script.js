$(document).ready(function() {

	if ($('select').selectBox)
		$('select').selectBox();

	if ($('.fancybox').fancybox)
		$('.fancybox').fancybox();

	$('.vk_share').html(VK.Share.button(false, {
		type: "round",
		text: "Сохранить"
	}));
	try {
		twttr.widgets.load();
		FB.XFBML.parse();
	} catch (ex) {}



	var $errorEl,
		rEmail = /[^\@]{2,}\@[^\@]{2,}/,
		tId ;

	$('.contacts_form #email').hide();


	$('.validate').on('submit', function(e) {



		$('.errortext').remove();
		$errorCont = null;


		var $inputsLabels = $(this).find('.text_c, .inp_cont').filter(function() {
				return $(this).find('span').length;
			}),
			$inputs = $inputsLabels.next('input[type="text"], textarea'),
			i = 0,
			l = $inputs.length,
			$curInput,
			errorCont = [],
			val;



		for (; i < l; i++) {

			$curInput = $inputs.eq(i);



			if (!(val = $.trim($curInput.val())).length) {
				errorCont.push('Поле \'' + $inputsLabels.eq(i).contents().first().text() + '\' не может быть пустым');
				$curInput.css('border', '1px solid red');
			} else {


				if ($curInput.is('.email')) {
					if (!rEmail.test(val)) {
						errorCont.push("Неверный формат email");
						$curInput.css('border', '1px solid red');
					}
				}

			}
		}

		clearTimeout(tId);

		tId = setTimeout(function() {

			$inputs.css('border', '');
			$errorEl.slideUp();

		}, 7000);



		if (!errorCont.length) {
			$inputs.css('border', '');
			$errorEl.slideUp();
			return true;
		} else {

			
		e.preventDefault();
		e.stopPropagation();


			$errorEl = $('<font class="errortext">').hide().prependTo(this)
			
			$('html, body').stop().animate({
				'scrollTop': $('form').offset().top - 50
			}, 500);

			$errorEl.html(errorCont.join('<br>')).slideDown();
		}

		return false;

	});


	$('input[type="text"], textarea').change(function() {


		$(this).css('border', '');



	})


});