<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
	
	$curPage = $APPLICATION->GetCurPage();
?>


<!DOCTYPE html>
<html>
<head lang="en">
	<title><?$APPLICATION->ShowTitle()?></title>
    
    <link href="<?=SITE_TEMPLATE_PATH?>/img/logo.png" rel="shortcut icon"  type="image/png" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/reset.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/fonts/fonts.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/responsive.css"/>

    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>


    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.selectbox.js"></script>

    <?$APPLICATION->ShowHead()?>
    
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
</head>
<body>

<?$APPLICATION->ShowPanel()?>


    <?php  $APPLICATION->AddHeadScript('//vk.com/js/api/openapi.js?116');?>

<div class="wrapper_q">
<div class="wrapper">
    <header>
        <a href="/"><div class="logo"></div></a>
        <div class="right_head">
            <nav>
            <?$APPLICATION->IncludeComponent( 'bitrix:menu', 'header_menu', array(
					"ROOT_MENU_TYPE" => "top", 
				    "MAX_LEVEL" => "1", 
				    "CHILD_MENU_TYPE" => "top", 
				    "USE_EXT" => "Y",
				    "DELAY" => "N",
				    "ALLOW_MULTI_SELECT" => "N",
				    "MENU_CACHE_TYPE" => "N", 
				    "MENU_CACHE_TIME" => "3600", 
				    "MENU_CACHE_USE_GROUPS" => "Y", 
				    "MENU_CACHE_GET_VARS" => "" 
				) );?>
            </nav>


            <ul class="social">
                <li><a class="home" href="/"></a></li>
                <li><a class="msg" href="mailto:<?=COption::GetOptionString('main', 'email_from', 'admin@norca.ru')?>"></a></li>
                <li><a class="map" href="/sitemap/"></a></li>
                <li><a class="in" target="_blank" href="https://www.linkedin.com/in/dmitrinorca"></a></li>
                <li><a class="fb" target="_blank" href="https://ru-ru.facebook.com/skazokhnik"></a></li>
                <li><a class="tw" target="_blank" href="http://twitter.com/norca_ru"></a></li>
                <li><a class="vk" target="_blank" href="http://vkontakte.ru/id19381151?40138"></a></li>
                <li><a class="ok" target="_blank" href="http://wg18.odnoklassniki.ru/dk?st.cmd=userMain&tkn=5967"></a></li>
            </ul> 



        </div>
    </header>

  