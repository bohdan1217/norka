$(document).ready(function(){
    var mySwiper = new Swiper('.slider_part .swiper-container',{
        paginationClickable: true,
        slidesPerView: 5,
        loop: true
    });

    $('.slider_part .arrow-left').on('click', function(e){
        e.preventDefault();
        mySwiper.swipePrev();
    });
    
    $('.slider_part .arrow-right').on('click', function(e){
        e.preventDefault();
        mySwiper.swipeNext();
    });

    var  supw = !!window.innerWidth,
        reInitSwiper = function() {
            var $ww = supw ? window.innerWidth : $(window).width();
            if (mySwiper && mySwiper.params) {
                if ($ww <= 1750 && $ww > 1400) {
                    mySwiper.params.slidesPerView = 4;
                    mySwiper.reInit();
                } else if ($ww <= 1400 && $ww > 1050) {
                    mySwiper.params.slidesPerView = 3;
                    mySwiper.reInit();
                } else if ($ww <= 1050 && $ww > 1000) {
                    mySwiper.params.slidesPerView = 2;
                    mySwiper.reInit();
                } else {
                    mySwiper.params.slidesPerView = 5;
                    mySwiper.reInit();
                }
            }
        };
    reInitSwiper();
    $(window).resize(reInitSwiper);

    $('select').selectBox();

    $("ul .comment").each(function(){
        var a1 = $(this).attr('class');
        a1 = a1.replace('comment','');
        a1 = a1.replace(' ','');
        var a2 = a1.replace('answ_','');
        if (a1 !== '') {
            var a3 = 130 * a2;
            $(this).addClass('answer');
            $(this).css('padding-left',a3);
        }
    });

});