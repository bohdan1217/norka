<?php
if(strlen($_POST['get'])>0) $get = explode("&",$_POST['get']);
foreach($get as $getI){
    $getI = explode("=",$getI);
    $_GET[$getI[0]] = $getI[1];
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) die('error');

//CPageOption::SetOptionString("main", "nav_page_in_session", "N");
CPageOption::SetOptionString("main", "nav_page_in_session", "Y");

$id = $_POST['data']['id'];
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_ANON_NAME", "PREVIEW_TEXT");
$arFilter = Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y", 'PROPERTY_GROUP_ID' => $id);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
$total = $res -> SelectedRowsCount();
if($total) {
    ?>
    <h4 class="newss-post-title section-indents-smd4">Комментарии</h4>
    <ul class="row comments-list comments-list-grey section-indents-mdl4">
        <?
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            ?>
            <li class="col-md-12 comments-list-nav-news">
                <ul class="row">
                    <li class="col-md-2 clients">
                        <div class="comments-photo-wrap comments-photo-wrap-news">
                            <div class="comments-photo-quote-news">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/src/img/comment/quote.png"/></div>
                            <img src="<?= SITE_TEMPLATE_PATH ?>/src/img/no-image.png" alt="" class="d-block"/>
                        </div>
                    </li>
                    <li class="col-md-10">
                        <h5 class="comments-name"><?= $arFields ["PROPERTY_ANON_NAME_VALUE"] ?></h5>
                        <p class="comments-text"><?= $arFields['PREVIEW_TEXT'] ?></p>
                    </li>
                </ul>
            </li>
        <?
        } ?>
    </ul>
    <?
    $navStr = $res->GetPageNavStringEx($navComponentObject, "Страницы:", "comment_in_element");
    echo $navStr;
}
?>