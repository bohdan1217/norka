<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');

$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require($DOCUMENT_ROOT. "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX

use Bitrix\Main\Entity;

if(Cmodule::IncludeModule('iblock')) {
    global $APPLICATION;
    /*if (!$APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_code"]) && $USER -> IsAuthorized() == false) {
        exit('Не правильная капча');
    }*/

    $reviews_iblosk_id = 26;
    if (!empty($_POST['IBLOCK_ID']))
        $reviews_iblosk_id = (int)$_POST['IBLOCK_ID'];

    if (
    isset($_POST['email'])
    ) {

        $name = htmlspecialchars($_POST['name']);
        $email = htmlspecialchars($_POST['email']);
        $phone = htmlspecialchars($_POST['phone']);

        $id_news = htmlspecialchars($_POST['id_news']);
        $name_news = htmlspecialchars($_POST['name_news']);

        $comment = htmlspecialchars($_POST['comment']);


        $subject = 'Комментарий к';
        if (!empty($_POST['subject']))
            $subject = htmlspecialchars($_POST['subject']);

        $el = new CIBlockElement;

        $PROP = $_POST['PROP'];
        if (empty($_POST['PROP']))
            $PROP = array();

        $PROP['ANON_NAME'] = $name;
        $PROP['EMAIL'] = $email;

        $PROP['PHONE'] = $phone;

        $PROP['GROUP_ID'] = $id_news;


        $element_title = $subject . ' : ' . $name_news;


        if (!empty($_POST['title']))
            $element_title = htmlspecialchars($_POST['title']);

        $ACTIVE = 'N';
        if (!empty($_POST['ACTIVE']) && $_POST['ACTIVE'] == 'Y')
            $ACTIVE = 'Y';

        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => '',          // элемент лежит в корне раздела
            "IBLOCK_ID" => $reviews_iblosk_id,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $element_title,
            "ACTIVE" => $ACTIVE,
            "CODE" => teil_get_lat_name($element_title),
            "PREVIEW_TEXT" => $comment,
            "DETAIL_TEXT" => $comment,
            "DETAIL_PICTURE" => '',
            'DATE_ACTIVE_FROM' => date('d.m.Y H:i:s', time())
        );


        if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            // отправляем заявку админу
//            $email_body = '';
//
//            //формируем список заполненных полей
//            $arSelect = Array("ID", "NAME", "IBLOCK_ID");
//            $arFilter = Array("IBLOCK_ID" => $reviews_iblosk_id, 'ID' => $PRODUCT_ID);
//            $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize" => 1), $arSelect);
//            $total = $res->SelectedRowsCount();
//            while ($ob = $res->GetNextElement()) {
//                $arFields = $ob->GetFields();
//                $arProps = $ob->GetProperties();
//                foreach ($arProps as $prop) {
//                    if (!empty($PROP[$prop['CODE']]))
//                        $email_body .= $prop['NAME'] . ': ' . htmlspecialchars($PROP[$prop['CODE']]) . '<br/>';
//                }
//            }
//
//
//            // отправляем на почту админу
//            $site = CSite::GetByID(SITE_ID);
//            $site_res = $site->arResult;
//            $server_mail = $site_res[0]['EMAIL'];
//            $site_name = $site_res[0]["NAME"];
//
//            $default_email_from = COption::GetOptionString("main", "email_from", "2");
//
//            $arEventFields = array(
//                "SITE_NAME" => $site_name,
//                "FIELDS" => $email_body,
//                "EMAIL" => $server_mail,
//                'DEFAULT_EMAIL_FROM' => $default_email_from,
//                "BCC" => $server_mail
//            );
//
//            CEvent::SendImmediate("SW_VACANCIES", s1, $arEventFields, "Y", 12);

            echo 1;
        } else {
            echo "Error: " . $el->LAST_ERROR;
        }

        exit();
    }

}

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");