<?php

if(strlen($_POST['get'])>0) $get = explode("&",$_POST['get']);

if(!empty($get)) {
    foreach ($get as $getI) {
        $getI = explode("=", $getI);
        $_GET[$getI[0]] = $getI[1];
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if(!CModule::IncludeModule('iblock')) die('error');

CPageOption::SetOptionString("main", "nav_page_in_session", "Y");


$id = $_POST['data']['id'];
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_ANON_NAME", "PREVIEW_TEXT", 'SHOW_COUNTER', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE');
$arFilter = Array("IBLOCK_ID"=>$id, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("date_active_from" => 'desc'), $arFilter, false, Array("nPageSize"=>5), $arSelect);
$total = $res -> SelectedRowsCount();
if($total) {
    ?>
    <ul class="row section-indents-sm">
        <li class="col-md-8 section-indents-sm">
            <ul class="blog-list">
        <?
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            ?>
            <li>
                <ul class="row blog-post">
                    <li class="col-md-4">
                        <div class="blog-post-img-wrap">
                            <?if (!empty($arFields["PREVIEW_PICTURE"])) {?>
                                <?php $file = CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"],
                                    array('width'=>224, 'height' => 158), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
                                <img src="<?=$file['src']?>" alt="<?=$arFields['NAME']?>" title="<?=$arFields['NAME']?>" class="blog-post-img"/>
                            <?} else {?>
                                <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arFields['NAME']?>" title="<?=$arFields['NAME']?>" class="blog-post-img"/>
                            <?}?>
                        </div>
                    </li>
                    <li class="col-md-8">
                        <ul class="d-flex flex-wrap blog-post-publication-list">
                            <li><span class="blog-post-publication-date"><?=date("d.m.Y", strtotime($arFields["DATE_ACTIVE_FROM"]));?></span></li>
                            <li><span class="blog-post-publication-views">
                                        <?
                                        if ($arFields["SHOW_COUNTER"]) {
                                            echo $arFields["SHOW_COUNTER"];}
                                        else {?>0<?} ?>
                                    </span>
                            </li>
                        </ul>
                        <h5 class="blog-post-title"><?=$arFields['NAME']?></h5>
                        <p class="blog-post-text"><?=$arFields['PREVIEW_TEXT']?></p>
                        <div class="d-flex justify-content-end">
                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>">
                                <button class="btn-more_info">Подробнее</button>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <?
        }

        $navStr = $res->GetPageNavStringEx($navComponentObject, "Страницы:", "news_list");
        echo $navStr;
        ?>
            </ul>
        </li>

        <li class="col-md-4 section-indents-sm">
            <div class="sidebar">
                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/includes/video_curse.php"
                    )
                );?>

                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/includes/subscribe.php"
                    )
                );?>
            </div>
        </li>
    </ul>

    <?
}
?>

