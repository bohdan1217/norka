<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<!DOCTYPE html>
<html>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>


    <link rel="shortcut icon" href="/bitrix/templates/norka_new/favicon.ico" type="image/x-icon">
    <link rel="icon" type="image/x-icon"
          href="/bitrix/templates/norka_new/favicon.ico" />


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/styles/css/preloader.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/dist/css/swiper.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/styles/css/style.css"/>
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/src/styles/css/hbmdev.css"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/smoothness/jquery-ui.css" />

    <link href="http://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
    <script src="http://vjs.zencdn.net/6.6.3/video.js"></script>

</head>
<body>
<?$APPLICATION->ShowPanel();?>

<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_TEMPLATE_PATH."/includes/preloader.php"
    )
);?>

<header class="header-page">
    <header class="header-top">
        <div class="container">
            <div class="row align-items-center header-top-nac">
                <li class="col-lg-1 d-flex justify-content-lg-start justify-content-sm-center justify-content-sm-center text-align-logo">
                    <a href="/">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH."/includes/header/logo.php"
                            )
                        );?>
                    </a>
                </li>
                <li class="col-lg-4 expert-li">
                    <a href="/">
                        <span class="expert">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_TEMPLATE_PATH."/includes/header/expert.php"
                                )
                            );?>
                        </span>
                    </a>
                </li>
                <li class="col-lg-7">
                    <ul class="header-contact d-flex flex-wrap justify-content-lg-end justify-content-md-center justify-content-sm-center align-items-center">
                        <li>
                            <?php
                            $phone = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/header/phone.php');
                            $clean = preg_replace("/[^0-9]/", "",$phone);
                            ?>
                            <a href="tel:<?=$clean?>">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH."/includes/header/phone.php"
                                    )
                                );?>
                            </a>
                        </li>
                        <li>
                            <?php
                            $email = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/header/email.php');
                            ?>
                            <a href="mailto:<?=$email?>">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH."/includes/header/email.php"
                                    )
                                );?>
                            </a>
                        </li>
                        <li><a href="/books/"><button class="btn-header">Школа экспертных продаж</button></a></li>
                    </ul>
                </li>
            </div>
        </div>
    </header>
    <footer class="header-bottom">
        <div class="container">
            <?
            $APPLICATION->IncludeComponent("bitrix:menu", "menu", Array(
                "COMPONENT_TEMPLATE" => ".default",
                "ROOT_MENU_TYPE" => "new",	// Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            ),
                false
            );?>
        </div>
    </footer>
</header>



<?if (cur_page == '/') {?>
<section class="main-statistic">
    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => SITE_TEMPLATE_PATH."/includes/header/main_data.php"
            )
        );?>



        <?$APPLICATION->IncludeComponent("bitrix:news.list", "slider", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "FIELD_CODE" => array(	// Поля
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "",	// Фильтр
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "IBLOCK_ID" => "32",	// Код информационного блока
                "IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",	// Количество новостей на странице
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "PROPERTY_CODE" => array(	// Свойства
                    0 => "VIDEO",
                    1 => "",
                ),
                "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
                "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "COMPONENT_TEMPLATE" => "slider"
            ),
            false
        );?>
    </div>
</section>


    <?
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_URL", 'PREVIEW_PICTURE');
    $arFilter = Array("IBLOCK_ID"=>12, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>30), $arSelect);
    $total = $res -> SelectedRowsCount();
    if($total){
        ?>
    <section class="main-slider">
        <div class="container">
            <div class="slider responsive">
                <?
                while($ob = $res->GetNextElement())
                 {
                    $arFields = $ob->GetFields();
                     if (!empty($arFields['PREVIEW_PICTURE'])) {
                        $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>155, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);  ?>
                    <div><a href="<?=$arFields['PROPERTY_URL_VALUE']?>" target="_blank"><img src="<?=$photo['src']?>"></a></div>
                <?}}?>
            </div>
            <div class="d-flex justify-content-center">
                <a href="/dmitry-norca/clients/">
                    <button class="btn-more_right btn-more_right-white"><span><i class="fa fa-play" aria-hidden="true"></i></span>
                        <p class="all_client">Все клиенты</p>
                    </button>
                </a>
            </div>
        </div>
    </section>
<?}?>


    <?
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_DESC", "DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID"=>20, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
    $total = $res -> SelectedRowsCount();
    if($total){
    ?>
    <section class="section-content-training section-bg-grey">
        <div class="container">
            <h2 class="section-title text-center section-indents-mdy1">Программы тренингов</h2>
            <ul class="row main-training-list align-items-stretch section-indents-md3 equalheight2">
                <?
                $key = 0;
                while($ob = $res->GetNextElement())
                {   $key++;
                    $arFields = $ob->GetFields();
                    ?>
                    <li class="col-lg-4 col-md-6 training-other-col">
                        <div class="section-bg-white">
                            <div class="training-other-col-number">
                                <span class="span-main"><?=$key?></span>
                            </div>
                            <ul class="row section-indents-md" style="margin-bottom: 0">
                                <li class="col-md-6 col-sm-6">
                                    <div class="training-other">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/src/img/training/other-training.png" class="d-block" alt="">
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6"></li>
                            </ul>

                            <ul class="row main-content-bottom">
                                <li class="col-md-12">
                                    <div class="main-training-content test">
                                                <span class="dotdotdot">
                                                    <h5 class="main-training-content-title"><?=$arFields['NAME']?></h5>
                                                    <p class="main-training-content-text">
                                                        <?=$arFields['PREVIEW_TEXT']?>
                                                    </p>
                                                </span>
                                        <div class="d-flex justify-content-start section-bg-white section-button">
                                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>">
                                                <button class="btn-services">подробнее</button>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                <?}?>
            </ul>
            <div class="d-flex justify-content-center">
                <a href="/programs/">
                    <button class="btn-more_right">
                        <span style="transform: rotate(90deg);"><i class="fa fa-play" aria-hidden="true"></i></span>все Программы тренингов
                    </button>
                </a>
            </div>
        </div>
    </section>
<?}?>



<?
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CITY", "PROPERTY_CORPORATETRAINING", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO"); //"ID", "NAME", "PREVIEW_TEXT"
    $arFilter = Array("IBLOCK_ID"=>38, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
    $total = $res -> SelectedRowsCount();
    if($total){
        ?>
    <section class="section-content-main-events">
        <div class="container">
            <h2 class="section-title text-center section-title-white section-indents-mdy1">Ближайшие мероприятия</h2>
            <ul class="schedule-list-main">
            <?
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    $date_active_from = date("n", strtotime($arFields["DATE_ACTIVE_FROM"]));
                    $date_active_to = date("n", strtotime($arFields["DATE_ACTIVE_TO"]));
                    ?>
                <li>
                    <ul class="schedule-list-content row align-items-center">
                        <li class="col-md-2 col-xs-2 d-flex justify-content-sm-center date_block">
                            <ul class="schedule-date">
                                <li><span class="d-block text-center schedule-date-day schedule-date-day-main"><?=date("d", strtotime($arFields["DATE_ACTIVE_FROM"]));?> - <?=date("d", strtotime($arFields["DATE_ACTIVE_TO"]));?></span></li>
                                <li><span class="d-block text-center schedule-date-month schedule-date-month-main">
                                        <?=$monthes[$date_active_from]?>
                                        <?if ($date_active_from != $date_active_to){
                                            echo $monthes[$date_active_to];
                                        }?>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-8 col-xs-8">
                            <h4 class="schedule-title schedule-title-main"><?=$arFields['NAME']?></h4>
                            <ul class="d-flex schedule-text">
                                <li>
                                    <span><?=$arFields['PROPERTY_CITY_VALUE']?></span>
                                </li>
                                <li>
                                    <span><?=$arFields['PROPERTY_CORPORATETRAINING_VALUE']?></span>
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-2 btn-right">
                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>">
                                <button class="btn-more_info btn-more_info-white">подробнее</button>
                            </a>
                        </li>
                    </ul>
                </li>
            <?}?>
            </ul>
            <div class="d-flex justify-content-center">
                <a href="/schedule/">
                    <button class="btn-more_right btn-more_right-white"><span style="transform: rotate(90deg);"><i class="fa fa-play" aria-hidden="true"></i></span>Все мероприятия</button>
                </a>
            </div>
        </div>
    </section>
<?}?>



    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"news_list", 
	array(
		"COMPONENT_TEMPLATE" => "news_list",
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "31",
		"NEWS_COUNT" => "3",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "main_news",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	),
	false
);?>


    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => SITE_TEMPLATE_PATH."/includes/header/book-buy.php"
        )
    );?>



    <?
    CModule::IncludeModule('iblock');
    $arSelect = Array(); //"ID", "NAME", "PREVIEW_TEXT"
    $arFilter = Array("IBLOCK_ID"=>25, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array("date_active_from "=>"desc"), $arFilter, false, Array("nPageSize"=>3), $arSelect);
    $total = $res -> SelectedRowsCount();
    if($total){
        ?>
    <section class="section-content-top section-bg-grey">
        <div class="container">
            <h2 class="section-title text-center section-indents-mdz4">Топ советов по продажам</h2>
            <ul class="main-top-list">
                <?
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    ?>
                <li>
                    <ul class="row blog-post">
                        <? if(!empty($arFields['PREVIEW_PICTURE'])) {?>
                        <li class="col-md-3">
                            <div class="blog-post-img-wrap">
                                <?php $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>255, 'height' => 171), BX_RESIZE_IMAGE_EXACT , true);  ?>
                                <img src="<?=$photo['src']?>" class="blog-post-img" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>"/>
                            </div>
                        </li>
                        <li class="col-md-8">
                        <?} else {?>
                        <li class="col-md-11">
                        <?}?>
                            <ul class="d-flex flex-wrap blog-post-publication-list">
                                <li><span class="blog-post-publication-date"><?=$arFields["DATE_ACTIVE_FROM"]?></span></li>
                                <li><span class="blog-post-publication-views"><?=$arFields["SHOW_COUNTER"]?></span></li>
                            </ul>
                            <h5 class="blog-post-title"><?=$arFields["NAME"]?></h5>
                            <p class="blog-post-text"><?=$arFields["PREVIEW_TEXT"]?></p>
                            <div class="d-flex justify-content-end">
                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                    <button class="btn-more_info">Подробнее</button>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <?}?>
            </ul>
        </div>
    </section>
<?}?>


    <?
    /*
    $APPLICATION->IncludeComponent("bitrix:main.include","",Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => SITE_TEMPLATE_PATH."/includes/header/book-free.php"
        )
    );
    */
    ?>
<?}?>