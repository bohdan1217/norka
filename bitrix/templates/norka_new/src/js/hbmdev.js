$(document).ready(function() {
    jQuery(function($) {
        $('.equalheight div').equalHeights();
        $('.equalheight2 .equalheightli').equalHeights();
        $('.equalheightnews').equalHeights();
        // $('.equalheightnewslistmain').equalHeights();
        $('.test').equalHeights();
        $('.test3').equalHeights();
        $('.text_top').equalHeights();
        $('.bottom_eq').equalHeights();
    });
    $(".dotdotdot").dotdotdot({
        ellipsis: "... ",
        height: 350
    }), $("body").on("resize_lg_once resize_md_once resize_sm_once resize_xs", function() {
        $(".dotdotdot").trigger("update")
    });
    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1500);
                    return false;
                }
            }
        });
    });
    $("input[type='file']").change(function() {
        $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
    });
    
    jQuery(function($) {
        $("#phone").mask("(999) 9999999999", {
            autoclear: false
        });
    });

    var nav = $('.nav-container');
    nav.addClass('default');
    $(window).scroll(function() {
        if (Math.max(0, nav.offset().top - $(window).scrollTop()) <= 0 && nav.hasClass("default")) {
            // nav.fadeOut('2000',function(){
            //     $(this).removeClass("default")
            //         .addClass("f-nav")
            //         .fadeIn('2000');
            // });
            nav.removeClass("default")
                .addClass("f-nav")
                .fadeIn('2000');
        } else if(Math.max(0, $('.header-bottom').offset().top - $(window).scrollTop()) > 0) {
            // nav.fadeOut('2000',function(){
            //     $(this).removeClass("f-nav")
            //         .addClass("default")
            //         .fadeIn('2000');
            // });

            nav.removeClass("f-nav")
                .addClass("default")
                .fadeIn('2000');
        }

    });

    $('.modal').on('scroll', function () {
        //hide datepicker
        $("#ui-datepicker-div").hide();
    });

    $('.navbar-toggler').on('click', function () {

            setTimeout(function () {
                if($('.navbar-collapse').is(':visible')){
                    if($(window).scrollTop() < 255) {
                        $('html, body').animate({
                            scrollTop: $('.header-bottom').offset().top
                        }, 800);
                    }
                    $('body').addClass('fixed');
                    console.log($(window).scrollTop());
                } else {
                    $('body').removeClass('fixed');
                }
            }, 500);

    });

    $('.hasDatepicker').on('click', function () {

        $("#ui-datepicker-div").slideToggle();

    });


    // var $menu = $(".nav-container");
    // $(window).scroll(function(){
    //     if ( $(this).scrollTop() > 100 && $menu.hasClass("default") ){
    //         $menu.fadeOut('fast',function(){
    //             $(this).removeClass("default")
    //                 .addClass("f-nav")
    //                 .fadeIn('fast');
    //         });
    //     } else if($(this).scrollTop() <= 100 && $menu.hasClass("fixed")) {
    //         $menu.fadeOut('fast',function(){
    //             $(this).removeClass("f-nav")
    //                 .addClass("default")
    //                 .fadeIn('fast');
    //         });
    //     }
    // });


    $('.responsive').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function(index, className) {
                return '<span class="' + className + '">0' + (index + 1) + '</span>';
            },
        },
    });
    swiper.on('slideChange', function() {
        $('.swiper-slide').removeClass('play-video');
        $('.swiper-pagination, .main-statistic').removeClass('hide-for-play');
        $('video').each(function() {
            $(this)[0].pause();
        });
    });
    $('.btn-main-play').on('click', function() {
        var slideItem = $(this).closest('.swiper-slide');
        var videoElement = slideItem.find('iframe');
        $('.swiper-slide').removeClass('play-video');
        videoElement.attr('src', videoElement.attr('data-src'));
        videoElement[0].src += "?autoplay=1";
        slideItem.addClass('play-video');
        $('.swiper-pagination, .main-statistic').addClass('hide-for-play');
        swiper.sliderOptions = {
            simulateTouch: false,
            allowSwipeToNext: false,
            allowSwipeToPrev: false,
            resistance: false
        };
    });
    $('.close-video').on('click', function() {
        $('.swiper-slide').removeClass('play-video');
        $('.swiper-pagination, .main-statistic').removeClass('hide-for-play');
        $('iframe').each(function() {
            var video = $(this);
            var src = video.attr('src');
            video.attr('src', '');
        });
    });
    $('.b-share-icon').addClass("display-none");
    $('.b-share-btn__vkontakte').append($('<i class="fa fa-vk" aria-hidden="true"></i>'));
    $('.b-share-btn__facebook').append($('<i class="fa fa-facebook" aria-hidden="true"></i>'));
    $('.b-share-btn__twitter').append($('<i class="fa fa-twitter" aria-hidden="true"></i>'));
    $('.b-share-btn__odnoklassniki').append($('<i class="fa fa-odnoklassniki" aria-hidden="true"></i>'));
    $('.comments-video-wrap .vjs-big-play-button').append($('<img src="/bitrix/templates/norka_new/src/img/otzivi/play-button-arrowhead.png" alt="">'));
    $('.video-big-about .video-js .vjs-big-play-button').append($('<img src="/bitrix/templates/norka_new/src/img/otzivi/play-button-arrowhead.png" alt="">'));
    $(document).on('click', '[data-show-more]', function() {
        var btn = $(this);
        var page = btn.attr('data-next-page');
        var id = btn.attr('data-show-more');
        var bx_ajax_id = btn.attr('data-ajax-id');
        var block_id = "#comp_" + bx_ajax_id;
        var data = {
            bxajaxid: bx_ajax_id
        };
        data['PAGEN_' + id] = page;
        $.ajax({
            type: "GET",
            url: window.location.href,
            data: data,
            timeout: 3000,
            success: function(data) {
                $("#btn_" + bx_ajax_id).remove();
                $(block_id).append(data);
            }
        });
    });
    $(function() {
        $('.datep').datepicker({
            minDate: 0,
            maxDate: +90,
            buttonImage: '/bitrix/templates/norka_new/src/img/calendar.png',
            showOn: 'both',
            buttonImageOnly: true
        });
    });
    var maxCountnumber = 25;
    $(".form-comments-name").keyup(function() {
        var revText = this.value.length;
        if (this.value.length > maxCountnumber)
        {
            this.value = this.value.substr(0, maxCountnumber);
        }
        var cnt = (maxCountnumber - revText);
        //console.log(cnt);
        if ((cnt == -1) || (cnt == 0)) {
            if (cnt <= 0) {
                $(".count_name").html('До 25 символов');
            } else {
                $(".count_name").html(cnt);
            }
        } else {
            $(".count_name").html('');
        }
    });
    var maxCountemail = 25;
    $(".form-comments-email").keyup(function() {
        var revText = this.value.length;
        if (this.value.length > maxCountemail)
        {
            this.value = this.value.substr(0, maxCountemail);
        }
        var cnt = (maxCountemail - revText);
        console.log(cnt);
        if (cnt == -1) {
            if (cnt <= 0) {
                $(".count_email").html('До 25 символов');
            } else {
                $(".count_email").html(cnt);
            }
        } else {
            $(".count_email").html('');
        }
    });
    var maxCount = 500;
    $(".counter_number").html(maxCount);
    $(".review-text").keyup(function() {
        var revText = this.value.length;
        if (this.value.length > maxCount)
        {
            this.value = this.value.substr(0, maxCount);
        }
        var cnt = (maxCount - revText);
        if (cnt <= 10) {
            $(".counter_number").addClass('color_red_count');
            $(".counter_reviews").addClass('color_red_count');
        } else {
            $(".counter_number").removeClass('color_red_count');
            $(".counter_reviews").removeClass('color_red_count');
        }
        if (cnt <= 0) {
            $(".counter_number").html('0');
        } else {
            $(".counter_number").html(cnt);
        }
    });
    $(".counter_number_comments").html(maxCount);
    $(".review-text-comments").keyup(function() {
        var revText = this.value.length;
        if (this.value.length > maxCount)
        {
            this.value = this.value.substr(0, maxCount);
        }
        var cnt = (maxCount - revText);
        if (cnt <= 10) {
            $(".counter_number_comments").addClass('color_red_count');
            $(".counter_comments").addClass('color_red_count');
        } else {
            $(".counter_number_comments").removeClass('color_red_count');
            $(".counter_comments").removeClass('color_red_count');
        }
        if (cnt <= 0) {
            $(".counter_number_comments").html('0');
        } else {
            $(".counter_number_comments").html(cnt);
        }
    });
    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value == '' || value.trim().length != 0;
    }, "Рядок пустой");
    jQuery.validator.addMethod("specialChars", function(value, element) {
        var regex = new RegExp("^[a-zA-Zа-яА-Я0-9і\\s]+$");
        var key = value;
        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "Используйте только буквенно-цифровые или буквенные символы");
    //span for books list
    $('.books-structure-nav').each(function() {
        $(this).find('li').each(function() {
            var text = $(this).find('.books-structure-pages li:nth-child(1)').text();
            $(this).find('.books-structure-pages li:nth-child(1)').html('<span>' + text + '</span>');
        });
    });
});


function cislo() {
    if (event.keyCode < 48 || event.keyCode > 57)
        event.returnValue = false;
}
$('body').delegate('.comment_ul_pagination li a', 'click', function() {
    if ($(this).hasClass('active')) return false;
    var val = $(this).attr('href');
    val = val.split('?');
    load_knowledge(val[1]);
    return false;
});

function load_knowledge(get) {
    var id = $('.comments_div').data('id');
    var data = {};
    data['id'] = id;
    $.ajax({
        url: '/bitrix/templates/norka_new/ajax/ajax.php',
        type: 'POST',
        dataType: 'html',
        data: {
            'ajax': 'load_knowledge',
            'data': data,
            'url': location.pathname,
            'get': get
        },
        beforeSend: function() {
            $('#loading').fadeIn('fast');
        },
        success: function(response) {
            //console.info(response);
            $('.comments_div').html(response);
            $('html,body').animate({
                scrollTop: $(".comments_div").offset().top
            }, 1500);
            $('#loading').fadeOut('fast');
        }
    });
}
$('body').delegate('.pagination a', 'click', function() {
    if ($(this).hasClass('active_a')) return false;
    var val = $(this).attr('href');
    val = val.split('?');
    load_knowledge_2(val[1]);
    return false;
});

// var savedScrollPosition;
//
// $(document).on('show.bs.modal', '.modal', function() {
//     savedScrollPosition = $(window).scrollTop();
// });
//
// $(document).on('hidden.bs.modal', '.modal', function() {
//     window.scrollTo(0, savedScrollPosition);
// });



function load_knowledge_2(get) {
    var id = $('.add_response').data('id');
    var data = {};
    data['id'] = id;
    $.ajax({
        url: '/bitrix/templates/norka_new/ajax/ajax_news_pagination.php',
        type: 'POST',
        dataType: 'html',
        data: {
            'ajax': 'load_knowledge',
            'data': data,
            'url': location.pathname,
            'get': get
        },
        beforeSend: function() {
            $('#loading').fadeIn('fast');
        },
        success: function(response) {
            $('.add_response').html(response);
            $('html,body').animate({
                scrollTop: $(".section-nav").offset().top
            }, 1500);
            $('#loading').fadeOut('fast');
        }
    });
}
$(".btn_book_buy").on("click", function() {
    var th = $(this),
        book = th.data("book"),
        modal = $("#book-buy");
    modal.find('input[name="book"]').val(book);
});
$(".add-review").on("click", function() {
    $(".section-content").find("#form").removeClass("display-none");
});
$(".add-coment-center").on("click", function() {
    var th = $(this),
        id = th.data("id");
    if ($(".news-press-center").find("#form_comment_" + id).hasClass("display-none")) {
        $(".news-press-center").find("#form_comment_" + id).removeClass("display-none");
    } else {
        $(".news-press-center").find("#form_comment_" + id).addClass("display-none");
    }
});
if ($('.book_buy').length > 0) {
    $('.book_buy').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $("#book-buy").modal("hide");
                        window.location.href = "/thank/";
                        //window.location.href = "/thank/";
                        // $("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}
$(".btn_service").on("click", function() {
    var th = $(this),
        id = th.data("id"),
        service = th.data("service"),
        modal = $("#service");
    modal.find('input[name="id_service"]').val(id);
    modal.find('input[name="service_name"]').val(service);
});
$('.btn-sidebar-submit').hover(

    function() {
        $(this).toggleClass('')
    }
);
if ($('.service_form').length > 0) {
    $('.service_form').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $("#service").modal("hide");
                        window.location.href = "/thank/";
                        //$("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}

$(document).on('change', '.datep', function() {
    $('.datePInput label').addClass('display-none');
    $('.datep').removeClass('error-input');
});

//if ($('.training_form').length > 0) {
    $('.training_form').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            },
            phone: {
                required: true,
                minlength: 5,
                maxlength: 25
            },
            orientation: {
                required: true,
                minlength: 3,
                maxlength: 22
            },
            position: {
                required: true,
                minlength: 3,
                maxlength: 22
            },
            city: {
                required: true,
                minlength: 3,
                maxlength: 22
            },
            address: {
                required: true,
                minlength: 3,
                maxlength: 22
            },
            date: {
                required: true
            },
            name_company: {
                required: true,
                minlength: 3,
                maxlength: 22
            },
            text: {
                required: true,
                minlength: 3,
                maxlength: 500,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            },
            "phone": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                number: "Введите правильный номер"
            },
            "orientation": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "date": {
                required: "Заполните поле \"Календарь\""
            },
            "position": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "address": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "city": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "name_company": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            }
            ,
            "text": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 600 символов"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $("#training").modal("hide");
                        window.location.href = "/thank/";
                        //$("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
//}


if ($('.question_form').length > 0) {
    $('.question_form').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            },
            question: {
                required: true,
                minlength: 3,
                maxlength: 500,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            },
            "question": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 500 символов"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $('.counter_comment_style_modal').removeClass('color_red_count');
                        $('.counter_comment_style_modal').html('500');
                        $("#question").modal("hide");
                        window.location.href = "/thank/";
                        //$("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}
if ($('.question_form_press_center').length > 0) {
    $('.question_form_press_center').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            },
            question: {
                required: true,
                minlength: 3,
                maxlength: 500,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            },
            "question": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 500 символов"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        window.location.href = "/thank/";
                        //$("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}
$(".btn_comment_id").on("click", function() {
    var th = $(this),
        id = th.data("id");
    $('.comment_form_' + id).validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            },
            comment: {
                required: true,
                minlength: 3,
                maxlength: 500,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            },
            "comment": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 500 символов"
            }
        },
        submitHandler: function(form) {
            //console.log(id);
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $('.counter_number').removeClass('color_red_count');
                        $('.counter_number').html('500');
                        //window.location.href = "/thank/";
                        $("#thank-comment").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
});
$('.comment_form').validate({
    errorClass: 'error-input',
    validClass: 'success-input',
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    rules: {
        name: {
            required: true,
            minlength: 3,
            maxlength: 25,
            noSpace: true,
            specialChars: true
        },
        email: {
            required: true,
            email: true,
            minlength: 3,
            maxlength: 25,
            noSpace: true
        },
        comment: {
            required: true,
            minlength: 3,
            maxlength: 500,
            noSpace: true
        }
    },
    messages: {
        "name": {
            required: "Заполните это поле",
            minlength: "От 3 символов",
            maxlength: "До 25 символов"
        },
        "email": {
            required: "Заполните это поле",
            minlength: "От 3 символов",
            maxlength: "До 25 символов",
            email: "Введите правильный email"
        },
        "comment": {
            required: "Заполните это поле",
            minlength: "От 3 символов",
            maxlength: "До 500 символов"
        }
    },
    submitHandler: function(form) {
        //console.log(id);
        var form = $(form)
        form_result = $('.error_result');
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            async: true,
            data: form.serialize(),
            beforeSend: function() {
                $('#loading').fadeIn('fast');
            },
            success: function(data) {
                if (data == 1) {
                    form_result.html('');
                    $('.counter_comments').removeClass('color_red_count');
                    $('.counter_number_comments').removeClass('color_red_count');
                    $('.counter_number_comments').html('500');
                    //window.location.href = "/thank/";
                    $("#thank-comment").modal("show");
                    form.trigger('reset');
                } else {
                    form_result.html(data);
                    $("#error_modal").modal('show');
                }
                $('#loading').fadeOut('fast');
            },
            error: function() {
                $('#loading').fadeOut('fast');
            }
        });
        return false;
    }
});
if ($('.reviews_form').length > 0) {
    $('.reviews_form').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            },
            comment: {
                required: true,
                minlength: 3,
                maxlength: 500,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            },
            "comment": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 500 символов"
            }
        },
        submitHandler: function(form) {
            // var fileInput = $('#fileUpload');
            // var maxSize = fileInput.data('max-size');
            //
            // if (fileInput) {
            //     var fileSize = fileInput.get(0).files[0].size;
            //     if (fileSize > maxSize) {
            //         alert('Размер файла более 120 мбайт');
            //         return false;
            //     }
            // }

            var form_node = $(form),
                form_result = form_node.find('.form_result');

            $.ajax({
                url: form_node.attr('action'),
                type: form_node.attr('method'),
                async : true,
                data: new FormData(form),
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    console.log(data);
                    if (data == 1) {
                        form_result.html('');
                        $('.counter_comments').removeClass('color_red_count');
                        $('.counter_number_comments').removeClass('color_red_count');
                        $('.counter_number_comments').html('500');
                        $("#val").html('');
                        //window.location.href = "/thank/";
                        $("#thank-reviews").modal("show");
                        form_node.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}
$(".reg").on("click", function() {
    var th = $(this),
        id = th.data("id"),
        name = th.data("name"),
        modal = $("#reg");
    modal.find('input[name="id_m"]').val(id);
    modal.find('input[name="name_m"]').val(name);
});
if ($('.req_form').length > 0) {
    $('.req_form').validate({
        errorClass: 'error-input',
        validClass: 'success-input',
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true,
                specialChars: true
            },
            email: {
                required: true,
                email: true,
                minlength: 3,
                maxlength: 25,
                noSpace: true
            }
        },
        messages: {
            "name": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов"
            },
            "email": {
                required: "Заполните это поле",
                minlength: "От 3 символов",
                maxlength: "До 25 символов",
                email: "Введите правильный email"
            }
        },
        submitHandler: function(form) {
            var form = $(form)
            form_result = $('.error_result');
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                async: true,
                data: form.serialize(),
                beforeSend: function() {
                    $('#loading').fadeIn('fast');
                },
                success: function(data) {
                    if (data == 1) {
                        form_result.html('');
                        $("#reg").modal("hide");
                        window.location.href = "/thank/";
                        //$("#thank-book").modal("show");
                        form.trigger('reset');
                    } else {
                        form_result.html(data);
                        $("#error_modal").modal('show');
                    }
                    $('#loading').fadeOut('fast');
                },
                error: function() {
                    $('#loading').fadeOut('fast');
                }
            });
            return false;
        }
    });
}

$(window).scroll(function(){
    if($(window).scrollTop()>1000){
        $('#new-subs').removeClass('display-none');
        $('#new-subs').addClass('pop',1000);
    } else if ($(window).scrollTop()< 1000) {
        $('#new-subs').removeClass('pop');
        $('#new-subs').addClass('display-none',2000);
    }
});

function remove_pop_up() {
    let date = new Date;
    date.setDate(date.getDate() + 30);
    document.cookie = 'DEFFERED=Y;path=/;expires=' + date.toUTCString();
    $('#new-subs').remove();
    $('#subs-success').remove();
}


$(document).ready(function(){
    $(".section-welcome-otzivi").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),
            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;
        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top-180}, 1500);
    });
    
});