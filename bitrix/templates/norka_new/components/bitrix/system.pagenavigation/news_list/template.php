<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
  if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<ul class="row pagination blog-page-nav section-indents-smd  justify-content-sm-center justify-content-md-start color-black-link a-margin">
<?if ($arResult["NavPageNomer"] > 3) {?>
    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><li><span>1</span></li></a>
<?}?>
<?$page = $arResult["nStartPage"]?>
<?while($page <= $arResult["nEndPage"]) {?>
    <?if ($page == $arResult["NavPageNomer"]) {?>
        <a href="#" class="active_a"><li class="active"><span><?=$page?></span></li></a>
    <?} else {?>
        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$page?>"><li><span><?=$page?></span></li></a>
    <?}?>
    <?$page++?>
<?}?>
<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {?>
    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><li><span><?=$arResult["NavPageCount"]?></span></li></a>
<?}?>
</ul>


<?php return;?>

<ul class="row pagination blog-page-nav section-indents-smd  justify-content-sm-center justify-content-md-start color-black-link a-margin">
    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

      <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
        <a href="#" class="active_a"><li class="active"><span><?=$arResult["nStartPage"]?></span></li></a>
      <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"?><li><span><?=$arResult["nStartPage"]?></span></li></a>
      <?else:?>
        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><li><span><?=$arResult["nStartPage"]?></span></li></a>
      <?endif?>
      <?$arResult["nStartPage"]++?>
    <?endwhile?>
</ul>


<?php return;?>

<?php var_dump($arResult["NavFirstRecordShow"]); ?>  <!-- Now sHown first -->
<?php var_dump($arResult["NavLastRecordShow"]); ?> <!-- Now sHown last -->
<?php var_dump($arResult["NavRecordCount"]); ?> <!-- Total namber -->
<?php var_dump($arResult["NavPageNomer"]); ?> <!-- Cuurent page -->
<?php var_dump($arResult["bSavePage"]); ?> <!-- Save page -->
<?php var_dump($arResult["nStartPage"]); ?> <!-- Save page -->
<?php var_dump($arResult["nEndPage"]);?> <!-- Save page -->
<?php var_dump($arResult["nEndPage"]);?> <!-- END page -->
