<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
  if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="d-flex justify-content-center">
    <ul class="main-nav d-flex comment_ul_pagination">
        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li>
                <a class="active">
                    <button class="btn-nav"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/prev.png"></button>
                </a>
            </li>
        <?else:?>
            <li>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_1=<?=($arResult["NavPageNomer"]-1)?>">
                    <button class="btn-nav"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/prev.png"></button>
                </a>
            </li>
        <?endif;?>
        <li><?=$arResult["NavPageNomer"]?>/<sup><?=$arResult["nEndPage"]?></sup></li>
        <?if ($arResult["nEndPage"] == $arResult["NavPageNomer"]):?>
            <li>
                <a class="active">
                    <button class="btn-nav"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/next.png"></button>
                </a>
            </li>
        <?else:?>
            <li>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_1=<?=($arResult["NavPageNomer"]+1)?>">
                    <button class="btn-nav"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/next.png"></button>
                </a>
            </li>
        <?endif;?>

    </ul>
</div>

<?php return;?>

<?php var_dump($arResult["NavFirstRecordShow"]); ?>  <!-- Now sHown first -->
<?php var_dump($arResult["NavLastRecordShow"]); ?> <!-- Now sHown last -->
<?php var_dump($arResult["NavRecordCount"]); ?> <!-- Total namber -->
<?php var_dump($arResult["NavPageNomer"]); ?> <!-- Cuurent page -->
<?php var_dump($arResult["bSavePage"]); ?> <!-- Save page -->
<?php var_dump($arResult["nStartPage"]); ?> <!-- Save page -->
<?php var_dump($arResult["nEndPage"]);?> <!-- Save page -->
<?php var_dump($arResult["nEndPage"]);?> <!-- END page -->
