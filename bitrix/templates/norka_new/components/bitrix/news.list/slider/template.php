<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="swiper-container swiper-container-horizontal swiper-container-android swiper-container-wp8-horizontal">
    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
        <?foreach($arResult["ITEMS"] as $key_val => $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="swiper-slide swiper-slide-active" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="clearfix"></div>
            <ul class="main-welcome">
                <li>
                    <h4 class="main-welcome-title"><?echo $arItem["NAME"]?></h4>
                    <span class="d-block main-welcome-text"><?echo $arItem["PREVIEW_TEXT"];?></span>
                    <div class="d-flex align-items-center main-play">
                        <button class="btn-main-play"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/play.png" alt="play"></button>
                        <span class="main-welcome-text_2">Смотреть видео</span>
                    </div>
                    <ul class="main-social d-flex slider-a-margin">
                        <?
                        $link = sw_get_option("VIBER");
                        if($link){
                        ?>
                            <a href="viber://chat?number=<?=$link?>"><li><img src="<?=SITE_TEMPLATE_PATH?>/src/img/viber.png"></li></a>
                        <?}?>

                        <?
                        $link = sw_get_option("TELEGRAM");
                        if($link){
                        ?>
                        <a href="tg://resolve?domain=<?=$link?>"><li><img src="<?=SITE_TEMPLATE_PATH?>/src/img/telegram.png"></li></a>
                        <?}?>

                        <?
                        $link = sw_get_option("WHATSAPP");
                        if($link){
                        ?>
                        <a href="whatsapp://send?phone=<?=$link?>"><li><img src="<?=SITE_TEMPLATE_PATH?>/src/img/whatsapp.png"></i></li></a>
                        <?}?>
                    </ul>
                </li>
            </ul>

            <div class="video-wrap">
                <button class="close-video"></button>
                <iframe id="iframe_video_<?=$key_val?>" data-src="https://www.youtube.com/embed/8hgcpxacygU">
                </iframe>
            </div>
        </div>
        <?endforeach;?>
    </div>


    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
        <?foreach($arResult["ITEMS"] as $key => $arItem):?>
            <span class="swiper-pagination-bullet swiper-pagination-bullet-active"><?=$key+1?></span>
        <?endforeach;?>
    </div>
</div>


<?return;?>
<video id="my-video" class="video-js vjs-default-skin vjs-16-9 video" controls preload="auto" width="1110" height="587"
       poster="<?=SITE_TEMPLATE_PATH?>/src/img/about/wrapper.png" data-setup="{}">
    <source src="<?=SITE_DIR?>upload/norca.mp4" type='video/mp4'>
    <source src="MY_VIDEO.webm" type='video/webm'>
    <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to a web browser that
        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
</video>

