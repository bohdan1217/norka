<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name);
?>

<ul class="row comments-list section-indents-mds">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
            <li class="col-md-12 comments-list-nav-otzivi" id="text_<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>">
                <ul class="row">
                    <li class="col-md-2 clients">
                        <div class="comments-photo-wrap comments-photo-wrap-otzivi">
                            <div class="comments-photo-quote"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/comment/quote.png"/></div>
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-image.png" alt="" class="d-block"/>
                        </div>
                    </li>
                    <li class="col-md-10 text-decoration-none">
                        <h5 class="comments-name"><?=$arItem['PROPERTIES']['NAME']['VALUE']?></h5>
                        <?if ($arItem['PROPERTIES']['CITY']['VALUE']) {?>
                            <span class="comments-city d-block">г. <?=$arItem['PROPERTIES']['CITY']['VALUE']?></span>
                        <?}?>
                        <p class="comments-text"><?=$arItem['PREVIEW_TEXT']?></p>


                        <?if ($arItem['PROPERTIES']['VIDEO']['VALUE']){?>
                            <a href="#video_<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>">
                                    <i class="fa fa-video-camera video_reviews" aria-hidden="true"></i>
                                    <span class="comments-text">Смотреть видео отзыв</span>
                            </a>
                        <?}?>
                    </li>
                </ul>
            </li>
    <?endforeach;?>


    <?if($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer<$arResult["NAV_RESULT"]->nEndPage):?>
        <li class="col-md-12" id="btn_<?=$bxajaxid?>">
            <div class="d-flex justify-content-center">
                <a data-ajax-id="<?=$bxajaxid?>" href="javascript:void(0)" data-show-more="<?=$arResult["NAV_RESULT"]->NavNum?>" data-next-page="<?=($arResult["NAV_RESULT"]->NavPageNomer + 1)?>" data-max-page="<?=$arResult["NAV_RESULT"]->nEndPage?>">
                    <button id="btn-slide-down" class="btn-more_right"><span style="transform: rotate(90deg);"><i class="fa fa-play" aria-hidden="true"></i></span>ещё отзывы</button>
                </a>
            </div>
        </li>
    <?endif?>
</ul>