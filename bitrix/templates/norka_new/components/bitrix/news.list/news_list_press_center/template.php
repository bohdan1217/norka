<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="section-title title-line text-center section-indents-mds news-div">Новости</h2>
    <ul class="blog-list section-indents-smd">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <ul class="row blog-post news-press-center">
                    <li class="col-md-3">
                        <div class="blog-post-img-wrap">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/news/news-img-7.png" alt="" class="blog-post-img"/>
                        </div>
                    </li>
                    <li class="col-md-9">
                        <ul class="d-flex flex-wrap blog-post-publication-list">
                            <li><span class="blog-post-publication-date">06.12.2017</span></li>
                            <li><span class="blog-post-publication-views">56</span></li>
                        </ul>
                        <h5 class="blog-post-title"><?=$arItem['NAME']?></h5>
                        <p class="blog-post-text"><?=$arItem['PREVIEW_TEXT']?></p>
                        <div class="d-flex justify-content-end section-indents-smd flex-wrap ">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <button class="btn-more_info btn-add-comment-margin section-indents-smd">Подробнее</button>
                            </a>
                            <a href="#form_comment_<?=$arItem['ID']?>">
                                <button class="btn-more_info section-indents-smd add-coment-center" data-id="<?=$arItem['ID']?>">добавить комментарий</button>
                            </a>
                        </div>
                    </li>

                    <li class="col-md-12 form-press display-none press-center-comments" id="form_comment_<?=$arItem['ID']?>">
                        <div>
                            <h4 class="form-press-title">Добавить комментарий</h4>
                            <form action="<?=SITE_TEMPLATE_PATH?>/ajax/comment.php" method="post" class="comment_form_<?=$arItem['ID']?>">
                                <ul class="row">
                                    <input type="hidden" name="id_news" value="<?echo $arItem["ID"];?>">
                                    <input type="hidden" name="name_news" value="<?echo $arItem["NAME"];?>">
                                    <li class="col-md-4">
                                        <ul>
                                            <li class="form-comments-name-wrap"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png"><input type="text" placeholder="Ваше имя" class="form-comments-name d-block" name="name" required/></li>
                                            <li class="form-comments-email-wrap"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png"></i><input type="email" placeholder="Ваш Е-mail" class="form-comments-email d-block" name="email" required></li>
                                        </ul>
                                    </li>
                                    <li class="col-md-8">
                                        <ul>
                                            <li class="section-indents-md form-comments-comment-wrap" style="margin-bottom: 15px;    padding-bottom: 10px;"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/pen.png"><textarea placeholder="Напишите комментарий" class="form-comments-comment d-block review-text" name="comment" rows="4"></textarea></li>
                                            <li class="counter">Осталось символов: <span class="counter_number"></span></li>
                                            <li>
                                                <div class="d-flex justify-content-end">
                                                    <button class="btn-submit btn_comment_id" data-id="<?echo $arItem["ID"];?>">отправить</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        <?endforeach;?>
    </ul>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <ul class="row blog-page-nav section-indents-smd justify-content-center">
            <?=$arResult["NAV_STRING"]?>
    </ul>
<?endif;?>


