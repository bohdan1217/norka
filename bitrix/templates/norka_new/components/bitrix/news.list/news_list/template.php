<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



    <section class="section-content-main-news section-bg-grey">
        <div class="container">
            <h2 class="section-title text-center section-indents-mds5">Новости</h2>
            <ul class="row main-list">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                    <li class="col-md-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <?if (!empty($arItem['PREVIEW_PICTURE'])) {?>
                            <?php $photo = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>350, 'height' => 234), BX_RESIZE_IMAGE_EXACT , true);  ?>
                            <div class="main-wrap section-indents-smd6">
                                <img src="<?=$photo['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="d-flex w-100"/>
                            </div>
                        <?} else {?>
                            <div class="main-wrap section-indents-smd6">
                                <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="d-flex w-100" style="height: 234px"/>
                            </div>
                        <?}?>

                        <ul>
                            <li>
                                <ul class="d-flex main-news-list">
                                    <li class="main-news-date"><img class="clock" src="<?=SITE_TEMPLATE_PATH?>/src/img/clock.png" alt="clock"><?=date("d.m.Y", strtotime($arItem["DATE_ACTIVE_FROM"]));?></li>
                                    <li class="main-news-views"><img class="glases" src="<?=SITE_TEMPLATE_PATH?>/src/img/glases.png" alt="glases">
                                        <?if ($arItem["SHOW_COUNTER"]) {
                                            echo $arItem["SHOW_COUNTER"];
                                        } else{?>0<?}?>
                                    </li>
                                </ul>
                            </li>
                            <li class="main-news-title "><?=$arItem["NAME"]?></li>
                            <li class="main-news-text equalheightnewslistmain"><?=$arItem["PREVIEW_TEXT"]?></li>
                            <li>
                                <div class="d-flex justify-content-sm-center justify-content-md-start main-w">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                        <button class="btn-more_info">подробнее</button>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?endforeach;?>
            </ul>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                <?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </div>
    </section>


