<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>

<h2 class="section-title title-line text-center section-indents-smb press-center-title">Пресс-кит</h2>
<span class="d-block text-center press-center-title-after section-indents-mdl">ФОТО ДЛЯ ОРГАНИЗАЦИИ ТРЕНИНГА</span>
<ul class="section-indents-mdl8 row press-center-list justify-content-center align-items-stretch">

    <?php foreach( $arItem["PROPERTIES"]["PHOTOS"]["VALUE"] as $key =>  $photoId ): ?>
        <?php
        $newImage = CFile::ResizeImageGet(  $photoId  , array( 'width'=>510, "height" => 329 ), BX_RESIZE_IMAGE_PROPORTIONAL , true);
        ?>
        <li class="<?if (($key == 0 ) || ($key == 4 )) :?>col-md-6<?else:?>col-md-3<?endif;?>">
            <img src="<?=$newImage["src"]?>" alt="" class="d-block press-center-list-img"/>
        </li>
    <?php endforeach; ?>
</ul>


<div class="d-flex justify-content-center section-indents-mdz">
    <a href="<?=CFile::GetPath( $arItem["PROPERTIES"]["ZIP_ARCHIVE"]["VALUE"] );?>">
        <button class="btn-download">СКАЧАТЬ АРХИВ<i class="fa fa-download" aria-hidden="true"></i></button>
    </a>
</div>

<?endforeach;?>

