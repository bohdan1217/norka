<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="add_response" data-id="<?=$arResult['ID']?>">
    <ul class="row section-indents-sm">
        <li class="col-md-8 section-indents-sm">
            <ul class="blog-list">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <ul class="row blog-post">
                        <li class="col-md-4">
                            <div class="blog-post-img-wrap">
                                <?if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) {?>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" class="blog-post-img"/>
                                <?} else {?>
                                <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" class="blog-post-img"/>
                                <?}?>
                            </div>
                        </li>
                        <li class="col-md-8">
                            <ul class="d-flex flex-wrap blog-post-publication-list">
                                <li><span class="blog-post-publication-date"><?=date("d.m.Y", strtotime($arItem["DATE_ACTIVE_FROM"]));?></span></li>
                                <li><span class="blog-post-publication-views">
                                        <?
                                        if ($arItem["SHOW_COUNTER"]) {
                                           echo $arItem["SHOW_COUNTER"];}
                                           else {?>0<?} ?>
                                    </span>
                                </li>
                            </ul>
                            <h5 class="blog-post-title"><?=$arItem['NAME']?></h5>
                            <div class="blog-post-text"><?=$arItem['PREVIEW_TEXT']?></div>
                            <div class="d-flex justify-content-end">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <button class="btn-more_info">Подробнее</button>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            <?endforeach;?>
            </ul>
            <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                <?=$arResult["NAV_STRING"]?>
            <?endif;?>
        </li>



        <li class="col-md-4 section-indents-sm">
            <div class="sidebar">
                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/includes/video_curse.php"
                    )
                );?>

                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH."/includes/subscribe.php"
                    )
                );?>
            </div>
        </li>
    </ul>
</div>