<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<h2 class="events-section-title">Информация</h2>
<ul class="row align-items-start">
    <?
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", 'DETAIL_PAGE_URL', 'DATE_ACTIVE_FROM');
    $arFilter = Array("IBLOCK_ID"=>38,"ACTIVE"=>"Y", "!ID"=>$arResult['ID']); //"ID"=>array(78,79,80,81,82,83,84,85)
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>4), $arSelect);
    $total = $res -> SelectedRowsCount();
    if($total){
    ?>
            <li class="col-md-8 section-indents-smd" style="padding-right: 30px;">
        <?} else {?>
            <li class="col-md-12 section-indents-smd" style="padding-right: 30px;">
        <?}?>


        <?if (!empty($arResult["PROPERTIES"]["DESC"]["~VALUE"]['TEXT'])) {?>
            <?=$arResult["PROPERTIES"]["DESC"]["~VALUE"]['TEXT']?>
        <?}?>

        <?if (!empty($arResult["PROPERTIES"]["BENEFITS"]["VALUE"])) {?>
        <h4 class="events-title section-indents-smd">Преимущества</h4>
        <ul class="events-text section-indents-smd">
         <?
         foreach ($arResult["PROPERTIES"]["BENEFITS"]["VALUE"] as $key => $value):?>
            <li>
                <ul class="d-flex">
                    <li>
                        <div class="events-text-index-number"><span class="events-text-index"><?=$key+1?></span></div>
                    </li>
                    <li>
                        <p><?=$arResult["PROPERTIES"]["BENEFITS"]["VALUE"][$key]['TEXT']?></p>
                    </li>
                </ul>
            </li>
            <?endforeach;?>
        </ul>
        <?}?>

        <h4 class="events-title section-indents-smd">Подробнее</h4>
        <div class="events-amply">
            <ul class="section-bg-white d-flex flex-wrap events-amply-list">
                <li class="col-md-6">
                    <ul class="d-flex flex-wrap">
                        <li class="events-amply-first events-amply-first-date">Дата:</li>
                        <li class="events-amply-second"><?=date("d.m.Y", strtotime($arResult["DATE_ACTIVE_FROM"]));?></li>
                    </ul>
                </li>

                <?if (!empty($arResult["PROPERTIES"]["EVENT"]["VALUE"])) {?>
                <li class="col-md-6">
                    <ul class="d-flex flex-wrap">
                        <li class="events-amply-first events-amply-first-event">Мероприятие:</li>
                        <li class="events-amply-second"><?=$arResult["PROPERTIES"]["EVENT"]["~VALUE"]?></li>
                    </ul>
                </li>
                <?}?>

                <?if (!empty($arResult["PROPERTIES"]["PRICE"]["VALUE"])) {?>
                <li class="col-md-6">
                    <ul class="d-flex flex-wrap">
                        <li class="events-amply-first events-amply-first-price">Цена:</li>
                        <li class="events-amply-second"><?=$arResult["PROPERTIES"]["PRICE"]["~VALUE"]?></li>
                    </ul>
                </li>
                <?}?>

                <?if (!empty($arResult["PROPERTIES"]["LINK"]["VALUE"])) {?>
                    <li class="col-md-6">
                        <ul class="d-flex flex-wrap">
                            <li class="events-amply-first events-amply-first events-amply-first-web">Сайт:</li>
                            <li class="events-amply-second"><a href="<?=$arResult["PROPERTIES"]["LINK"]["VALUE"]?>" target="_blank" class="events-amply-second">Посетить сайт мероприятия</a></li>
                        </ul>
                    </li>
                <?}?>


            </ul>
            <button class="btn-events reg reg-color-black" type="button" data-toggle="modal" data-target="#reg" data-id="<?=$arResult['ID']?>" data-name="<?=$arResult['NAME']?>">регистрация</button>
        </div>

        <?if (!empty($arResult["PROPERTIES"]["FULL_ADDRESS"]["~VALUE"])) {?>
        <h4 class="events-title section-indents-smd">Место проведения</h4>
        <ul class="events-amply-place-list">
            <li>
                <ul>
<!--                    <li class="events-amply-place-list-title">Концертный зал «Премио»</li>-->
                    <li class="events-amply-place-list-text">
                        <?=$arResult["PROPERTIES"]["FULL_ADDRESS"]["~VALUE"]['TEXT']?>
                    </li>
                </ul>
            </li>
            <li class="events-amply-place-list-title"><?=$arResult["PROPERTIES"]["PHONE"]["~VALUE"]?></li>
        </ul>

        <div class="events-wrap section-indents-md3">
            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/events/map.png" class="d-block w-100"/>
        </div>
        <?}?>

        <?if (!empty($arResult['PROPERTIES']['SCHEDULE']['VALUE'])) {?>
            <h4 class="events-title section-indents-md3">Расписание</h4>
            <ul class="row align-items-stretch ul-events-list">
                <li class="col-md-5 col-sm-12">
                    <div class="events-pencil-wrap">
                        <img src="<?=SITE_TEMPLATE_PATH?>/src/img/events/pancel.png" alt="" class="d-block w-100"/>
                    </div>
                </li>
                <li class="col-md-7 section-indents-smd col-sm-12">
                    <ul class="events-schedule-list">
                        <?foreach ($arResult['PROPERTIES']['SCHEDULE']['VALUE'] as  $key => $shedule ):?>
                            <li>
                                <ul class="d-flex">
                                    <li>
                                        <div class="events-schedule-circle"></div>
                                    </li>
                                    <li class="events-schedule-list-time"><?=$arResult['PROPERTIES']['SCHEDULE']['DESCRIPTION'][$key]?></li>
                                    <li class="events-schedule-list-action"><?=$shedule;?></li>
                                </ul>
                            </li>
                        <?endforeach;?>
                    </ul>
                </li>
            </ul>
        <?}?>
    </li>


    <?
    if($total){?>
    <li class="col-md-4 section-bg-white news-list-wrap section-indents-smd">
        <h3 class="news-list-title news-list-title-events">Будущие события</h3>
        <ul>
            <?
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                ?>
                <li class="news-post">
                    <div class="d-flex">
                        <?if (!empty($arFields['PREVIEW_PICTURE'])) {?>
                            <?php $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>110, 'height' => 83), BX_RESIZE_IMAGE_EXACT , true);  ?>
                            <div class="news-img-wrap">
                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                    <img src="<?=$photo['src']?>" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>"  class="news-img"/>
                                </a>
                            </div>
                        <?} else {?>
                            <div class="news-img-wrap">
                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>" class="news-img" style="height: 83px"/>
                                </a>
                            </div>
                        <?}?>
                        <div class="news-info">
                            <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                <h4 class="news-info-title"><?=$arFields["NAME"]?></h4>
                            </a>
                            <span class="d-block news-info-date"><?=date("d.m.Y", strtotime($arFields["DATE_ACTIVE_FROM"]));?></span>
                        </div>
                    </div>
                </li>
            <?}?>
        </ul>
     </li>
    <?}?>
</ul>