<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="shedule-h2 section-title title-line text-center section-indents-mds">Ближайшие Мероприятия</h2>
<ul class="schedule-list section-indents-mdl4">
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <ul class="schedule-list-content row align-items-space-between">
            <li class="col-md-2 d-flex justify-content-sm-center date_block">
                <ul class="schedule-date">
                    <li><span class="d-block text-center schedule-date-day"><?=date("d", strtotime($arItem["DATE_ACTIVE_FROM"]));?> - <?=date("d", strtotime($arItem["DATE_ACTIVE_TO"]));?></span></li>
                    <?
                    $monthes = array(
                    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                    );
                    ?>
                    <li><span class="d-block text-center schedule-date-month"><?=$monthes[date("n", strtotime($arItem["DATE_ACTIVE_FROM"]))]?></span></li>
                </ul>
            </li>
            <li class="col-md-8">
                <h4 class="schedule-title"><?echo $arItem["NAME"]?></h4>
                <ul class="d-flex schedule-text">
                    <li style="color: #2b2928;">
                        <span><?=$arItem["PROPERTIES"]['CITY']['VALUE']?></span>
                    </li>
                    <li style="color: #2b2928;">
                        <span><?=$arItem["PROPERTIES"]['CORPORATETRAINING']['VALUE']?></span>
                    </li>
                </ul>
            </li>
            <li class="col-md-2 schedule-nb">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <button class="btn-more_info">подробнее</button>
                </a>
            </li>
        </ul>
    </li>
<?endforeach;?>
</ul>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<!--    <div class="d-flex justify-content-center">-->
<!--        <button class="btn-more_right"><span><i class="fa fa-play" aria-hidden="true"></i></span>Посмотреть все мероприятия</button>-->
<!--    </div>-->
<?endif;?>