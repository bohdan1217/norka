<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
        <?if (!empty($arResult["PROPERTIES"]['VIDEO']['VALUE'])) {?>
            <div class="video-about video-big-about section-indents-mds">
                <video id="my-video" class="video-js vjs-default-skin vjs-16-9" controls preload="auto" width="1110" height="587"
                       poster="<?=SITE_TEMPLATE_PATH?>/src/img/about/wrapper.png" data-setup="{}">
                    <source src="<?=CFile::GetPath($arResult["PROPERTIES"]['VIDEO']['VALUE']);?>" type='video/mp4'>
                    <source src="MY_VIDEO.webm" type='video/webm'>
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
            </div>
        <?}?>

        <?if ($arResult["PROPERTIES"]['NAMEIN']['~VALUE']['TEXT']) {?>
            <h2 class="section-title-uslugi text-center title-line text-center section-indents-mdy">
                <?=$arResult["PROPERTIES"]['NAMEIN']['~VALUE']['TEXT']?>
            </h2>
        <?}?>

        <ul class="uslugi-list">



            <li>
                <div class="uslugi-list-number"><span class="one">1</span></div>
                <div class="uslugi-list-content">
                    <h4 class="uslugi-list-title">Увеличение среднего размера сделки</h4>
                    <p class="uslugi-list-text">Консультанты использующие экспертный подход работают эффективней их более традиционных коллег на 200% сделать умное, продуманное решение, которое в первую очередь соответствует их интересам, положительные результаты не заставят себя ждать.</p>
                    <span class="uslugi-list-resulted">Результат: Высокий оборот не заставят себя ждать</span>
                </div>
            </li>


        </ul>


        <?
        CModule::IncludeModule('iblock');
        $arSelect = Array("ID", "NAME", 'DETAIL_PAGE_URL'); //"ID", "NAME", "PREVIEW_TEXT"
        $arFilter = Array("IBLOCK_ID"=>35, "ACTIVE"=>"Y", '!ID'=>$arResult['ID']);
        $res = CIBlockElement::GetList(Array("date_active_from "=>"desc"), $arFilter, false, Array("nPageSize"=>3), $arSelect);
        $total = $res -> SelectedRowsCount();
        if($total){
            ?>
            <h2 class="section-title title-line text-center section-indents-lgm">Другие услуги</h2>
            <ul class="training-other-list row">
                <?
                $key = 0;
                while($ob = $res->GetNextElement())
                {
                    $key++;
                    $arFields = $ob->GetFields();
                    ?>
                    <li class="col-lg-4 col-md-6 training-other-col">
                        <div class="section-bg-white">
                            <div class="training-other-col-number">
                                <span><?=$key?></span>
                            </div>
                            <ul class="row section-indents-md">
                                <li class="col-md-6 col-sm-6">
                                    <div class="training-other">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/src/img/training/other-training.png" class="d-block" alt="">
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6"></li>
                            </ul>
                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>"><p class="equalheightnews"><?=$arFields['NAME']?></p></a>
                        </div>
                    </li>
                <?}?>
            </ul>
        <?}?>
<?return;?>
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>