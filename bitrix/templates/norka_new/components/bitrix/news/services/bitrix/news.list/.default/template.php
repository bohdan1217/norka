<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="services-list">
    <?foreach($arResult["ITEMS"] as $key => $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
        <li class="section-bg-white" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <ul class="row align-items-start">
                <li class="col-sm-3 col-lg-2">
                    <div class="services-wrap section-bg-grey">
                        <div class="services-block">
                            <span><?=$key+1?></span>
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/uslugi/img-1.png" alt=""/>
                        </div>
                    </div>
                </li>
                <li class="col-sm-9 col-lg-10 services-content">
                    <h4 class="services-title"><?=$arItem['NAME']?></h4>
                    <p class="services-text"><?=$arItem['PREVIEW_TEXT']?></p>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <button class="btn-services" type="button">Подробнее</button>
                    </a>
                </li>
            </ul>
        </li>
    <?endforeach;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
</ul>