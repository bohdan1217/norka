<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="row">
    <li class="col-md-8 section-indents-smd">
        <div class="newss-post-wrap">
            <?echo $arResult["DETAIL_TEXT"];?>
            <div class="newss-social-grey section-indents-md8">
                <h5 class="newss-social-title-grey">Поделиться в социальных сетях</h5>
                <ul class="d-flex flex-wrap newss-social-list-grey">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:sp-artgroup.share",
                        "header",
                        array(
                            "COMPONENT_TEMPLATE" => "header",
                            "SERVICES" => array(
                                0 => "vkontakte",
                                1 => "facebook",
                                2 => "twitter",
                                3 => "odnoklassniki",
                            ),
                            "SHOW" => "none",
                            "HOW_INC" => "local"
                        ),
                        false
                    );?>

                </ul>
            </div>


            <div class="comments_div" data-id="<?=$arResult['ID']?>">
                <?
                CModule::IncludeModule('iblock');
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_ANON_NAME", "PREVIEW_TEXT");
                $arFilter = Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y", 'PROPERTY_GROUP_ID' => $arResult['ID']);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>3), $arSelect);
                $total = $res -> SelectedRowsCount();
                if($total) {
                    ?>
                    <h4 class="newss-post-title section-indents-smd4">Комментарии</h4>
                    <ul class="row comments-list comments-list-grey section-indents-mdl4">
                        <?
                        while ($ob = $res->GetNextElement()) {
                            $arFields = $ob->GetFields();
                            ?>
                            <li class="col-md-12 comments-list-nav-news">
                                <ul class="row">
                                    <li class="col-md-2 clients">
                                        <div class="comments-photo-wrap comments-photo-wrap-news">
                                            <div class="comments-photo-quote-news">
                                                <img src="<?= SITE_TEMPLATE_PATH ?>/src/img/comment/quote.png"/></div>
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/src/img/no-image.png" alt=""
                                                 class="d-block"/>
                                        </div>
                                    </li>
                                    <li class="col-md-10">
                                        <h5 class="comments-name"><?= $arFields ["PROPERTY_ANON_NAME_VALUE"] ?></h5>
                                        <p class="comments-text"><?= $arFields['PREVIEW_TEXT'] ?></p>
                                    </li>
                                </ul>
                            </li>
                        <?
                        } ?>
                    </ul>
                    <?
                    $navStr = $res->GetPageNavStringEx($navComponentObject, "Страницы:", "comment_in_element");
                    echo $navStr;
                }
                ?>
            </div>


            <div id="form">
                <h4 class="newss-post-title section-indents-am5">Добавить комментарий</h4>
                <form action="<?=SITE_TEMPLATE_PATH?>/ajax/comment.php" method="post" class="form-comments comment_form">
                    <ul class="row">

                        <input type="hidden" name="id_news" value="<?echo $arResult["ID"];?>">
                        <input type="hidden" name="name_news" value="<?echo $arResult["NAME"];?>">

                        <li class="col-md-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" placeholder="Ваше имя" name="name" maxlength="25" class="form-comments-name d-block" required/>
                        </li>
                        <li class="col-md-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" maxlength="25" class="form-comments-email d-block" required>
                        </li>
                        <li class="col-md-12 form-comments-comment-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/pen.png">
                            <textarea placeholder="Напишите комментарий" name="comment" maxlength="500" class="form-comments-comment d-block review-text-comments" rows="5"></textarea>
                        </li>
                        <li class="counter_comments">Осталось символов: <span class="counter_number_comments">500</span></li>
                        <li class="col-md-12 justify-content-sm-center">
                            <button class="btn-submit">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </li>


    <li class="col-md-4 section-indents-smd">
        <div class="sidebar">
            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/video_curse.php"
                )
            );?>

            <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_TEMPLATE_PATH."/includes/subscribe.php"
                )
            );?>
        </div>
    </li>
</ul>




