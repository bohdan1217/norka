<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="books-list">
    <li>
        <ul class="row books-nav">
            <li class="col-md-5 section-bg-light-grey books-col">
                <?php $file = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE']['ID'], array('width'=>310, 'height' => 390), BX_RESIZE_IMAGE_PROPORTIONAL, true);  ?>
                <img src="<?=$file["src"]?>" alt="<?echo $arResult["NAME"]?>" title="<?echo $arResult["NAME"]?>"class="d-block books-img" />
            </li>
            <li class="col-md-7 books-col-right">
                <h4 class="books-title"><?echo $arResult["NAME"]?></h4>
<!--                <span class="books-title-after">Почему они не хотят покупать так, как вы им продаете, и как нужно продавать, чтобы они покупали</span>-->
                <div class="books-text-block">
                    <?echo $arResult["DETAIL_TEXT"];?>
                </div>
                <button class="btn-books-buy section-indents-mdz btn_book_buy" type="button" data-toggle="modal" data-target="#book-buy" data-book="<?echo $arResult["NAME"]?>">купить сейчас</button>
                <? if (!empty($arResult["PROPERTIES"]["CONTENT"]['~VALUE']['TEXT'])) {?>
                <div class="books-structure">
                    <h3 class="books-structure-title-main">Cодержание</h3>
                    <?=$arResult["PROPERTIES"]["CONTENT"]['~VALUE']['TEXT']?>
                </div>
            <?}?>
            </li>
        </ul>
    </li>
</ul>