<?

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)

    die();

?>

<?if (cur_page != '/thank/') {?>

<section class="subscribe-section">
	<div class="photo">
		<img src="<?=SITE_TEMPLATE_PATH?>/src/img/norkag.png" alt="some text" />
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				
			</div>
			<div class="col-lg-7">
				<div class="text">
					<p><strong>А я бы с радостью писал вам письма!</strong></p>
					<p>Только вот адреса не знаю...</p>
					<p>Я могу высылать Вам:</p>
					<ul>
						<li>1. Советы по продажам</li>
						<li>2. Учебные видеоуроки</li>
						<li>3. Информацию о мероприятиях и тренингах</li>
					</ul>
					<form class="subscribe-form">
						<input type="email" />
						<button type="submit">Подписаться</button>
					</form>
					<p>А ещё, я подарю Вам электронную мини книгу <i>"Как довести профессиональный успех до автоматизма"</i></p>
				</div>
			</div>
		</div>
	</div>
</section>



<footer class="footer-page">

    <div class="footer-content">

        <div class="container">

            <ul class="row footer-column">

                <li class="col-lg-4 col-md-6 footer-col">

                    <a href="/">

                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                "AREA_FILE_SHOW" => "file",

                                "AREA_FILE_SUFFIX" => "inc",

                                "EDIT_TEMPLATE" => "",

                                "PATH" => SITE_TEMPLATE_PATH."/includes/footer/logo.php"

                            )

                        );?>

                    </a>

                    <h4 class="footer-title text-center">

                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                "AREA_FILE_SHOW" => "file",

                                "AREA_FILE_SUFFIX" => "inc",

                                "EDIT_TEMPLATE" => "",

                                "PATH" => SITE_TEMPLATE_PATH."/includes/footer/name.php"

                            )

                        );?>

                    </h4>

                    <p class="footer-text">

                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                "AREA_FILE_SHOW" => "file",

                                "AREA_FILE_SUFFIX" => "inc",

                                "EDIT_TEMPLATE" => "",

                                "PATH" => SITE_TEMPLATE_PATH."/includes/footer/text.php"

                            )

                        );?>

                    </p>

                    <div class="d-flex justify-content-center">
                        <img src="<?=SITE_TEMPLATE_PATH?>/src/img/footer-norka.png" class="d-flex" alt="">
                    </div>

                </li>

                <li class="col-lg-4 col-md-6 footer-border">

                    <div class="d-flex flex-column justify-content-between align-items-center h-100">

                        <div>

                            <ul class="d-flex justify-content-center footer-new-social">

                                <a href="http://twitter.com/norca_ru" target="_blank"><li><i class="fa fa-twitter" target="_blank" aria-hidden="true"></i></li></a>

                                <a href="http://ru-ru.facebook.com/people/Dmitriy-Norka/1211541062" target="_blank"><li><i class="fa fa-facebook-f" target="_blank" aria-hidden="true"></i></li></a>

                                <a href="https://www.instagram.com/dnorka/" target="_blank"><li><i class="fa fa-instagram" target="_blank" aria-hidden="true"></i></li></a>

                                <a href="http://vkontakte.ru/id19381151?40138" target="_blank"><li><i class="fa fa-vk" aria-hidden="true"></i></li></a>

                                <a href="https://www.youtube.com/channel/UCX_jZ2R3rjwUg9cp7ScAUpw" target="_blank"><li><i class="fa fa-youtube-play" aria-hidden="true"></i></li></a>





                            </ul>

                            <ul class="footer-contact">

                                <li>

                                    <?php

                                    $email = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/footer/email.php');

                                    ?>

                                    <a href="mailto:<?=$email?>">email:

                                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                                "AREA_FILE_SHOW" => "file",

                                                "AREA_FILE_SUFFIX" => "inc",

                                                "EDIT_TEMPLATE" => "",

                                                "PATH" => SITE_TEMPLATE_PATH."/includes/header/email.php"

                                            )

                                        );?>

                                    </a>

                                </li>





                                <li>

                                    <?php

                                    $phone1 = file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/includes/footer/phone.php');

                                    $clean = preg_replace("/[^0-9]/", "",$phone1);

                                    ?>

                                    <a href="tel:<?=$clean?>">

                                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                                "AREA_FILE_SHOW" => "file",

                                                "AREA_FILE_SUFFIX" => "inc",

                                                "EDIT_TEMPLATE" => "",

                                                "PATH" => SITE_TEMPLATE_PATH."/includes/footer/phone.php"

                                            )

                                        );?>

                                    </a>

                                </li>
                                <li class="sub_t">
                                    <a href="http://forms.sendpulse.com/7f6a09e9e4/?_ga=1.110586950.296049080.1479981059" target="_blank">
                                        <span>Подписаться на рассылку</span>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        <div class="footer-copyright">

                            <ul class="row flex-column align-items-center sub">
                                <li><a href="/">Политика конфиденциальности</a></li>
                                <li class="footer-copyright-bold">Дмитрий Норка ©</li>
                            </ul>

                        </div>

                    </div>

                </li>

                <li class="col-lg-4 col-md-6">

                    <h4 class="footer-title text-center">Мои проекты</h4>

                    <ul class="footer-projects">

                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

                                "AREA_FILE_SHOW" => "file",

                                "AREA_FILE_SUFFIX" => "inc",

                                "EDIT_TEMPLATE" => "",

                                "PATH" => SITE_TEMPLATE_PATH."/includes/footer/projects.php"

                            )

                        );?>

                    </ul>

                </li>

            </ul>

        </div>

    </div>

</footer>

<?}?>


<div id="new-subs" class="display-none">
    <span class="off" onclick="remove_pop_up()"></span>
    <span class="img"><span class="img-inner"><img src="<?=SITE_TEMPLATE_PATH?>/src/img/norkag.png"></span></span>
    <span class="slogan">А я бы с радостью писал вам письма, <span>если оставите e-mail:</span></span>
    <form action="/bitrix/templates/.default/ajax/subscribe.php" onsubmit="return _subsPop.check(this)">
        <input type="hidden" name="template" value="basic">
        <input type="email" name="email" required="" pattern="([a-zA-Z0-9._%+-])+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}" placeholder="ваш e-mail">
        <button type="submit">Подписаться</button>
    </form>
</div>

<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(

        "AREA_FILE_SHOW" => "file",

        "AREA_FILE_SUFFIX" => "inc",

        "EDIT_TEMPLATE" => "",

        "PATH" => SITE_TEMPLATE_PATH."/includes/modal.php"

    )

);?>





<script src="<?=SITE_TEMPLATE_PATH?>/node_modules/jquery/dist/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/node_modules/bootstrap/dist/js/bootstrap.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/node_modules/slick-carousel/slick/slick.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/main.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/jquery.validate.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/dist/js/swiper.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/jquery.dotdotdot.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/maskedinput.js"></script>

<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>





<script src="<?=SITE_TEMPLATE_PATH?>/src/js/datepicker-ru.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/hbmdev.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/src/js/jquery.equalheights.min.js"></script>







<script>

    // $( document ).ready(function() {

    //     $("body").click();

    // });

</script>

</body>

</html>



