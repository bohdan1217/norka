<ul class="main-statistic-list">
    <?
    $key = 0;
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "PROPERTY_NUMBER", "PROPERTY_DESC"); //"ID", "NAME", "PREVIEW_TEXT"
    $arFilter = Array("IBLOCK_ID"=>33, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $key++;
        ?>
        <li class="col">
            <h5 class="main-statistic-title w-100 text-center <?if ($key == 5):?>main-statistic-title_2<?endif;?>"><?=$arFields['PROPERTY_NUMBER_VALUE']?></h5>
            <span class="d-block main-statistic-text text-center <?if ($key != 1):?>text-left_<?=$key?><?endif;?> <?if ($key != 4):?>main-statistic-text_2<?endif;?>"><?=$arFields['~PROPERTY_DESC_VALUE']['TEXT']?></span>
        </li>
    <?}?>
</ul>