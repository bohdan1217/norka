<section class="section-content-main">
    <div class="container">
        <ul class="row align-items-center">
            <li class="col-md-3 col-sm-6">
                <div class="main-wrap">
                    <img src="<?=SITE_TEMPLATE_PATH?>/src/img/index/book-2.png" alt=""/>
                </div>
            </li>
            <li class="col-md-9">
                <h4 class="main-books-title">Прочитайте книгу “Как довести успех в продажах до автоматизма”</h4>
                <span class="d-block main-books-text">Вы сможете очень быстро получить устойчивые результаты в своей работе</span>
                <div class="d-flex justify-content-md-start justify-content-sm-center">
                    <button class="btn-events btn-events-s btn_book_buy" type="button" data-toggle="modal" data-target="#book-buy" data-book="Как довести успех в продажах до автоматизм">получить бесплатно</button>
                </div>
            </li>
        </ul>
    </div>
</section>