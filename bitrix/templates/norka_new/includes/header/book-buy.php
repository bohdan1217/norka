<section class="section-content-main_2">
    <div class="container">
        <ul class="row align-items-center ul-book-list">
            <li class="offset-md-1 col-md-4">
                <div class="main-wrap">
                    <img src="<?=SITE_TEMPLATE_PATH?>/src/img/index/book.png" alt=""/>
                </div>
            </li>
            <li class="col-md-7">
                <h4 class="main-books-title_2">  Хотите стать <br> экспертом продаж</h4>
                <span class="d-block main-books-text_2">Закажите книгу  “Экспертные продажи”</span>
                <div class="d-flex justify-content-md-start justify-content-sm-center">
                    <a href="http://norcabook.ru/" target="_blank">
                        <button class="btn-events">заказать книгу сейчас</button>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</section>
