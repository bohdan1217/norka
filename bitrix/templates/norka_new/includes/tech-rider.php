<ul class="services-list section-indents-mdl">
    <li class="section-bg-white">
        <ul class="row align-items-start">
            <li class="col-sm-4 col-md-3 col-lg-2">
                <div class="services-wrap section-bg-grey">
                    <div class="services-block">
                        <img src="<?=SITE_TEMPLATE_PATH?>/src/img/press-center/settings.png" alt=""/>
                    </div>
                </div>
            </li>
            <li class="col-sm-8 col-md-9 col-lg-10 services-content">
                <h4 class="services-title services-title-small">Технический райдер</h4>
                <p class="services-text">Сегодня, главная задача лучших продавцов заключается в том, чтобы добиться доверия и взаимодействие с клиентом, действуя в первую очередь как помощник, а не продавец. Суть эффективной продажи не в том, чтобы продать товар, а в том чтобы помочь своему клиенту добиться успеха в его деятельности. Если помочь клиентам сделать умное</p>
                <a href="/press-center/tech-rider/">
                    <button class="btn-services">подробнее</button>
                </a>
            </li>
        </ul>
    </li>
</ul>