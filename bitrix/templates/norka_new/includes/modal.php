<div id="book-buy" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Заказать книгу</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>

            <div class="container form-modal">
                <form class="form-contact book_buy" action="<?=SITE_TEMPLATE_PATH?>/ajax/book-buy.php" method="POST">
                    <ul class="row">
                        <p class="error_result"></p>
                        <input type="hidden" name="book" value="">
                        <li class="col-md-12 col-lg-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" maxlength="25" placeholder="Ваше имя" name="name" class="form-name-label form-comments-name d-block form-comments-name-tr" required/>
                            <label id="name-error" class="count_name margin_top color_red_count"></label>
                        </li>
                        <li class="col-md-12 col-lg-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" maxlength="25" class="form-name-label form-comments-email form-comments-email-tr d-block" required>
                            <label id="email-error" class="count_email margin_top color_red_count"></label>
                        </li>

                        <li class="col-md-12 d-flex justify-content-center">
                            <button class="btn-submit" type="submit">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="service" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Заказать услугу</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>

            <div class="container form-modal">
                <form class="form-contact service_form" action="<?=SITE_TEMPLATE_PATH?>/ajax/service.php" method="POST">
                    <ul class="row">
                        <p class="error_result"></p>
                        <input type="hidden" name="id_service" value="">
                        <input type="hidden" name="service_name" value="">
                        <li class="col-md-12 col-lg-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" maxlength="25" placeholder="Ваше имя" name="name" class="form-name-label form-comments-name d-block form-comments-name-tr" required/>
                            <label id="name-error" class="count_name margin_top color_red_count"></label>
                        </li>
                        <li class="col-md-12 col-lg-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" maxlength="25" class="form-name-label form-comments-email form-comments-email-tr d-block" required>
                            <label id="email-error" class="count_email margin_top color_red_count"></label>
                        </li>

                        <li class="col-md-12 d-flex justify-content-center">
                            <button class="btn-submit" type="submit">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="training" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Заказать тренинг</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>

            <div class="container form-modal form-training">
                <form class="form-contact training_form" action="<?=SITE_TEMPLATE_PATH?>/ajax/training.php" method="POST">
                    <ul class="row">
                        <p class="error_result"></p>
                        <input type="hidden" name="id_service" value="">
                        <input type="hidden" name="servi ce_name" value="">

                        <span class="span-modal">Личная информация:</span>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text"  maxlength="25" data-dependency="second" placeholder="Товарная ориентация*" name="orientation" class="form-comments-name d-block form-comments-name-tr" required tabindex="1"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text" maxlength="25" id="second" data-dependency="third" placeholder="Ваше имя*" name="name" class="form-comments-name d-block form-comments-name-tr" required tabindex="2"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text" maxlength="25" id="third" placeholder="Должность*" name="position" class="form-comments-name d-block form-comments-name-tr" required tabindex="3"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="tel" onKeyPress="cislo()"  maxlength="25" placeholder="Номер телефона (с кодом города)*" name="phone" id="phone" class="form-comments-name d-block form-comments-name-tr" required tabindex="4"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text" maxlength="25" placeholder="Город*" name="city" class="form-comments-name d-block form-comments-name-tr" required tabindex="5"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="email" placeholder="Email*" name="email" maxlength="25" class="form-comments-name d-block form-comments-name-tr" required tabindex="6"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text" maxlength="25" placeholder="Адрес сайта вашей компании*" name="address" class="form-comments-name d-block form-comments-name-tr" required tabindex="7"/>
                        </li>
                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            Кто ваши клиенты: <br>
                        <li class="col-md-6 col-lg-6 form-comments-name-wrap-blog">
                            <input type="checkbox" name="clients" class="option-input checkbox" value="11"/>Физические лица
                        </li>
                        <li class="col-md-6 col-lg-6 form-comments-name-wrap-blog">
                            <input type="checkbox" name="clients" class="option-input checkbox" value="12"/>Розничные сети
                        </li>
                        <li class="col-md-6 col-lg-6 form-comments-name-wrap-blog">
                            <input type="checkbox" name="clients" class="option-input checkbox" value="13"/>Юридические лица
                        </li>
                        <li class="col-md-6 col-lg-6 form-comments-name-wrap-blog" style="margin-bottom: 15px">
                            <input type="checkbox" name="clients" class="option-input checkbox" value="14"/>Дилеры
                        </li>


                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog img-input datePInput">
                            <input type="text" maxlength="25" readonly="true" placeholder="Дата проведения*" name="date" class="form-comments-name d-block form-comments-name-tr datep dpDate"  required/>
                        </li>


                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog select-image" style="margin-bottom: 10px">
                            <span style="margin-right: 10px">Количество участников:</span>
                            <select onchange="" onclick="return false;" id="" name="count">
                                <option value="0">Выбрать</option>
                                <option value="1">5-10</option>
                                <option value="2" >10-20</option>
                                <option value="3">20-30</option>
                                <option value="4">30-40</option>
                                <option value="5">40-50</option>
                            </select>
                            <img src="/bitrix/templates/norka_new/src/img/down-arrow.png">
                        </li>

                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            Откуда вы узнали о Дмитрии Норка: <br>
                            <li class="col-md-4 col-lg-4 form-comments-name-wrap-blog">
                                    <input type="checkbox"  name="info" value="6" class="option-input checkbox"/>Яндекс
                            </li>
                            <li class="col-md-4 col-lg-4 form-comments-name-wrap-blog">
                                    <input type="checkbox"  name="info" value="7" class="option-input checkbox"/>Рамблер
                            </li>
                            <li class="col-md-4 col-lg-4 form-comments-name-wrap-blog">
                                    <input type="checkbox" name="info" value="8" class="option-input checkbox"/>Google
                            </li>
                            <li class="col-md-4 col-lg-4 form-comments-name-wrap-blog">
                                    <input type="checkbox" name="info" value="10" class="option-input checkbox"/>Другое
                            </li>
                            <li class="col-md-5 col-lg-5 form-comments-name-wrap-blog" style="margin-bottom: 15px">
                                <input type="checkbox" name="info" value="9" class="option-input checkbox"/>Рекомендации
                            </li>
                        </li>

                        <li class="col-md-12 col-lg-12 form-comments-name-wrap-blog">
                            <input type="text" maxlength="25" placeholder="Название вашей компании*" name="name_company" class="form-comments-name d-block form-comments-name-tr" required/>
                        </li>

                        <li class="col-md-12 form-comments-comment-wrap-blog section-indents-sm">
                            <textarea placeholder="Пожелания, комментарии, вопросы" name="text" class="form-comments-comment form-comments-comment-tr d-block" rows="5"></textarea>
                        </li>
                        <li class="col-md-12 d-flex">
                            <button class="btn-submit" type="submit" style="letter-spacing:0.75px">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="reg" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Регистрация</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>

            <div class="container form-modal">
                <form class="form-contact req_form" action="<?=SITE_TEMPLATE_PATH?>/ajax/reg.php" method="POST">
                    <ul class="row">
                        <p class="error_result"></p>
                        <input type="hidden" name="id_m" value="">
                        <input type="hidden" name="name_m" value="">
                        <li class="col-md-12 col-lg-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" maxlength="25" placeholder="Ваше имя" name="name" class="form-comments-name d-block form-comments-name-tr" required/>
                        </li>
                        <li class="col-md-12 col-lg-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" maxlength="25" class="form-comments-email form-comments-email-tr d-block" required>
                        </li>

                        <li class="col-md-12 d-flex justify-content-center">
                            <button class="btn-submit" type="submit">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>



<div id="question" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Задать вопрос автору</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>

            <div class="container form-modal">
                <form class="form-contact question_form" action="<?=SITE_TEMPLATE_PATH?>/ajax/question.php" method="POST">
                    <ul class="row">
                        <p class="error_result"></p>
                        <input type="hidden" name="id_service" value="">
                        <input type="hidden" name="service_name" value="">
                        <li class="col-md-12 col-lg-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" maxlength="25" placeholder="Ваше имя" name="name" class="form-name-label form-comments-name d-block form-comments-name-tr" required/>
                            <label id="name-error" class="count_name margin_top color_red_count"></label>
                        </li>
                        <li class="col-md-12 col-lg-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" maxlength="25" class="form-name-label form-comments-email form-comments-email-tr d-block" required>
                            <label id="email-error" class="count_email margin_top color_red_count"></label>
                        </li>
                        <li class="col-md-12 form-comments-comment-wrap-blog section-indents-sm">
                             <img src="<?=SITE_TEMPLATE_PATH?>/src/img/pen.png">
                            <textarea name="question" placeholder="Сообщение" class="form-comments-comment form-comments-comment-tr d-block review-text-comments" rows="5"></textarea>
                            <label id="question-error" class="counter_comment_style_modal counter_comments">Осталось символов: <span class="counter_number_comments">500</span></label>
                        </li>


                        <li class="col-md-12 d-flex justify-content-center">
                            <button class="btn-submit" type="submit">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="thank-book" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Спасибо!</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>
            <div class="container form-modal">
                     <span class="thankyou-title-form">Мы свяжимся с Вами в ближайшее время</span>
            </div>
        </div>
    </div>
</div>


<div id="thank-comment" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Комментарий отправлен</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>
            <div class="container form-modal">
                <span class="thankyou-title-form">Он будет опубликован на сайте после того, как пройдёт модерацию</span>
            </div>
        </div>
    </div>
</div>



<div id="thank-reviews" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="section-title modal-title">Отзыв отправлен</h4>
                <button class="close" type="button" data-dismiss="modal">×</button>
            </div>
            <div class="container form-modal">
                <span class="thankyou-title-form">Он будет опубликован на сайте после того, как пройдёт модерацию</span>
            </div>
        </div>
    </div>
</div>
