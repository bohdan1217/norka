var initReloadCaptchaLink = function (path, link, img, sid){
    $('#' + link).click(function(){
        $.getJSON(path + '/reload_captcha.php', function(data) {
            $('#' + img).attr('src','/bitrix/tools/captcha.php?captcha_sid='+data);
            $('#' + sid).val(data);
        });
        return false;
    });
}
