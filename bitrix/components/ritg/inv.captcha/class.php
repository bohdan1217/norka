<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class Captcha extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "CACHE_TYPE" => 'N',
            "CACHE_TIME" => 0,
            "CAN_RELOAD_CAPTCHA" => $arParams["CAN_RELOAD_CAPTCHA"] == 'Y'
        );
        return $result;
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $this->arResult = array(
            "CAPTCHA_CODE" => htmlspecialcharsbx($APPLICATION->CaptchaGetCode())
        );

        $this->includeComponentTemplate();
    }
}