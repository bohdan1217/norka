<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "CAN_RELOAD_CAPTCHA" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("INV_CAPTCHA_CAN_RELOAD"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N"
        )
    )
);