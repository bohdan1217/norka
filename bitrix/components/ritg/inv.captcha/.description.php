<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("INV_CAPTCHA_NAME"),
	"DESCRIPTION" => GetMessage("INV_CAPTCHA_DESCRIPTION"),
	"ICON" => "",
	"CACHE_PATH" => "N",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "inv.captcha",
			"NAME" => GetMessage("INV_CAPTCHA")
		)
	),
);