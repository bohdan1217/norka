<?
$MESS["IBLOCK_ADD_ERROR_REQUIRED"] = "Поле '#PROPERTY_NAME#' должно быть заполнено!";
$MESS["IBLOCK_ADD_ACCESS_DENIED"] = "Нет доступа";
$MESS["IBLOCK_FORM_WRONG_CAPTCHA"] = "Неверно введено слово с картинки";
$MESS["IBLOCK_ADD_LEVEL_LAST_ERROR"] = "Разрешено добавление только в разделы последнего уровня";
$MESS["IBLOCK_USER_MESSAGE_ADD_DEFAULT"] = "Элемент успешно добавлен";
$MESS["IBLOCK_ERROR_FILE_TOO_LARGE"] = "Размер загруженного файла превышает допустимое значение";
$MESS["CC_BIEAF_IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";
?>