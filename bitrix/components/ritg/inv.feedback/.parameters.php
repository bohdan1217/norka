<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
    return;

if ($arCurrentValues["IBLOCK_ID"] > 0) {
    $arIBlock = CIBlock::GetArrayByID($arCurrentValues["IBLOCK_ID"]);

    $bWorkflowIncluded = ($arIBlock["WORKFLOW"] == "Y") && CModule::IncludeModule("workflow");
    $bBizproc = ($arIBlock["BIZPROC"] == "Y") && CModule::IncludeModule("bizproc");
} else {
    $bWorkflowIncluded = CModule::IncludeModule("workflow");
    $bBizproc = false;
}

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}

$arProperty_LNSF = array(
    "NAME" => GetMessage("IBLOCK_ADD_NAME"),
    "TAGS" => GetMessage("IBLOCK_ADD_TAGS"),
    "DATE_ACTIVE_FROM" => GetMessage("IBLOCK_ADD_ACTIVE_FROM"),
    "DATE_ACTIVE_TO" => GetMessage("IBLOCK_ADD_ACTIVE_TO"),
    "IBLOCK_SECTION" => GetMessage("IBLOCK_ADD_IBLOCK_SECTION"),
    "PREVIEW_TEXT" => GetMessage("IBLOCK_ADD_PREVIEW_TEXT"),
    "PREVIEW_PICTURE" => GetMessage("IBLOCK_ADD_PREVIEW_PICTURE"),
    "DETAIL_TEXT" => GetMessage("IBLOCK_ADD_DETAIL_TEXT"),
    "DETAIL_PICTURE" => GetMessage("IBLOCK_ADD_DETAIL_PICTURE"),
);
$arVirtualProperties = $arProperty_LNSF;

$rsProp = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $arCurrentValues["IBLOCK_ID"]));
while ($arr = $rsProp->Fetch()) {
    $arProperty[$arr["ID"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S", "F"))) {
        $arProperty_LNSF[$arr["ID"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    }
}

$arGroups = array();
$rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", Array("ACTIVE" => "Y"));
while ($arGroup = $rsGroups->Fetch()) {
    $arGroups[$arGroup["ID"]] = $arGroup["NAME"];
}

$site = ($_REQUEST["site"] <> '' ? $_REQUEST["site"] : ($_REQUEST["src_site"] <> '' ? $_REQUEST["src_site"] : false));

$arEvents = array();
$rsSites = CSite::GetByID($site);
$lang = 'ru';
if($arSite = $rsSites->Fetch()){
    $lang = $arSite['LANGUAGE_ID'];
}
$rsET = CEventType::GetList(array('LID' => $lang));
while ($arET = $rsET->Fetch()) {
    $arEvents[$arET["EVENT_NAME"]] = "[" . $arET["ID"] . "] " . $arET["EVENT_NAME"];
}

$arFilter = Array("TYPE_ID" => $arCurrentValues["EVENT_NAME"], "ACTIVE" => "Y");
if ($site !== false)
    $arFilter["LID"] = $site;

$arMessages = Array();
$dbType = CEventMessage::GetList($by = "ID", $order = "DESC", $arFilter);
while ($arType = $dbType->GetNext())
    $arMessages[$arType["ID"]] = "[" . $arType["ID"] . "] " . $arType["SUBJECT"];

$arComponentParameters = array(
    "GROUPS" => array(
        "PARAMS" => array(
            "NAME" => GetMessage("IBLOCK_PARAMS"),
            "SORT" => "200"
        ),
        "ACCESS" => array(
            "NAME" => GetMessage("IBLOCK_ACCESS"),
            "SORT" => "400",
        ),
        "FIELDS" => array(
            "NAME" => GetMessage("IBLOCK_FIELDS"),
            "SORT" => "300",
        ),
        "TITLES" => array(
            "NAME" => GetMessage("IBLOCK_TITLES"),
            "SORT" => "1000",
        ),
        "EMAILS" => array(
            "NAME" => GetMessage("IBLOCK_EMAILS"),
            "SORT" => "350",
        ),
    ),

    "PARAMETERS" => array(

        "IBLOCK_TYPE" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),

        "IBLOCK_ID" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCK_IBLOCK"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),

        "PROPERTY_CODES" => array(
            "PARENT" => "FIELDS",
            "NAME" => GetMessage("IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperty_LNSF,
        ),

        "PROPERTY_CODES_REQUIRED" => array(
            "PARENT" => "FIELDS",
            "NAME" => GetMessage("IBLOCK_PROPERTY_REQUIRED"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $arProperty_LNSF,
        ),

        "GROUPS" => array(
            "PARENT" => "ACCESS",
            "NAME" => GetMessage("IBLOCK_GROUPS"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $arGroups,
        ),

        "STATUS_NEW" => array(
            "PARENT" => "PARAMS",
            "NAME" => GetMessage("IBLOCK_ACTIVE_NEW"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => 'Y',
            "PARENT" => "PARAMS",
        ),

        "LIST_URL" => array(
            "PARENT" => "PARAMS",
            "TYPE" => "TEXT",
            "NAME" => GetMessage("IBLOCK_ADD_LIST_URL"),
        ),

        "ADD_ELEMENTS" => Array(
            "NAME" => GetMessage("IBLOCK_ADD_ELEMENTS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => 'N',
            "PARENT" => "PARAMS",
        ),
        "SEND_EMAILS" => Array(
            "NAME" => GetMessage("IBLOCK_SEND_EMAILS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => 'N',
            "PARENT" => "PARAMS",
        ),
        "EVENT_NAME" => Array(
            "NAME" => GetMessage("IBLOCK_EVENT_NAME"),
            "TYPE" => "LIST",
            "VALUES" => $arEvents,
            "DEFAULT" => "",
            "MULTIPLE" => "N",
            "COLS" => 25,
            "PARENT" => "EMAILS",
            "REFRESH" => "Y",
        ),
        "EVENT_MESSAGE_ID" => Array(
            "NAME" => GetMessage("IBLOCK_EMAIL_TEMPLATES"),
            "TYPE" => "LIST",
            "VALUES" => $arMessages,
            "DEFAULT" => "",
            "MULTIPLE" => "N",
            "COLS" => 25,
            "PARENT" => "EMAILS",
        ),
        "EMAIL_TO" => Array(
            "NAME" => GetMessage("IBLOCK_EMAIL_TO"),
            "TYPE" => "STRING",
            "DEFAULT" => htmlspecialcharsbx(COption::GetOptionString("main", "email_from")),
            "PARENT" => "EMAILS",
        ),
    ),
);

$arComponentParameters["PARAMETERS"]["USE_CAPTCHA"] = array(
    "PARENT" => "PARAMS",
    "NAME" => GetMessage("IBLOCK_USE_CAPTCHA"),
    "TYPE" => "CHECKBOX",
);

$arComponentParameters["PARAMETERS"]["USER_MESSAGE_ADD"] = array(
    "PARENT" => "PARAMS",
    "NAME" => GetMessage("IBLOCK_USER_MESSAGE_ADD"),
    "TYPE" => "TEXT",
);

$arComponentParameters["PARAMETERS"]["DEFAULT_INPUT_SIZE"] = array(
    "PARENT" => "PARAMS",
    "NAME" => GetMessage("IBLOCK_DEFAULT_INPUT_SIZE"),
    "TYPE" => "TEXT",
    "DEFAULT" => 30,
);

$arComponentParameters["PARAMETERS"]["RESIZE_IMAGES"] = array(
    "PARENT" => "PARAMS",
    "NAME" => GetMessage("CP_BIEAF_RESIZE_IMAGES"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["MAX_FILE_SIZE"] = array(
    "PARENT" => "ACCESS",
    "NAME" => GetMessage("IBLOCK_MAX_FILE_SIZE"),
    "TYPE" => "TEXT",
    "DEFAULT" => "0",
);

$arComponentParameters["PARAMETERS"]["PREVIEW_TEXT_USE_HTML_EDITOR"] = array(
    "PARENT" => "ACCESS",
    "NAME" => GetMessage("CP_BIEAF_PREVIEW_TEXT_USE_HTML_EDITOR"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["DETAIL_TEXT_USE_HTML_EDITOR"] = array(
    "PARENT" => "ACCESS",
    "NAME" => GetMessage("CP_BIEAF_DETAIL_TEXT_USE_HTML_EDITOR"),
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

foreach ($arVirtualProperties as $key => $title) {
    $arComponentParameters["PARAMETERS"]["CUSTOM_TITLE_" . $key] = array(
        "PARENT" => "TITLES",
        "NAME" => $title,
        "TYPE" => "STRING",
    );
}

?>