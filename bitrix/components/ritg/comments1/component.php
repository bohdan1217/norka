<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if(!CModule::IncludeModule ('iblock') )
{
	die();
}

/**
* 
*/
class Comment
{

	public $property;

	function __construct( $commentProp )
	{
	  $this->property = $commentProp;
	}


	public function delete( )
	{
 

	}


	public function update()
	{

	}


	public function getID()
	{

	}


	public function getName()
	{
		return $this->property["name"];
	}


}

	
/**
* 
*/
class CommentsList
{

	public $IBLOCK_ID;
	public $GROUP_ID;
	
	function __construct( $IBLOCK_ID, $GROUP_ID )
	{

		$this->IBLOCK_ID = $IBLOCK_ID;
		$this->GROUP_ID = $GROUP_ID;
	 
	}



	public function getComment ( $commentProp )
	{
		return new Comment( $commentProp  );
	}

	public function getComments( )
	{

		$comments = array();

		$cElems = CIBlockElement::GetList(array(

		), 
			array(
				"ACTIVE" => "Y",
				"IBLOCK_ID" => $this->IBLOCK_ID,
				"PROPERTY_GROUP_ID" => $this->GROUP_ID
			), 
			
		false,
		false, 
		array( 
			"ID", 
			"IBLOCK_ID",
			"NAME", 
			"PROPERTY_GROUP_ID",
			"PROPERTY_LIKES",
			"PROPERTY_DISLIKES",
			"DETAIL_TEXT",
			"ACTIVE_FROM" ));

		while( $arComment = $cElems->GetNextElement()) {
			$comments[] = $this->getComment( $arComment->GetFields());
		}

		return $comments;

	}


	public function groupByParentId(  )
	{

	}

}



$arResult["ITEMS"] = ( new CommentsList( 26, 0 ) )->getComments();




$this->IncludeComponentTemplate();