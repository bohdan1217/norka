<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php

global $USER;
  
function tree($id, $childsId, $comments, $isAuth, $th, $level) {

	global $USER;

	$comment = $comments->commentsManager->GetById( $id );
	$user = $comment->getUser(); 

	$curUser = $comments->commentsManager->users->GetCurUser();

	echo "<li id=\"comment_". $id ."\" data-commentid=\"" . $id . "\" class=\"comment ". ($level % 4 == 0 ? "level_reset" : '' )." comment_" . $id . "\" ><div class=\"wrap_comment\">";

	echo "<div class=\"name\">
				<p>" . $user->getName() . "</p>";

	if( $user->props["EMAIL"] && $user->props["ID"] < 0  ) {
		echo "<span class=\"email\">(" . $user->props["EMAIL"] . ")</span>";
	}

	echo "<span >" . FormatDate("x", MakeTimeStamp( $comment->props["FIELDS"]["ACTIVE_FROM"])) . "</span>
		 </div>";

	echo "<div class=\"content_com\">
			<div class=\"ava\"><img width=\"55\" height=\"55\" src=\"" . $user->getAva() . "\"></div>
			<div class=\"text\">" . HTMLToTxt($comment->props["FIELDS"]["PREVIEW_TEXT"]);


	if(isset( $comment->props["FIELDS"]["DETAIL_PICTURE"])) {
		echo "<div><img width=\"300\" height=\"200\" src=\"". CFile::GetPath( $comment->props["FIELDS"]["DETAIL_PICTURE"])."\"></div>";
	}

	echo "</div><div class=\"tumbs\">";

 		echo "<span class=\"count_like\">+" . $comment->countLikes(). "</span>";
 
		echo "<a href=\"#\" class=\"like ". ($comment->isLikedCurUser() ? "active" : "") . "\"></a>";
 
	echo "<div class=\"line\"></div>

			<span class=\"count_dislike\">-" .   $comment->countDislikes()  . "</span>
			<a href=\"#\" class=\"dislike ". ($comment->isDislikedCurUser() ? "active" : "") . "\"></a>";

 

	if ($isAuth) {

		if (!$user->isCurrentUser()) {
			echo "<a href=\"\" class=\"answer reply\" >Ответить</a>";
		}

		if($user->isCurrentUser() || $USER->isAdmin() ) {
			echo " <a href=\"\" class=\"answer delete_comment\" >Удалить</a>";
		}

	}
 

	echo "<span class=\"share\" href=\"\">Поделиться</span>
				<ul class=\"soc_com\">                  
                    <li><a class=\"fb\" target=\"_blank\"  href=\"http://www.facebook.com/sharer.php?s=100&p[title]=".$user->getName()."&p[summary]=" . HTMLToTxt(substr( $comment->props["FIELDS"]["PREVIEW_TEXT"]), 0, 250 ) ."&p[url]=". selfURL() ."\"></a></li>
                    <li><a class=\"tw\" target=\"_blank\" href=\"http://twitter.com/share?text=".substr( HTMLToTxt($comment->props["FIELDS"]["PREVIEW_TEXT"]), 0, 250 ) ."&url=".selfURL()."&hashtags=Дмитрий+Норка,Norca\"></a></li>
                    <li><a class=\"vk\" href=\"http://vk.com/share.php?url=" . selfURL() . "&title=". $user->getName() ."&description=". substr( HTMLToTxt($comment->props["FIELDS"]["PREVIEW_TEXT"]), 0, 250 ). "&image=&noparse=true\" target=\"_blank\"></a></li>
                </ul>
			</div>
		 </div></div>

		 <ul>";

		if($curUser && !$user->isCurrentUser())
		{
			echo "<li class=\"comment reply_cont\" style=\"display:none;\">
	                            <div class=\"wrap_comment\">
	                                <div class=\"name\"><p>" . $curUser->getName() . "</p></div>
	                                <div class=\"content_com\">
	                                    <div class=\"t_con\">
	                                        <div class=\"ava\">
	                                            <img src=\"". $curUser->getAva() ."\">
	                                        </div>
	                                        <div class=\"text\">
	                                            <textarea></textarea>
	                                            <input class=\"reply_btn\" type=\"submit\" value=\"Отправить комментарий\">

 												<input type=\"file\" class=\"photo_input\"  accept=\"image/*\" name=\"photo\">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>";

        }

	if (!empty($childsId["CHILDS"])) {



		foreach ($childsId["CHILDS"] as $k => $cid) {
			tree($cid, $comments->commentsManager->commentsTree[$cid], $comments, $isAuth, $th, $level + 1);
		}

	}
		

	echo "</ul>";

	echo ' </li>';
}

?>



<div class="comments" id="com_ajax">



	<?if(isset( $_REQUEST["AJAX"])) {
		$APPLICATION->RestartBuffer();
	}
?>
 <?php foreach($arResult["ERROR"] as $er): ?>

 	<div class="error_msg">
 		<?=$er;?>
 	</div>

 <?php endforeach; ?>

	<h5>Оставьте, пожалуйста, свой комментарий</h5>





	<span><?=$arResult["COMMENTS"]->commentsManager->getLabel()?></span>


<?php if( !$USER->isAuthorized() ): ?>
	<div> 
	 	<div class="auth_c">

		<?php $APPLICATION->IncludeComponent(
			"ulogin:auth",
			"",
			array(
				"PROVIDERS" => "vkontakte,odnoklassniki,facebook",
				"HIDDEN" => "other",
				"TYPE" => "panel",
				"REDIRECT_PAGE" => $arParams["REDIRECT_PAGE"],
				"UNIQUE_EMAIL" => "Y",
				"SEND_MAIL" => "N",
				"GROUP_ID" => array(
				),
			),
			false
		);?>

		<?$APPLICATION->IncludeComponent("ulogin:sync", "", array(
 			"AUTH_SERVICES" => 'Y'
		)); ?>
 
    </div>
	    <input type="email" class="style_input" value="<?=!empty($arResult["ERROR"]) ? $_REQUEST["email"]:"";?>" placeholder="Введите e-mail:" name="email">

    	<input type="text" class="style_input" value="<?=!empty($arResult["ERROR"]) ? $_REQUEST["anon_name"]:"";?>" placeholder="Введите имя:" name="anon_name">
	</div>
<?php endif; ?>

	<textarea name="message" <?echo (!$USER->isAdmin() ? " maxlength=\"1000\" " : "") ?> placeholder="Введите текст сообщения:"  cols="30" rows="10"><?=!empty($arResult["ERROR"]) ? $_REQUEST["message"] : ""?></textarea>
	
 	<input type="file" class="main_photo_input"  accept="image/*" name="photo">
     	<div>
    			
    			<?php if( !$USER->isAuthorized() ): ?>

    			<div class="captcha_cont">
    					<div>
    						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />  
    						<div class="text_ca">Введите слово с картинки<span>*</span></div>
    						<div class="captcha_block">
	    						<div class="captcha_img">
	    							<img class="captcha_i" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA">
	    						</div>
	     							<a class="reload" data-path="<?=$this->__folder?>"></a>
	    						 	<input name="captcha" class="style_input captcha_input"   type="text"> 
							</div>


    					</div>
 
    			</div> 

    			<?php endif; ?>


    	</div>
	<input type="submit" class="new_comment" value="Отправить сообщение">
<?


$arResult["COMMENTS"]->commentsManager->Sort();

foreach ( $arResult["COMMENTS"]->commentsManager->commentsTree as $pId => $childsId ) if( $childsId["MAIN"] ) {
	echo "<ul>"; 
	tree($pId, $childsId, $arResult["COMMENTS"], $USER->isAuthorized(), $this, 1 );
	echo "</ul>";
}


if ( isset($_REQUEST['AJAX']) )
{
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
   	die();
}


?>
 

</div>