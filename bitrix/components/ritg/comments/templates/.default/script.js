$(function() {


	function commentAction(action, props) {

		if (!props)
			props = {};


		props.action = action;
		props.AJAX = "1";

		props.anon_name = $('[name="anon_name"]').val();
		props.email = $('[name="email"]').val();


		var captchaEl = $('[name="captcha"');

		if (captchaEl) {
			props.captcha = captchaEl.val();
			props.captcha_sid = $('[name="captcha_sid"]').val();
		}


		if (window.FormData) {
			var formData = new FormData();
		} else formData = {};

		$.each(props, function(k, v) {


			if (v && v.type) {
				if (v.type.match(/^image/)) {

					if (window.FileReader) {
						reader = new FileReader();
						reader.onloadend = function(e) {
							//	showUploadedItem(e.target.result);
						};
						reader.readAsDataURL(v);
					}

					if (formData.append) {
						formData.append("photo", v);
					}

				}
			} else
			if (formData.append)
				formData.append(k, v);
			else
				formData[k] = v;
		});


		return $.ajax({
			url: '',
			type: 'POST',

			processData: false,
			contentType: false,
			data: formData
		})

		.success(function(html) {
			$('#com_ajax').html(html);


		})

		.fail(function() {


		});
	}


	function deleteComment(id) {
		return commentAction('delete', {
			comment_id: id
		})
	}


	function reply(parentId, props) {


		return newComment($.extend(props, {
			type: 'reply',
			reply_to: parentId
		}));
	}


	function newComment(props) {

		return commentAction('add', props);
	}

	function likeComment(commentId) {
		return commentAction('like', {
			comment_id: commentId
		});
	}

	function dislikeComment(commentId) {
		return commentAction('dislike', {
			comment_id: commentId
		});
	}

	function updateCaptcha( path ) {
		return $.ajax({
			url: path + '/captcha.php',
			type: 'POST',
		}).success(function(s) {
			var key = $.parseJSON(s);

			if(key.KEY)
			{
				$('[name=captcha_sid]').val(key.KEY);
				$('.captcha_i').attr('src', "/bitrix/tools/captcha.php?captcha_sid=" + key.KEY);
			}
		})
	}


	$(document)


	.on('click', '.delete_comment', function(e) {
		e.stopPropagation();
		e.preventDefault();

		deleteComment($(this).closest('[data-commentid]').data('commentid'));
	})

	.on('click', '.new_comment', function(e) {
		e.stopPropagation();
		e.preventDefault();


		var files = $('.main_photo_input[name=photo]').prop('files');

		newComment({
			message: $('[name="message"]').val(),
			photo: files.item(0)
		})
	})

	.on('click', '.reply', function(e) {
		e.stopPropagation();
		e.preventDefault();

		$(this).closest('.comment').find('.reply_cont').toggle(400);

	})

	.on('click', '.reply_btn', function(e) {
		e.stopPropagation();
		e.preventDefault();

		var inp = $(this).closest('.reply_cont');

		reply($(this).closest('[data-commentid]').data('commentid'), {

			reply_msg: inp.find('textarea').val(),
			photo: inp.find('.photo_input').prop('files').item(0)

		});

	})

	.on('click', '.like', function(e) {
		e.stopPropagation();
		e.preventDefault();


		likeComment($(this).closest('[data-commentid]').data('commentid'));

	})

	.on('click', '.dislike', function(e) {
		e.stopPropagation();
		e.preventDefault();

		dislikeComment($(this).closest('[data-commentid]').data('commentid'));

	})

	.on('click', '.reload', function(e) {
		e.stopPropagation();
		e.preventDefault();


		updateCaptcha($(this).data('path'));

	});


});