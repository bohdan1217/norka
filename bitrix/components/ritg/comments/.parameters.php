<?
CModule::IncludeModule("iblock");

$dbIBlockType = CIBlockType::GetList(
   array("sort" => "asc"),
   array("ACTIVE" => "Y")
);
while ($arIBlockType = $dbIBlockType->Fetch())
{
   if ($arIBlockTypeLang = CIBlockType::GetByIDLang($arIBlockType["ID"], LANGUAGE_ID))
      $arIblockType[$arIBlockType["ID"]] = "[".$arIBlockType["ID"]."] ".$arIBlockTypeLang["NAME"];
}

$arComponentParameters = array(
   "GROUPS" => array(

   ),
   "PARAMETERS" => array(

   )
);


$arParams["COLOR"] = Array(
    "PARENT" => "BASE",
    "NAME" => 'Выбор цвета',
    "TYPE" => "COLORPICKER",
    "DEFAULT" => 'FFFF00'
);?>