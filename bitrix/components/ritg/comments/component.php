<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/*
require_once 'class.php';*/

$comments = new Comments( $arParams );

$arResult["CAPTCHA_CODE"] = htmlspecialchars( $APPLICATION->CaptchaGetCode() ); 

if(isset( $_REQUEST["action"] )  )
{
 
$ACTION = $_REQUEST["action"];
$commentId = $_REQUEST["comment_id"];

$comment = $comments->commentsManager->GetbyId( $commentId );

 

switch ($ACTION) {
	case 'add':


		$type = $_REQUEST["type"];


 		if(!$USER->isAuthorized() )
		{
		 		
		 	$cptcha = new CCaptcha(); 

			$_REQUEST["anon_name"] = trim($_REQUEST["anon_name"]);

		 	if(empty($_REQUEST["anon_name"]))
		 	{

		 		$arResult["ERROR"][] = "Введите имя"; 

		 	} else if( strlen($_REQUEST["anon_name"]) < 3   )
			{
		 		$arResult["ERROR"][] = "Слишком короткое имя"; 
			}



			if(empty($_REQUEST["email"]))
			 	{

			 		$arResult["ERROR"][] = "Введите email"; 

			 	} else if( !filter_var( $_REQUEST["email"], FILTER_VALIDATE_EMAIL)   )
				{
			 		$arResult["ERROR"][] = "Неверный формат e-mail"; 
				}


			if( strlen($_REQUEST["captcha"]) < 1  )
			{
				$arResult["ERROR"][] = "Введите код с картинки"; 
			} else if(!$cptcha->CheckCode($_REQUEST["captcha"], $_REQUEST["captcha_sid"]))
			{
				$arResult["ERROR"][] = "Неверно введено слово с картинки.";
			 		
			}

			if(!empty($arResult["ERROR"]))   
				break;
 
		}
  

		if($type == 'reply') {
  
 			$parentId = intval($_REQUEST["reply_to"]);
 			$message = trim($_REQUEST["reply_msg"]);
 			 
 		} else {

 			$parentId = "";
 			$message = trim($_REQUEST["message"]);
 		}


 		if(!$USER->isAdmin())
 			$message = mb_substr( $message, 0, 1000, "UTF-8" );

 

 		if(!empty( $message) || !empty($_FILES["photo"]))
		 	$arResult["MESSAGE"][] = $comments->commentsManager->Add( $parentId, $message, $_REQUEST["anon_name"], $_REQUEST["email"] );
		 else 
		 	$arResult["ERROR"][] = "Сообщение не может быть пустым.";

		break;

	case 'like':

		if( $USER->isAuthorized() && $comment && !$comment->isCurrentUser() )
		{
			$arResult["MESSAGE"] = $comments->commentsManager->Like( $commentId );
		}

		break;

	case 'dislike':
		if( $USER->isAuthorized() && $comment && !$comment->isCurrentUser() )
		{
			$arResult["MESSAGE"] = $comments->commentsManager->Dislike( $commentId );
		}
		break;

	case 'delete':

		if( $USER->isAuthorized() && $comment && ($comment->isCurrentUser() || $USER->isAdmin()) )
		{
			$arResult["MESSAGE"] = $comments->commentsManager->Delete(  $commentId  );
		}

		break;

	case 'update':

		break;


	default:
		# code...
		break;
}


}


 
	$arResult["COMMENTS"] = $comments;

	$this->IncludeComponentTemplate();
 