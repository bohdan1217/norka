<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

if (!CModule::IncludeModule("iblock")) {
	die();
}
 

function getNumEnding($number, $endingArray)

{

	$number = $number % 100;

	if ($number>=11 && $number<=19)

	{

	$ending=$endingArray[2];

	} else 	{

	$i = $number % 10;

	switch ($i)	{

	case (1): $ending = $endingArray[0]; break;

	case (2):	case (3):	case (4): $ending = $endingArray[1]; break;

	default: $ending=$endingArray[2];	}

	}

	return $ending;

}

 

/**
 *
 */
class Comment {

	public $props;

	function __construct($props) {

		$this->props = $props;

	}

	public function getProp($propName) {
		return $this->props["PROP"][$propName]["VALUE"];
	}

	public function isAnon( )
	{
		return $this->getProp("IS_ANON");
	}

	public function getID() {
		return $this->props["FIELDS"]["ID"];
	}

	public function getUser() {

		return $this->props["USER"];
	}

	public function getName()
	{
		if($this->isAnon())
		{
			return "Anonymus";
		}

		return $this->getUser()->getName();
	}

	public function isCurrentUser() {
		global $USER;

		return intval($this->getProp("USER_ID")) == $USER->GetID();
	}

	public function delete() {

		if (CIBlockElement::Delete($this->getID())) {
			 
			return true;
		} 
	}

	public function getProperties()
	{
		$ar = array();

		foreach ($this->props["PROP"] as $k => $v  ) {
			$ar[$k] = $v["VALUE"];
		}

		return $ar;

	}

	public function update($prop) {


		$el = new CIBlockElement;

		$props = $this->getProperties();

		if( $el->Update( $this->getID(), array(
				"PROPERTY_VALUES" => array_merge( $props, $prop )
			)) )
		{
			return true;
		}
 
	}

	public  function isLikedCurUser( )
	{
		global $USER;

		return in_array($USER->GetID(), $this->getProp('LIKES') );
	}


	public function isDislikedCurUser( )
	{
		global $USER;

		return in_array($USER->GetID(), $this->getProp('DISLIKES') );
	}


	public function countLikes()
	{
		$l = $this->getProp('LIKES');

		if(!is_array( $l ))
		{
			return 0;
		}

		return count( $l );
	}

	public function countDislikes()
	{
		$l = $this->getProp('DISLIKES');
		
		if(!is_array( $l ))
		{
			return 0;
		}

		return count( $l );
	}

	public function clearVote()
	{
		global $USER;

 
		if (($key = array_search($USER->GetID(), $this->props["PROP"]["LIKES"]["VALUE"])) !== false) {
			unset($this->props["PROP"]["LIKES"]["VALUE"][$key]);
		} 

		if (($key = array_search($USER->GetID(), $this->props["PROP"]["DISLIKES"]["VALUE"])) !== false) {
			unset($this->props["PROP"]["DISLIKES"]["VALUE"][$key]);
		} 
	}

	public function like() {
		global $USER;

 		$this->clearVote();

 		$this->props["PROP"]["LIKES"]["VALUE"][] = $USER->GetID();

		$this->update( 
			array(
				"LIKES"=>  $this->getProp( "LIKES" ) ,
				"DISLIKES" =>  $this->getProp( "DISLIKES" ) 
			)
		);
	}

	public function dislike() {
 		global $USER;

 
		$this->clearVote();
 		$this->props["PROP"]["DISLIKES"]["VALUE"][] = $USER->GetID();
 
		$this->update( 
			array( 
				"LIKES"=>  $this->getProp( "LIKES" ) ,
				"DISLIKES" =>  $this->getProp( "DISLIKES" )  
			)
		);
	}

}

/**
 *
 */
class CommentUser {
	public $props;

	function __construct($props) {
		$this->props = $props;
	}

	public function getName() {

		if (empty($this->props["NAME"])) {
			return $this->props["LOGIN"];
		}

		return $this->props["NAME"] . ' ' . $this->props["LAST_NAME"];
	}

	public function isCurrentUser() {
		global $USER;
		return $this->props["ID"] == $USER->GetID();
	}

	public function isAdmin() {
		return $this->props["ADMIN"];
	}

	public function getAva() {

		if(empty($this->props["PERSONAL_PHOTO"]))
			return SITE_TEMPLATE_PATH .'/img/zaglushka.jpg';

		return CFile::GetPath( $this->props["PERSONAL_PHOTO"] );
	}

	public function isGuest()
	{
		return  $this->props["ID"] == -1;
	}
}

/**
 * `
 */
class UserManager {

	public $users;
	function __construct($users) {
		$this->users = $users;
	}

	public function GetById($ID) {
 
		return $this->users[$ID];
	}
 
	public function Create( $props )
	{
		
		$user = new CUser;


		$ID = $user->Add( $props );

		if( intval( $ID ) > 0 )
		{
			return true;
		} else {
			return $user->LAST_ERROR;
		}


	}

	public function GetCurUser()
	{
		global $USER;

		return $this->GetById( $USER->GetID() );
	}

	public function Delete( $ID )
	{
		if($user = $this->GetById($ID) && $user->delete())
		{

		}
	}
}

/**
 *
 */
class CommentsManager {

	public $comments;
	public $commentsTree;

	public $groupEl;

	public $users;

	function __construct($comments, $params, $users) {
		
		$this->comments = $comments;
		$this->users = $users ;

		$this->IBLOCK_ID = $params["IBLOCK_ID"];
		$this->GROUP_ID = $params["GROUP_ID"];

		$this->params = $params;

		foreach ($this->comments as $id => $value) {

			$parentId = $value["PROP"]["PARENT_ID"]["VALUE"];

			if ($parentId) {
				$this->commentsTree[$parentId]["CHILDS"][] = $id;
			} else {

				$this->commentsTree[$id]["MAIN"] = $value["MAIN"] = true;
				if (!isset($this->commentsTree[$id]["CHILDS"])) {
					$this->commentsTree[$id]["CHILDS"] = array();
				}

			}

			$this->comments[$id] = new Comment($value);
		}

		if( !$this->groupEl ) {
			$this->groupEl = CIBlockElement::GetById( $this->GROUP_ID );

			if( $this->groupEl ) {
				$this->groupEl = $this->groupEl->GetNext(); 
			}
		}

	}

	public function GetById($ID) {

		if (!isset($this->comments[$ID])) {
			return null;
		}

		return $this->comments[$ID];
	}

	private function _delete($ID) {

		if (($curComment = $this->GetById($ID)) 
			&& $curComment->delete()) 
		{
  

			if ($pID = $curComment->getProp("PARENT_ID")) {
				if (($key = array_search($ID, $this->commentsTree[$pID]["CHILDS"])) !== false) {
					unset($this->commentsTree[$pID]["CHILDS"][$key]);
				}
			}

			if(!empty($this->commentsTree[$ID]["CHILDS"]))
			{
				foreach ($this->commentsTree[$ID]["CHILDS"] as $value) {
					$this->_delete($value); 
				}
			}

			unset( $this->comments[$ID] );
			unset( $this->commentsTree[$ID] );
 

			return true;
		}

	}

	private function _update($ID, $props) {
		if (($curComment = $this->GetById($ID))
			&& $curComment->update($props)) 
		{
			return true;
		}
	}


	private function _add( $parentID, $msg, $anonName, $email )
	{


			$newEl = new CIBlockElement;


			global $USER;
 

			$prop = array(

				"FIELDS" => array(
					"PREVIEW_TEXT" => $msg,
					"ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
				),

				"PROP" => array(
					"USER_ID" => $USER->isAuthorized() ? $USER->GetID() : "",
					"GROUP_ID" => $this->GROUP_ID,
					"PARENT_ID" => $parentID,
					"LIKES" => array(),
					"DISLIKES" => array(),
					"IS_ANON" => !$USER->isAuthorized(),
					"ANON_NAME" => $anonName,
					"EMAIL" => $email
				),
			);

			$pr = array( 
				"IBLOCK_ID" => $this->IBLOCK_ID,
				"ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
				"PROPERTY_VALUES" => $prop["PROP"],
				"NAME" => "Комментарий к " . $this->groupEl["NAME"] , 
				"PREVIEW_TEXT" => $msg,
				"DETAIL_PICTURE" => $_FILES["photo"]
			);

			$newId = $newEl->Add(array_merge( array(), $pr ));

			$parentComment = $this->GetById($parentID);


			if (  $newId  ) {

				$prop["FIELDS"]["ID"] = $newId;

				$this->comments[$newId] = new Comment( $prop );

				$curEl = CIBlockElement::GetById( $newId );

				if( $resEl = $curEl->GetNext()) {
					$this->GetById($newId)->props["FIELDS"]["DETAIL_PICTURE"] = $resEl["DETAIL_PICTURE"];
				

					if( isset($anonName) )
					{
						$userData = array(
							"ID" => -1,
							"LOGIN" => $anonName,
							"EMAIL" => $email,
			 				"PERSONAL_PHOTO" => ""
						);

						$this->GetById( $newId )->props["USER"] = new CommentUser($userData);

					} else {


						$user = $this->users->GetById( $this->GetById( $newId )->getProp('USER_ID') );

						$userData = array(
							"ID" => $user->props["ID"],
							"LOGIN" => $user->getName(), 
			 				"PERSONAL_PHOTO" => ""
						);

						$this->GetById( $newId )->props["USER"] =  $user;

					}


					$pr = array_merge( $resEl, $userData );

					CEvent::Send(
						$this->params['EVENT_NAME'], 
						SITE_ID, 
						$pr,
						"Y", 
						(int) $this->params['EVENT_MESSAGE_ID']);


					if ($parentComment && $parentID) {
						$this->commentsTree[$parentID]["CHILDS"][] = $newId;
					} else {
						$this->commentsTree[$newId]["CHILDS"] = array();
						$this->commentsTree[$newId]["MAIN"] = true;
					}

				}
 

			} else {

				return $newEl->LAST_ERROR;
			}

	} 

	public function Like($ID) {
		if (($curComment = $this->GetById($ID))
			&& $curComment->like()) {
			return true;
		}
	}

	public function Dislike($ID) {
		if (($curComment = $this->GetById($ID))
			&& $curComment->dislike()) {
			return true;
		}
	}

	public function Delete($ID) {

		global $DB;

		$DB->StartTransaction();

		if (is_array($ID)) {
			foreach ($ID as $curID) {
				if (!$this->_delete($curID)) {
					$DB->Rollback();
					break;
				}

			}
		} else {
			if(!$this->_delete($ID)) {
				return $DB->Rollback();
			}
		}

 		$DB->Commit();
	}

	public function Update($ID, $props) {
		if (is_array($ID)) {
			foreach ($ID as $curID => $_props) {
				if (!$this->_update($curID, $_props)) {
					break;
				}

			}
		} else {
			$this->_update($curID, $props);
		}

	}

	public function Add($parentID, $msg, $anonName, $email) {

		global $USER;
		global $_FILES;
		global $arResult;

 		if( !$USER->isAuthorized() )
 		{

			if($anonName ) {
			 
						$this->_add( $parentID, $msg, $anonName, $email );

			 }
 		} else 
 		{
 			if (!$parentID || $parentComment = $this->GetById($parentID)) {

				if ($parentComment && $parentComment->isCurrentUser()) {
					return;
				}

				$this->_add( $parentID, $msg );

			}
 		} 
	}

	private function _filter($prop, $val, $item )
	{
		return $item->props["PROP"][$prop]["VALUE"] == $val;
	}

	public function filter($prop, $val) {
		return array_filter($this->comments, array( $this, "_filter", $props, $val ) );

	}

	public function Count() {
		return count($this->comments);
	}

	public function _sort($a, $b )
	{
		return $a < $b ? 1 : -1;
	}


	public function Sort()
	{
		$mainIds = array_keys( $this->commentsTree );
 

		uasort(  $mainIds, array($this, "_sort"));
 
		foreach ( $mainIds as $id ) {
			
			$childsKeys = $this->commentsTree[ $id ];

			uasort(  $childsKeys, array( $this, "_sort" ) );

			$newTree[ $id ] = $childsKeys;
		}

		$this->commentsTree = $newTree;

		return $newTree;
	}

	public function getLabel( )
	{
		return $this->Count() . " комментар" . getNumEnding( $this->Count(), array(
				"ий",
				"ия",
				"иев"
			) );
	}

 

}
 
/**
 *
 */
class Comments {

	public $commentsManager;

	public $commentsTree;

	public $IBLOCK_ID;
	public $GROUP_ID;

	function __construct( $arParams ) {

		$this->params = $arParams;


		$this->IBLOCK_ID = $this->params["IBLOCK_ID"];
		$this->GROUP_ID = $this->params["GROUP_ID"];


		$arFilter = array(
			"ACTIVITY" => "Y",
			"IBLOCK_ID" => $this->IBLOCK_ID,
			"PROPERTY_GROUP_ID" => $this->GROUP_ID,
		);

		$cEls = CIBlockElement::GetList(
			array("ACTIVE_FROM"),
			$arFilter,

			false,
			false,
			array("ID",
				"IBlOCK_ID",
				"ACTIVE_FROM",

				"PROPERTY_USER_NAME",
				"PROPERTY_LIKES",
				"PROPERTY_DISLIKES",
				"PROPERTY_PARENT_ID",
				"PROPERTY_GROUP_ID",
				"PROPERTY_USER_ID",

				"PROPERTY_IS_ANON",
				"PROPERTY_ANON_NAME",
				"PROPERTY_EMAIL",


				"PREVIEW_TEXT",
				"DETAIL_PICTURE",

				"NAME")
		);

		$uEls = CUser::GetList(
			$by = "ID",
			$sort = "ASC", array(

			), array(
				"NAME",
				"LAST_NAME",
				"PERSONAL_PHOTO",
			));

		while ($arUser = $uEls->Fetch()) { 
			$users[$arUser["ID"]] = new CommentUser($arUser);
		}

		$usersManager = new UserManager( $users );

		while ($el = $cEls->GetNextElement()) {

			$fields = $el->GetFields();

			$comments[$fields["ID"]] = array(
				"FIELDS" => $fields,
				"PROP" => $el->GetProperties()
			);


			if( !empty($fields["PROPERTY_USER_ID_VALUE"]) )
			{
				$comments[$fields["ID"]]["USER"] = $usersManager->GetById( $fields["PROPERTY_USER_ID_VALUE"] );
			} else {
				$comments[$fields["ID"]]["USER"] = new CommentUser(array(
						"ID" => -1,
						"LOGIN" => $fields["PROPERTY_ANON_NAME_VALUE"],
						"EMAIL" => $fields["PROPERTY_EMAIL_VALUE"],
						"PERSONAL_PHOTO" => ""
					));
			}

		}

		$this->commentsManager = new CommentsManager($comments, $this->params, $usersManager);
	}



	public function reload()
	{

	}

}
