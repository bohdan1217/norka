<?
define('cur_page',  $APPLICATION->GetCurPage());
define('CATALOG_IBLOCK',  3);
define('CATALOG_OFFERS',  9);


if (CModule::IncludeModule("iblock"))
{

    class SW_LIST extends CIBlockPropertyElementList
    {
        // инициализация пользовательского свойства для инфоблока
        function GetIBlockPropertyDescription()
        {
            return array(
                "PROPERTY_TYPE" => "E",
                "USER_TYPE" => "sw_list",
                "DESCRIPTION" => "SW_LIST",
                'GetPropertyFieldHtml' => array('SW_LIST', 'GetPropertyFieldHtml'),
                "ConvertToDB" => array("SW_LIST","ConvertToDB"),
                "ConvertFromDB" => array("SW_LIST","ConvertFromDB"),
            );
        }
        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
        {
            $arItem = Array(
                "ID" => 0,
                "IBLOCK_ID" => 0,
                "NAME" => ""
            );
            if(intval($value["VALUE"]) > 0)
            {
                $arFilter = Array(
                    "ID" => intval($value["VALUE"]),
                    "IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"],
                );
                $rsItem = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID", "IBLOCK_ID", "NAME"));
                $arItem = $rsItem->GetNext();
            }
            $html.=
                ' Количество:<input type="text" id="quan" name="'.$strHTMLControlName["VALUE"].'" value="'.htmlspecialchars($value["VALUE"]).'" />'.
                ' Количество:<input type="text" id="quan2" name="'.$strHTMLControlName["DESCRIPTION"].'" value="'.htmlspecialchars($value["DESCRIPTION"]).'" />'.
                ' Количество:<input type="text" id="quan3" name="'.$strHTMLControlName["TEST"].'" value="'.htmlspecialchars($value["TEST"]).'" />'
            ;

            return  $html;
        }
        function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
        {
            return;
        }
        function ConvertToDB($arProperty, $value) // сохранение в базу
        {
            $return = array("VALUE" => serialize($value["VALUE"]),
                "DESCRIPTION" => $value["DESCRIPTION"],
                "TEST" => $value["TEST"]);
            return $return;
        }
        function ConvertFromDB($arProperty, $value) //извлечение из БД
        {
            $return = array("VALUE" => unserialize($value["VALUE"]),
                "DESCRIPTION" => unserialize($value["DESCRIPTION"]),
                "TEST" => unserialize($value["TEST"]));
            return $return;
        }
    }
}
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('SW_LIST', 'GetIBlockPropertyDescription'));






function sw_get_ptoperty_val($prop){
    $val = $prop['VALUE'];
    if($prop['USER_TYPE_SETTINGS']['TABLE_NAME']){
        $val = sw_get_value_by_id($prop['USER_TYPE_SETTINGS']['TABLE_NAME'], $prop['VALUE']);
    }
    return $val;
}


function get_city_by_ip($ip){
    $data = file_get_contents_timeout("http://ipgeobase.ru:7020/geo?ip=".$ip);
    if($data){
        $xml = simplexml_load_string($data);
        return $xml->ip->city;
    }else{
        return "Москва";
    }
}

function file_get_contents_timeout($filename, $timeout=3)
{
    if(strpos($filename,"://")===false) return file_get_contents($filename);
    if(!function_exists("curl_init")) return false;
    $session=curl_init($filename);
    curl_setopt($session,CURLOPT_MUTE,true);
    curl_setopt($session,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($session,CURLOPT_CONNECTTIMEOUT,$timeout);
    curl_setopt($session,CURLOPT_TIMEOUT,$timeout);
    curl_setopt($session,CURLOPT_USERAGENT,"Mozilla/5.0 (compatible)");
    $result=curl_exec($session);
    curl_close($session);
    return $result;
}


$monthes = array(
    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
);


    function sw_get_value_by_id($table, $id = false, $return_array = false){
    global $DB;
    $q = "SELECT distinct `UF_NAME`, `UF_XML_ID`, `UF_DESCRIPTION` FROM `". $table ."` ";
    if($id != false){
        if(is_array($id)){
            foreach($id as $key => $line){
                $dev = ' AND';
                if($key == 0)
                    $dev = ' WHERE';
                $q .= $dev .= " `UF_XML_ID` = '". $line . "'" ;
            }
        }else{
            $q .= "WHERE `UF_XML_ID` = '". $id . "'" ;
        }
    }
    $q  .= ' ;';

    $results = $DB->Query($q);
    if(!is_array($id) && !$return_array){

        $row = $results->Fetch();
        return $row['UF_NAME'];
    }elseif($return_array && !is_array($id)){

        //$row = $results->Fetch();
        return $results;
    }
    return $results;
}

function sw_get_option($code, $id = 42){
    CModule::IncludeModule("iblock");
    $arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_" . $code);
    $arFilter = Array("IBLOCK_ID" => $id);
    $res = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        $arFilter,
        false,
        Array("nPageSize"=>1),
        $arSelect
    );
    while($el = $res->GetNextElement()){
        $arFields = $el->GetFields();
        return $arFields['PROPERTY_'. $code .'_VALUE'];
    }
    return '';
}

function sw_get_option_html($code, $id = 10){
    CModule::IncludeModule("iblock");
    $arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_" . $code);
    $arFilter = Array("IBLOCK_ID" => $id);
    $res = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        $arFilter,
        false,
        Array("nPageSize"=>1),
        $arSelect
    );
    while($el = $res->GetNextElement()){
        $arFields = $el->GetFields();
        return $arFields['~PROPERTY_'. $code .'_VALUE']['TEXT'];
    }
    return '';
}



function teil_get_lat_name($str){
    $str = strtolower($str);
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=>"_", "-"=>"_", "+"=>"_", "="=>"_"
    );
    return strtr($str,$tr);
}

function sw_get_show_price($product_id, $price = 0, $currency = 'RUB'){
    CModule::IncludeModule("catalog");
    global $USER;
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
        $product_id,
        $USER->GetUserGroupArray(),
        "N",
        array(),
        SITE_ID,
        array()
    );

    $discont_price = CCatalogProduct::CountPriceWithDiscount(
        $price,
        $currency,
        $arDiscounts
    );

    $show_price = $price;
    if($price != $discont_price)
        $show_price = $discont_price;


    $offers = array();
    $arSelect = Array("ID", "NAME", "CATALOG_GROUP_1", "CATALOG_PRICE_1" );
    $arFilter = Array("IBLOCK_ID" => CATALOG_OFFERS,  "ACTIVE"=>"Y", 'PROPERTY_CML2_LINK' => $product_id);
    $reviews_res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
    //$total = $res -> SelectedRowsCount();
    $min_price = 0;
    while($offer = $reviews_res->GetNextElement()){
        $offer_fields = $offer -> getFields();

        if($offer_fields['CATALOG_PRICE_1'] < $min_price || $min_price == 0){
            $price = $offer_fields['CATALOG_PRICE_1'];
            $min_price = $offer_fields['CATALOG_PRICE_1'];
            $currency = $offer_fields['CATALOG_CURRENCY_1'];

            $show_price = CCatalogProduct::CountPriceWithDiscount(
                $price,
                $currency,
                $arDiscounts
            );
        }
    }

    $ret_arr = array(round($show_price, 0), round($price, 0), $currency);

    return $ret_arr;
}

function sw_product($arItem, $class = "") { ?>
    <?
    $product_id = $arItem['ID'];
    if(!empty($arItem['ITEM_ID']))
        $product_id = $arItem['ITEM_ID'];
    if(!empty($arItem['PRODUCT_ID']))
        $product_id = $arItem['PRODUCT_ID'];


    $first_input_id = $product_id;
    $parent_id = '';


    ?>


    <?

//var_dump($arItem["SHOW_COUNTER"]);

    $arFilter = Array("IBLOCK_ID"=>4);
    $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID","NAME", "SHOW_COUNTER"));
    while($ar_fields = $res->GetNext())
    {
//echo "У элемента ".$ar_fields["NAME"]." ".$ar_fields["SHOW_COUNTER"]." показов<br>";
    }

    ?>

    <?
    $active = '';
    if(sw_check_in_fav($product_id))
        $active = ' active';
    if(cur_page == '/favorites/')
        $active .= ' remove';
    ?>


    <?php
    $img = $arItem['PREVIEW_PICTURE'];
    if(is_array($img))
        $img = $arItem['PREVIEW_PICTURE']['ID'];
    ?>				<?
    if(cur_page == '/favorites/') :?>
        <div class="catalog-item__delete add_to_fav<?=$active?>" data-id="<?=$first_input_id?>" <?=$parent_id?>></div>
    <?else :?>
        <div class="catalog-item__like ico-like add_to_fav<?=$active?>" data-id="<?=$first_input_id?>" <?=$parent_id?>></div>
    <?endif;?>
    <div class="catalog-item__img"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
            <?
            //$renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 221, "height" => 191), BX_RESIZE_IMAGE_EXACT, false);
            $file = CFile::ResizeImageGet($img, array('width'=>221, 'height' => 191), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $src = $file["src"];
            ?>
            <img src="<?=$src?>" alt="<?=$arItem["NAME"]?>"></a></div>
    <?if(cur_page == '/catalog-lenses/') :?>
        <div class="catalog-item__name lenses-setting"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
        <div class="catalog-item__caption"><?=sw_get_ptoperty_val($arItem["PROPERTIES"]["LENSTYPE"])?></div>
    <?else :?>
        <div class="catalog-item__name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
    <?endif;?>


    <div class="catalog-item__price">


        <?php
        //if(!empty($arItem['CATALOG_PRICE_1']))
        //$price = sw_get_show_price($product_id , $arItem['CATALOG_PRICE_1']);
        //elseif(!empty($arItem['PRICE']))
        //$price = sw_get_show_price($product_id , $arItem['PRICE']);
        //var_dump($arItem['CATALOG_PRICE_1']);
        ?>


        <?
        $price = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, 'N');
        ?>
        <?php if($price['RESULT_PRICE']['BASE_PRICE'] != $price['RESULT_PRICE']["DISCOUNT_PRICE"]){ ?>
            <span class="old"><?=$price['RESULT_PRICE']['BASE_PRICE']?><i class="rouble">a</i></span>
        <?php } ?>
        <span class="new"><?=$price['RESULT_PRICE']["DISCOUNT_PRICE"]?><i class="rouble">a</i></span></div>


<? }

function sw_product_viewed($arItem, $class = "") { ?>
    <?

    $product_id = $arItem['ID'];
    if(!empty($arItem['ITEM_ID']))
        $product_id = $arItem['ITEM_ID'];
    if(!empty($arItem['PRODUCT_ID']))
        $product_id = $arItem['PRODUCT_ID'];


    $first_input_id = $product_id;
    $parent_id = '';

    ?>
    <?php
    $img = $arItem['PREVIEW_PICTURE'];
    if(is_array($img))
        $img = $arItem['PREVIEW_PICTURE']['ID'];
    ?>

    <div class="similar-items__item">
        <div class="similar-items__item-img">

            <?
            //$renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 221, "height" => 191), BX_RESIZE_IMAGE_EXACT, false);
            $file = CFile::ResizeImageGet($img, array('width'=>167, 'height' => 82), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $src = $file["src"];
            ?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$src?>" alt="<?=$arItem["NAME"]?>"></a>
        </div>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="similar-items__item-name"><?=$arItem["NAME"]?><!-- <br>Ban 7069 5206 --></a>
        <div class="similar-items__item-price">
            <?
            $price = CCatalogProduct::GetOptimalPrice($arItem["ID"], 1, 'N');
            ?>

            <?php if($price['RESULT_PRICE']['BASE_PRICE'] != $price['RESULT_PRICE']["DISCOUNT_PRICE"]){ ?>
                <s><?=$price['RESULT_PRICE']['BASE_PRICE']?><i class="rouble">a</i></s>
            <?php } ?>
            <b><?=$price['RESULT_PRICE']["DISCOUNT_PRICE"]?><i class="rouble">a</i></b>
        </div>
    </div>

<? }

function sw_check_in_compare($product_id){
    $compare_res = $_SESSION['CATALOG_COMPARE_LIST'][CATALOG_IBLOCK];
    if(isset($compare_res['ITEMS'][$product_id]))
        return true;
    else
        return false;
}
function sw_compare_total(){
    $compare_res = $_SESSION['CATALOG_COMPARE_LIST'][CATALOG_IBLOCK];
    return sizeof($compare_res['ITEMS']);
}

function sw_get_default_photo($thumb = array()){
    $photo_id = sw_get_option('DEFAULT_PHOTO');
    if($photo_id){
        if(empty($thumb)){
            return CFile::GetPath($photo_id);
        }else{
            $file = CFile::ResizeImageGet($photo_id,
                $thumb, BX_RESIZE_IMAGE_PROPORTIONAL, true);
            return $file['src'];
        }
    }else{
        return '/bitrix/templates/crushpro/s/img/default.jpg';
    }
}


function sw_add_product_to_basket($product_id, $quantity = 1){
    CModule::IncludeModule('catalog');
    CModule::IncludeModule("sale");

    $basket_product = CSaleBasket::GetList(
        array("ID" => "DESC"),
        array(
            "PRODUCT_ID" => $product_id,
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array()
    )->Fetch();

    // Update product quantity


    if ($basket_product)
    {
        $result = false;

        CSaleBasket::Update($basket_product['ID'], array(
            "QUANTITY" => $quantity
        ));
        $product_quantity = $quantity;
        $item_id = $basket_product['ID'];
    }else{
        $item_id = Add2BasketByProductID($product_id, $quantity);
    }
    return $item_id;
}


/*----------  events  ----------*/


function sw_check_in_fav($product_id){
    /* global $USER;
     global $DB;*/
    CModule::IncludeModule("sale");
    $res = CSaleBasket::GetList(
        array("ID" => "DESC"),
        array(
            "PRODUCT_ID" => $product_id,
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            'DELAY' => 'Y',
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array()
    );
    if($res->Fetch()){
        return true;
    }else{
        return false;
    }
    /* if(!$USER->IsAuthorized())
       return false;
     $q = "SELECT `id` FROM `sw_favorites` WHERE `product_id` = ". $product_id ." AND `user_id` = " . CUser::GetID();
     $results = $DB->Query($q);
     if($results->Fetch()){
       return true;
     }else{
       return false;
     }*/
}


function sw_favorites_total(){
    global $DB;
    global $USER;

    CModule::IncludeModule("sale");

    $res = CSaleBasket::GetList(
        array("ID" => "DESC"),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            'DELAY' => 'Y',
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array()
    );
    $total = $res->SelectedRowsCount();
    return $total;

    /*if(!$USER->IsAuthorized()){
      return 0;
    }
    $q = "SELECT `id`, `product_id` FROM `sw_favorites` WHERE `user_id` = " . CUser::GetID();
    $results = $DB->Query($q);
    return intval($results->SelectedRowsCount());*/
}



function sw_get_ending($number){
    $end = '';
    if($number == 1){
        $end = '';
    }elseif($number > 1 && $number < 5){
        $end = 'а';
    }elseif(($number > 4 && $number < 21) || $number == 0){
        $end = 'ов';
        if(cur_lang == 'ua')
            $end = 'ів';
    }else{
        if(preg_match_all('/\d+/', $text, $numbers))
            $lastnum = end($numbers[0]);
        if($lastnum = 1){
            $end = '';
        }elseif($lastnum > 1 && $lastnum < 5){
            $end = 'а';
        }else{
            $end = 'ов';
            if(cur_lang == 'ua')
                $end = 'ів';
        }
    }
    return $end;
}


function sw_get_ending_groups($number){
    $end = '';
    if($number == 1){
        $end = 'а';
    }elseif($number > 1 && $number < 5){
        $end = 'ы';
    }elseif(($number > 4 && $number < 21) || $number == 0){
        $end = '';
        if(cur_lang == 'ua')
            $end = 'ів';
    }else{
        if(preg_match_all('/\d+/', $text, $numbers))
            $lastnum = end($numbers[0]);
        if($lastnum = 1){
            $end = '';
        }elseif($lastnum > 1 && $lastnum < 5){
            $end = 'а';
        }else{
            $end = 'ов';
            if(cur_lang == 'ua')
                $end = 'ів';
        }
    }
    return $end;
}

function sw_check_payment_avaliable($payment_id, $delivery){
    if(!empty($_REQUEST['delivery']) || !empty($delivery)){
        if(!empty($_REQUEST['delivery']))
            $delivery = (int) $_REQUEST['delivery'];
        global $DB;
        $q = ' SELECT dp.DELIVERY_ID
	  				FROM b_sale_delivery2paysystem dp 
	   					LEFT JOIN b_sale_pay_system_action pa ON pa.PAY_SYSTEM_ID = dp.PAYSYSTEM_ID
	      					WHERE dp.PAYSYSTEM_ID = ' . $payment_id;
        $results = $DB->Query($q);
        $delivery_list = array();
        while ($row = $results->Fetch()){
            array_push($delivery_list, $row['DELIVERY_ID']);
        }
        if(!empty($delivery_list) && !in_array($delivery, $delivery_list))
            return false;
    }
    return true;
}


function sw_workshop_val(){
    global $APPLICATION;
    $shop = "";
    $shop_val = $APPLICATION->get_cookie("sw_per_shop");
    if($shop_val)
        $shop = $shop_val;
    return $shop;
}

function sw_city_val(){
    global $APPLICATION;
    $city = "";
    $city_val = $APPLICATION->get_cookie("sw_per_city");
    if($city_val)
        $city = $city_val;
    return $city;
}


function sw_per_page_val(){
    global $APPLICATION;
    $grid = 333;
    $grid_val = $APPLICATION->get_cookie("sw_per_page");
    if($grid_val) {
        $grid = $grid_val;
    }
    return $grid;
}


function sw_per_age_val(){
    global $APPLICATION;
    $age = '';
    $age_val = $APPLICATION->get_cookie("sw_per_age");
    if($age_val) {
        $age = $age_val;
    }
    return $age;
}


?>