<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "Карта сайта | Norca"); 
$APPLICATION->SetTitle("Карта сайта | Norca");
?>
 <div class="content">
 <div class="map_site">
<h1>Карта сайта</h1>
<?$APPLICATION->IncludeComponent("bitrix:menu","sitemap",Array(
        "ROOT_MENU_TYPE" => "top", 
        "MAX_LEVEL" => "4", 
        "CHILD_MENU_TYPE" =>"main_child", 
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "N", 
        "MENU_CACHE_TIME" => "3600", 
        "MENU_CACHE_USE_GROUPS" => "Y", 
        "MENU_CACHE_GET_VARS" => "" 
    )
);?>

</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
