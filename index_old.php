<?
if(isset($_GET['no_mobile'])){
    setcookie('mobile','no',time()+3600*48, "/", ".".$_SERVER['HTTP_HOST']);
}
function is_mobile() {
  $user_agent=strtolower(getenv('HTTP_USER_AGENT'));
  $accept=strtolower(getenv('HTTP_ACCEPT'));
 
  if ((strpos($accept,'text/vnd.wap.wml')!==false) ||
      (strpos($accept,'application/vnd.wap.xhtml+xml')!==false)) {
    return 1; // Мобильный браузер обнаружен по HTTP-заголовкам
  }
 
  if (isset($_SERVER['HTTP_X_WAP_PROFILE']) ||
      isset($_SERVER['HTTP_PROFILE'])) {
    return 2; // Мобильный браузер обнаружен по установкам сервера
  }
 
  if (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|'.
    'wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|'.
    'lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|'.
    'mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|'.
    'm881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|'.
    'r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|'.
    'i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|'.
    'htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|'.
    'sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|'.
    'p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|'.
    '_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|'.
    's800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|'.
    'd736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |'.
    'sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|'.
    'up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|'.
    'pocket|kindle|mobile|psp|treo|android|iphone|ipod|webos|wp7|wp8|'.
    'fennec|blackberry|htc_|opera m|windowsphone)/', $user_agent)) {
    return 3; // Мобильный браузер обнаружен по сигнатуре User Agent
  }

  if (in_array(substr($user_agent,0,4),
    Array("1207", "3gso", "4thp", "501i", "502i", "503i", "504i", "505i", "506i",
          "6310", "6590", "770s", "802s", "a wa", "abac", "acer", "acoo", "acs-",
          "aiko", "airn", "alav", "alca", "alco", "amoi", "anex", "anny", "anyw",
          "aptu", "arch", "argo", "aste", "asus", "attw", "au-m", "audi", "aur ",
          "aus ", "avan", "beck", "bell", "benq", "bilb", "bird", "blac", "blaz",
          "brew", "brvw", "bumb", "bw-n", "bw-u", "c55/", "capi", "ccwa", "cdm-",
          "cell", "chtm", "cldc", "cmd-", "cond", "craw", "dait", "dall", "dang",
          "dbte", "dc-s", "devi", "dica", "dmob", "doco", "dopo", "ds-d", "ds12",
          "el49", "elai", "eml2", "emul", "eric", "erk0", "esl8", "ez40", "ez60",
          "ez70", "ezos", "ezwa", "ezze", "fake", "fetc", "fly-", "fly_", "g-mo",
          "g1 u", "g560", "gene", "gf-5", "go.w", "good", "grad", "grun", "haie",
          "hcit", "hd-m", "hd-p", "hd-t", "hei-", "hiba", "hipt", "hita", "hp i",
          "hpip", "hs-c", "htc ", "htc-", "htc_", "htca", "htcg", "htcp", "htcs",
          "htct", "http", "huaw", "hutc", "i-20", "i-go", "i-ma", "i230", "iac",
          "iac-", "iac/", "ibro", "idea", "ig01", "ikom", "im1k", "inno", "ipaq",
          "iris", "jata", "java", "jbro", "jemu", "jigs", "kddi", "keji", "kgt",
          "kgt/", "klon", "kpt ", "kwc-", "kyoc", "kyok", "leno", "lexi", "lg g",
          "lg-a", "lg-b", "lg-c", "lg-d", "lg-f", "lg-g", "lg-k", "lg-l", "lg-m",
          "lg-o", "lg-p", "lg-s", "lg-t", "lg-u", "lg-w", "lg/k", "lg/l", "lg/u",
          "lg50", "lg54", "lge-", "lge/", "libw", "lynx", "m-cr", "m1-w", "m3ga",
          "m50/", "mate", "maui", "maxo", "mc01", "mc21", "mcca", "medi", "merc",
          "meri", "midp", "mio8", "mioa", "mits", "mmef", "mo01", "mo02", "mobi",
          "mode", "modo", "mot ", "mot-", "moto", "motv", "mozz", "mt50", "mtp1",
          "mtv ", "mwbp", "mywa", "n100", "n101", "n102", "n202", "n203", "n300",
          "n302", "n500", "n502", "n505", "n700", "n701", "n710", "nec-", "nem-",
          "neon", "netf", "newg", "newt", "nok6", "noki", "nzph", "o2 x", "o2-x",
          "o2im", "opti", "opwv", "oran", "owg1", "p800", "palm", "pana", "pand",
          "pant", "pdxg", "pg-1", "pg-2", "pg-3", "pg-6", "pg-8", "pg-c", "pg13",
          "phil", "pire", "play", "pluc", "pn-2", "pock", "port", "pose", "prox",
          "psio", "pt-g", "qa-a", "qc-2", "qc-3", "qc-5", "qc-7", "qc07", "qc12",
          "qc21", "qc32", "qc60", "qci-", "qtek", "qwap", "r380", "r600", "raks",
          "rim9", "rove", "rozo", "s55/", "sage", "sama", "samm", "sams", "sany",
          "sava", "sc01", "sch-", "scoo", "scp-", "sdk/", "se47", "sec-", "sec0",
          "sec1", "semc", "send", "seri", "sgh-", "shar", "sie-", "siem", "sk-0",
          "sl45", "slid", "smal", "smar", "smb3", "smit", "smt5", "soft", "sony",
          "sp01", "sph-", "spv ", "spv-", "sy01", "symb", "t-mo", "t218", "t250",
          "t600", "t610", "t618", "tagt", "talk", "tcl-", "tdg-", "teli", "telm",
          "tim-", "topl", "tosh", "treo", "ts70", "tsm-", "tsm3", "tsm5", "tx-9",
          "up.b", "upg1", "upsi", "utst", "v400", "v750", "veri", "virg", "vite",
          "vk-v", "vk40", "vk50", "vk52", "vk53", "vm40", "voda", "vulc", "vx52",
          "vx53", "vx60", "vx61", "vx70", "vx80", "vx81", "vx83", "vx85", "vx98",
          "w3c ", "w3c-", "wap-", "wapa", "wapi", "wapj", "wapm", "wapp", "wapr",
          "waps", "wapt", "wapu", "wapv", "wapy", "webc", "whit", "wig ", "winc",
          "winw", "wmlb", "wonu", "x700", "xda-", "xda2", "xdag", "yas-", "your",
          "zeto", "zte-"))) {
    return 4; // Мобильный браузер обнаружен по сигнатуре User Agent
  }
 
  return false; // Мобильный браузер не обнаружен
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "бесплатные бизнес тренинги, бесплатные видео тренинги, бесплатные семинары и тренинги, бесплатные семинары и тренинги в москве, бесплатные семинары и тренинги в москве 2016, бесплатные тренинги, бесплатные тренинги в москве, бесплатные тренинги в москве 2016, бесплатные тренинги онлайн, бесплатные тренинги скачать, бесплатные тренинги скачать бесплатно, бесплатный тренинг по продажам, бесплатный тренинг 2016, бизнес тренинги бесплатно, видео тренинг бесплатно, семинары и тренинги в москве 2016 бесплатно, семинары и тренинги в москве бесплатно, тренинг бесплатно, тренинги в москве бесплатно, тренинги по продажам бесплатно, тренинги бесплатно смотреть, тренинги онлайн бесплатно тренинги семинар бесплатно, бесплатные семинары, бесплатные бизнес тренинги, бесплатный интернет тренинг, онлайн тренинг, программа тренинга, тренинги в москве, тренинг по продажам, тренинги в москве, тренинги и семинары, тренинги и семинары в москве, выездной тренинг 2016, семинары тренинги 2016, тренинг 2016, тренинги в москве 2016, тренинг продажи 2016, дайте советы продажам, прямые продажи советы, советы продажам, советы продажам продавцов, активные продажи тренинг,бизнес тренинги, бизнес тренинги по продажам, видео тренинг, курсы тренинги, лучшие тренинги, лучшие тренинги по продажам, методы тренинга, новые тренинги, обучение тренинги, основы продаж тренинг, проведение тренингов, проведение тренингов продаж, программа тренинга, продажи по телефону тренинг, профессиональные тренинги продаж, система тренингов, смотреть тренинг продажам, смотреть тренинги, технология продажи тренингов, тренинг по продажам москва, тренинг по продажам услуг, тренинг менеджер, тренинг навыков продаж, тренинг отдела продаж, тренинг продаж работа, тренинг розничным продажам, тренинг телефонных продаж, тренинг техника продаж, тренинг тренеров продажам, тренинг центр, тренинги в москве, тренинги для женщин, тренинги по продажам, тренинги по продажам для менеджеров, тренинги по продажам бесплатно, тренинги по продажам видео, тренинги по продажам спб, тренинги возражениям продажах, тренинги продаж обучение, тренинги сотрудников продаж, увеличение продаж тренинг, управление продажами тренинг, читать тренинг, эффективный тренинг, продажи тренинг, бизнес тренинги, тренинги, тренинги Дмитрий Норка, концепция продаж, тренинг по продажам, продажи тренинг, тренинг корпоративным клиентам, тренинги москва, тренинги в москве, Дмитрий Норка тренинги продаж, телефонные продажи, продажи по телефону, продажа по телефону без проблем, продажа холдный звонок, тренинг, тренинг техника продаж b2b, тренинг продаж b2b, продажи в b2b, технология продаж b2c, техника продаж b2c, бизнес тренер Дмитрий Норка, тренинг отдел продаж, руководить отделом продаж тренинг, отдел продаж тренинг, отдел продаж, руководить отделом продаж тренинг, отдел продаж тренинг, отдел продаж, управление продажами тренинг, управление сотрудниками продажи, тренинг руководителей отдела продаж, управление отделом продаж, мотивация отдела продаж, тренинг мотивация отдела продаж, экспертные продажи, тренинг по продажам, продавать боелее успешно, успешные продажи, экспертные продажи, эффективные продажи,");
$APPLICATION->SetPageProperty("description", "Бесплатные советы и тренинги по продажам Сайт бизнес тренера по продажам Дмитрия Норки. Программы тренингов продаж. Корпоративное обучение, тестирование менеджеров по продажам, Бесплатные тренинги продаж в Москве");
$APPLICATION->SetPageProperty("keywords", "Сайт бизнес-тренера, советы по продажам, официальный сайт бизнес-тренера Дмитрия Норки, тренинги по продажам, корпоративные бизнес-тренинги продаж, бесплатные советы по продажам,  тренинги продаж, сайт тренера по продажам");
$APPLICATION->SetPageProperty("title", "Тренинги по продажам - бесплатные советы и тренинги по эффективным продажам – Бизнес-тренер Дмитрий Норка, Корпоративные тренинги продаж в Москве");
$APPLICATION->SetTitle("Тренинги по продажам – Бизнес-тренер Дмитрий Норка! Корпоративные тренинги продаж в Москве!");
?>



	
<?
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH. '/js/idangerous.swiper.min.js');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/swiper.min.css');



?>


 <img class="norca" alt="Тренинг продаж от Дмитрия Норки! Тренинг по продажам №1! Бизнес тренинги" src="<?=SITE_TEMPLATE_PATH?>/img/norca.jpg">

 <?php 
 
     $GLOBALS["arSliderFilter"] = array(
            "!PREVIEW_PICTURE" => false
        );

  ?>

<?$APPLICATION->IncludeComponent("bitrix:news.list","clients_slider",Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "norca",
        "IBLOCK_ID" => "12",
        "NEWS_COUNT" => "10",
        "SORT_BY1" => "PROPERTY_SORT_SLIDER",
        "SORT_ORDER1" => "ASC",/*
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "DESC",*/
        "FILTER_NAME" => "arSliderFilter",
        "FIELD_CODE" => Array("ID", "PREVIEW_PICTURE"),
        "PROPERTY_CODE" => Array("DESCRIPTION", "SORT_SLIDER" ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "Y",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_STATUS_404" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
    )
);?>

<?php 

    //$APPLICATION->AddHeadScript('https://imgs.smartresponder.ru/52568378bec6f68117c48f2f786db466014ee5a0/');
 ?>

<div class="order">
       <div class="label_b">Еженедельные советы по продажам от Дмитрия Норка</div>
       <div  class="label_s">Введите свое имя и электронный адрес, чтобы получать еженедельные советы от Дмитрия Норка</div>

  <!--<form action="//norca.justclick.ru/subscribe/process/?rid[0]=1478078028.0477298065&" class="sr-box" method="post"  id="subscr-form-5161">

       <input type="hidden" name="field_name" class="sr-name">

       <input type="text" name="lead_name" placeholder="Ваше имя:">
       <input type="text" name="lead_email" class="sr-required" placeholder="Ваш email-адрес:">

       <input type="hidden" name="uid" value="42060">
       <input type="hidden" name="tid" value="0"><input type="hidden" name="lang" value="ru">
       <input type="hidden" name="did[]" value="21607">
       <input type="hidden" name="hash" value="Zm9ybWlkPTM5NDQmdW5hbWU9dHJ1ZSZtaWRkbGVuYW1lPWZhbHNlJmxhc3RuYW1lPWZhbHNlJmVtYWlsPXRydWUmcGhvbmU9ZmFsc2UmY2l0eT1mYWxzZQ=="/>
   
       <input type="submit" name="subscribe" value="Подписаться">
   </form>-->

	<div id="c6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf" style="margin-top: 50px; overflow: hidden; text-align: center; height: 54px;">
	<input type="hidden" name="sform[hash]" value="c6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf">

				<input class="c6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf_text" style="    font-size: 16px;
    width: 217px;
    height: 41px;
    margin-left: 0;
    padding: 0 25px;
    box-shadow: inset 11px 11px 30px 0 rgba(0,0,0,.19);
    border: 1px solid #a0a0a0;
    background-color: #2b4054;
    font-family: 'MuseoSansCyrl-500',sans-serif;
    color: #d3d3d3;
    margin-right: 34px;
    box-sizing: content-box" maxlength="255" name="sform[0LjQvNGP]" required="required" placeholder="Ваше Имя:">


					<input class="c6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf_email" style="    font-size: 16px;
    width: 217px;
    height: 41px;
    margin-left: 0;
    padding: 0 25px;
    box-shadow: inset 11px 11px 30px 0 rgba(0,0,0,.19);
    border: 1px solid #a0a0a0;
    background-color: #2b4054;
    font-family: 'MuseoSansCyrl-500',sans-serif;
    color: #d3d3d3;
    margin-right: 34px;
    box-sizing: content-box" maxlength="255" name="sform[email]" type="email" required="required" placeholder="Ваш email-адрес:">


				<button id="buttonc6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf" style="font-size: 18px;
    width: 250px;
    height: 45px;
    margin-left: 0;
    background: linear-gradient(to bottom,#fb7e65 0%,#f46043 100%);
    box-shadow: -3px 4px 5px 0 rgba(0,0,0,.45);
    border: 0;
    border-radius: 25px;
    outline: none;
    text-shadow: 1px 1px 5px rgba(0,0,0,.95);
    color: #fff;
    text-transform: uppercase;
    font-family: MuseoSansCyrl-500,sans-serif;
    cursor: pointer;
    box-sizing: content-box;
}" onclick="sformc6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf._button(this);">Подписаться</button>


			
			<script data-pagespeed-no-defer>(function(){var g=this;function h(b,d){var a=b.split("."),c=g;a[0]in c||!c.execScript||c.execScript("var "+a[0]);for(var e;a.length&&(e=a.shift());)a.length||void 0===d?c[e]?c=c[e]:c=c[e]={}:c[e]=d};function l(b){var d=b.length;if(0<d){for(var a=Array(d),c=0;c<d;c++)a[c]=b[c];return a}return[]};function m(b){var d=window;if(d.addEventListener)d.addEventListener("load",b,!1);else if(d.attachEvent)d.attachEvent("onload",b);else{var a=d.onload;d.onload=function(){b.call(this);a&&a.call(this)}}};var n;function p(b,d,a,c,e){this.h=b;this.j=d;this.l=a;this.f=e;this.g={height:window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight,width:window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth};this.i=c;this.b={};this.a=[];this.c={}}function q(b,d){var a,c,e=d.getAttribute("data-pagespeed-url-hash");if(a=e&&!(e in b.c))if(0>=d.offsetWidth&&0>=d.offsetHeight)a=!1;else{c=d.getBoundingClientRect();var f=document.body;a=c.top+("pageYOffset"in window?window.pageYOffset:(document.documentElement||f.parentNode||f).scrollTop);c=c.left+("pageXOffset"in window?window.pageXOffset:(document.documentElement||f.parentNode||f).scrollLeft);f=a.toString()+","+c;b.b.hasOwnProperty(f)?a=!1:(b.b[f]=!0,a=a<=b.g.height&&c<=b.g.width)}a&&(b.a.push(e),b.c[e]=!0)}p.prototype.checkImageForCriticality=function(b){b.getBoundingClientRect&&q(this,b)};h("pagespeed.CriticalImages.checkImageForCriticality",function(b){n.checkImageForCriticality(b)});h("pagespeed.CriticalImages.checkCriticalImages",function(){r(n)});function r(b){b.b={};for(var d=["IMG","INPUT"],a=[],c=0;c<d.length;++c)a=a.concat(l(document.getElementsByTagName(d[c])));if(0!=a.length&&a[0].getBoundingClientRect){for(c=0;d=a[c];++c)q(b,d);a="oh="+b.l;b.f&&(a+="&n="+b.f);if(d=0!=b.a.length)for(a+="&ci="+encodeURIComponent(b.a[0]),c=1;c<b.a.length;++c){var e=","+encodeURIComponent(b.a[c]);131072>=a.length+e.length&&(a+=e)}b.i&&(e="&rd="+encodeURIComponent(JSON.stringify(t())),131072>=a.length+e.length&&(a+=e),d=!0);u=a;if(d){c=b.h;b=b.j;var f;if(window.XMLHttpRequest)f=new XMLHttpRequest;else if(window.ActiveXObject)try{f=new ActiveXObject("Msxml2.XMLHTTP")}catch(k){try{f=new ActiveXObject("Microsoft.XMLHTTP")}catch(v){}}f&&(f.open("POST",c+(-1==c.indexOf("?")?"?":"&")+"url="+encodeURIComponent(b)),f.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),f.send(a))}}}function t(){var b={},d=document.getElementsByTagName("IMG");if(0==d.length)return{};var a=d[0];if(!("naturalWidth"in a&&"naturalHeight"in a))return{};for(var c=0;a=d[c];++c){var e=a.getAttribute("data-pagespeed-url-hash");e&&(!(e in b)&&0<a.width&&0<a.height&&0<a.naturalWidth&&0<a.naturalHeight||e in b&&a.width>=b[e].o&&a.height>=b[e].m)&&(b[e]={rw:a.width,rh:a.height,ow:a.naturalWidth,oh:a.naturalHeight})}return b}var u="";h("pagespeed.CriticalImages.getBeaconData",function(){return u});h("pagespeed.CriticalImages.Run",function(b,d,a,c,e,f){var k=new p(b,d,a,e,f);n=k;c&&m(function(){window.setTimeout(function(){r(k)},0)})});})();pagespeed.CriticalImages.Run('/ngx_pagespeed_beacon','http://www.norca.ru/dmitry-norca/about/','39ChalpEno',true,false,'kS5eLotaXV8');</script>
			

	<div id="_plain_messagec6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf" style="display:none;"></div>
</div>
<script type="text/javascript" src="https://fast.sendpulse.com/members/forms/user-form-js/ac/c6c2fc2ebcdd86c9727f6f9c0d198d2e77020fa424a820c980d1a532b77c42cf/c/1/"></script><script type="text/javascript">var sform_lang='ru'</script>

	<h1 class="main-title">Тренинг продаж №1!  Тренинг по продажам! Бизнес тренинги в Москве!</h1>
	

	
</div>
	

<!--<div class="main-text-class">
<p>Тренинг по улучшению продаж для условий «новой реальности»</p>
<p>До того, как  интернет-торговля вошла в повседневную жизнь, доступ к информации был  сконцентрирован в руках продавцов. С изменением ситуации происходит переход к  новому типу покупателей: подготовленных, стойких к традиционным способам  продвижения товаров. К тому же, продукция различных марок все больше сближается  по своим характеристикам, и главный критерий, интересующих потребителей ‒ цена.</p>
<p>  Чтобы влиться в  условия этой реальности, разработан двухдневный тренинг продаж. Он сможет дать множество полезных  навыков и тем, кто только начал карьеру, и тем, кто уже давно специализируется по  этим направлениям. Курс «Продажи без ограничений в условиях новой реальности» учитывает  особенности и конкретику работы заказчиков (компаний), включает в структуру блоки  (теорию) и тренинги продаж,  максимально близкие к сбытовым реалиям заказчика и его потребностям. </p>
<p>Уникальность тренинга и содержание его программы</p>
<p>Курс  сбалансировано соединяет бизнес-тренинги  продаж и личностно-ориентированного роста. Кроме получения навыков и  инструментария для работы, участники настраиваются на активное использование их  в деле. Тренинг по  продажам включает блоки:</p>
<ul>
  <li>Вопросы конкуренции в  «новых реалиях» ‒  причины, по которым  старые методы не работают.</li>
  <li>Факторы и инструментарий  самосовершенствования: что нужно для неограниченных продаж. Удвойте результаты  за 21 день.</li>
  <li>Диалог с покупателями:  открыть отношения, профессионально слушать, узнать подлинные мотивы  собеседников, заразить из своей идеей. Моделирование «Взрослого диалога», его  отрабатывание. Обсуждение цены ‒ почему его не стоит опасаться.</li>
  <li>Сбор информации:  опросные системы («Мост А-Я»), проблематика достоверности данных.</li>
  <li>Возражения и отказ ‒ превентивные  меры, обработка, работа по окончательным отказам.</li>
</ul>
<p>Кроме этого, в  рамках тренинга рассматриваются вопросы продаж без ограничений и принцип «пожизненного»  подхода. </p>
</div>--->


</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>