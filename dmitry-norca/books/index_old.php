<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Лучшие книги по продажам. Книги по технике продаж и переговорам для менеджеров и руководителей отдела продаж, для продажников.");
$APPLICATION->SetPageProperty("keywords", "Книги по продажам, по технике продаж, переговорам, для менеджеров, для руководителей отдела продаж, лучшие книги о продажах для продажников");
$APPLICATION->SetPageProperty("title", "Лучшие книги по продажам от Дмитрия Норки! Книги для продажников!"); 
$APPLICATION->SetTitle("Лучшие книги по продажам от Дмитрия Норки! Книги для продажников!");
?>	
 
 <div class="content  no_padding pad_right">

		<?$APPLICATION->IncludeComponent("bitrix:news.list","books",Array(
	        "DISPLAY_DATE" => "N",
	        "DISPLAY_NAME" => "Y",
	        "DISPLAY_PICTURE" => "Y",
	        "DISPLAY_PREVIEW_TEXT" => "Y",
	        "AJAX_MODE" => "N",
	        "IBLOCK_TYPE" => "norca",
	        "IBLOCK_ID" => "14",
	        "NEWS_COUNT" => "20",
	        "SORT_BY1" => "ACTIVE_FROM",
	        "SORT_ORDER1" => "DESC",
	        "SORT_BY2" => "SORT",
	        "SORT_ORDER2" => "ASC",
	        "FILTER_NAME" => "",
	        "FIELD_CODE" => Array( ),
	        "PROPERTY_CODE" => Array("DESCRIPTION"),
	        "CHECK_DATES" => "Y",
	        "DETAIL_URL" => "",
	        "PREVIEW_TRUNCATE_LEN" => "",
	        "ACTIVE_DATE_FORMAT" => "d.m.Y",
	        "SET_TITLE" => "N",
	        "SET_BROWSER_TITLE" => "N",
	        "SET_META_KEYWORDS" => "Y",
	        "SET_META_DESCRIPTION" => "Y",
	        "SET_STATUS_404" => "Y",
	        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	        "ADD_SECTIONS_CHAIN" => "N",
	        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
	        "PARENT_SECTION" => "",
	        "PARENT_SECTION_CODE" => "",
	        "INCLUDE_SUBSECTIONS" => "Y",
	        "CACHE_TYPE" => "N",
	        "CACHE_TIME" => "3600",
	        "CACHE_FILTER" => "Y",
	        "CACHE_GROUPS" => "Y",
	        "DISPLAY_TOP_PAGER" => "N",
	        "DISPLAY_BOTTOM_PAGER" => "N",
	        "PAGER_TITLE" => "Новости",
	        "PAGER_SHOW_ALWAYS" => "Y",
	        "PAGER_TEMPLATE" => "",
	        "PAGER_DESC_NUMBERING" => "N",
	        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	        "PAGER_SHOW_ALL" => "Y",
	        "AJAX_OPTION_JUMP" => "N",
	        "AJAX_OPTION_STYLE" => "Y",
	        "AJAX_OPTION_HISTORY" => "N",
	        "AJAX_OPTION_ADDITIONAL" => ""
	    )
	);?>
	
<div class="right_col_pr">
        

        <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( 

      "VKID" => "",
      "FACEBOOKID" => ""
     ), array()); ?>

   
	</div>
</div>






<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
