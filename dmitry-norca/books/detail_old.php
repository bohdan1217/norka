<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Лучшая книга для руководителей отдела продаж \"Управление отделом продаж, стратегии и тактики успеха\". Принципы, технологии, тактики успеха...");
$APPLICATION->SetPageProperty("keywords", "Лучшая книга для руководителей отдела продаж, управление продажами, управление отделом продаж, книга,");
$APPLICATION->SetPageProperty("title", "Книга для руководителей отдела продаж. Книга управление продажами"); 
$APPLICATION->SetTitle("Книга для руководителей \"Управление отделом...Стратегии..\"");
?>


    <div class="content no_padding pad_right">
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"book",
	Array(
		"COMPONENT_TEMPLATE" => "book",
		"IBLOCK_TYPE" => "norca",
		"IBLOCK_ID" => "14",
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => $_REQUEST["CODE"],
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array("DETAIL_TEXT", "PREVIEW_PICTURE", "NAME"),
		"PROPERTY_CODE" => array("", ""),
		"IBLOCK_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_CANONICAL_URL" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_SHOW_ALL" => "N"
	)
);?>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( 

      "VKID" => "",
      "FACEBOOKID" => ""
     ), array()); ?>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>