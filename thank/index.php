<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Спасибо");
?>

<?
$back = $_SERVER['HTTP_REFERER']; // для справки, не обязательно создавать переменную
?>

    <section class="thankyou">
        <div class="container h-100">
            <div class="d-flex flex-column justify-content-center h-100">
                <ul class="row">
                    <li class="col-md-7 offset-md-5 offset-sm-0 blockthank">
                        <div class="thankyou-content d-flex flex-column h-100 justify-content-md-center justify-content-sm-start align-items-start">
                            <h4 class="thankyou-title">Спасибо!</h4>
                            <span class="thankyou-text d-block">Мы свяжимся с Вами в ближайшее время</span>
                            <div class="schedule-nb">
                                <a href="<?=$back?>">
                                    <button class="btn-events">ВЕРНУТЬСЯ НА САЙТ</button>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>