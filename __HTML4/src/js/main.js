// $( document.body ).click(function () {
//
// });

$(document).ready(function () {

    $('.responsive').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.btn-main-play, .video').on('click', function () {

        var slideItem = $(this).closest('.swiper-slide');
        var videoElement = slideItem.find('video')[0];

        if( slideItem.hasClass('play-video') ){
            slideItem.removeClass('play-video');
            $('.swiper-pagination, .main-statistic').removeClass('hide-for-play');
            videoElement.pause();
        } else{
            slideItem.addClass('play-video');
            $('.swiper-pagination, .main-statistic').addClass('hide-for-play');
            videoElement.play();
        }

    });

});

