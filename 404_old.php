<?
//header( 'Location: http://www.norca.ru', true, 301 );
//exit;


include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404_","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
?>
<div class="content no_padding">
        <div class="page404">
            <h2>Страница не найдена</h2>
            <h3>404</h3>
            <h4>К сожалению, такой страницы не существует. Вероятно, она была удалена автором с сервера,<br>либо её здесь никогда не было. Попробуйте перейти на главную страницу или воспользоваться меню.</h4>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>