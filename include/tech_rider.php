<p>
 <b>ТЕХНИЧЕСКИЙ РАЙДЕР</b>
</p>
<p>
 <u>Размер группы</u>
</p>
<p>
	 Корпоративный тренинг предполагает от 10 до 25 участников. Если необходимо присутствие большего количества людей, то это обговаривается отдельно.
</p>
<p>
 <u>Зал</u>
</p>
<p>
	 Зал должен соответствовать количеству участников. В зале не должны присутствовать лишние предметы и ненужная мебель. Должна быть возможность переставлять стулья полукругом и моделировать рабочее пространство. Окна обязательны. Высокие потолки - желательны. В зале должны быть обязательная вентиляция и система кондиционирования воздуха или возможность проветривания. В месте проведения тренинга не должно быть посторонних запахов, звуков (строительные работы, передвижения людей, телефонные звонки). На время проведения тренинга обязательно наличие маркеров, флипчата или маркерной доски. Проекционное оборудование и компьютер предоставляются по требованию и согласовываются заранее.
</p>
<p>
 <u>Раздаточный материал</u>
</p>
<p>
	 Раздаточный материал разрабатывается бизнес-тренером заблаговременно и высылается организаторам обучения за пять дней до начала тренинга по электронной почте в формате PDF. Организаторы должны распечатать полученный файл на принтере и сброшюровать. Количество рабочих тетрадей должно соответствовать количеству участников тренинга плюс один экземпляр для ведущего. Также организаторы должны обеспечить участников тренинга ручками. Наличие у участников персональных беджей с именами желательно, но не является обязательным условием.
</p>
<p>
 <u>Тайминг тренинга</u>
</p>
<p>
	 &nbsp; 9.20 - 10.00&nbsp;&nbsp; сбор участников
</p>
<p>
	 10.00 - 11.15&nbsp;&nbsp; первый блок тренинга
</p>
<p>
	 11.15 - 11.30&nbsp;&nbsp; кофе-пауза
</p>
<p>
	 11.30 - 13.00&nbsp;&nbsp; второй блок тренинга
</p>
<p>
	 13.00 - 14.00&nbsp;&nbsp; обед
</p>
<p>
	 14.00 - 15.15&nbsp;&nbsp; третий блок тренинга
</p>
<p>
	 15.15 - 15.30&nbsp;&nbsp; кофе-пауза
</p>
<p>
	 15.30 - 17.00&nbsp;&nbsp; четвёртый блок тренинга
</p>
<p>
	 &nbsp;
</p>
<p>
 <b>БЫТОВОЙ РАЙДЕР</b>
</p>
<p>
 <u>Во время проведения тренинга</u>
</p>
<p>
	 Необходимо обеспечение бизнес-тренера питьевой водой в бутылках. Объём - 1.5-2 литра в день.
</p>
<p>
 <u>Проживание</u>
</p>
<p>
	 Исключаются съёмные квартиры. Только гостиница. Если гостиница - четыре звезды, то номер должен быть «стандарт». Если гостиница - три звезды, то номер - «люкс». Категория - для некурящих. В номере должна быть минеральная вода в объёме не менее двух литров. Наличие wi-fi - обязательно. В стоимость проживания должны быть включены завтрак и ужин. Требования к кухне: Не обработанные овощи и фрукты. Минеральная вода в стекле, предпочтение - Боржоми, Perrier.
</p>
<p>
 <b>&nbsp;</b>
</p>
<p>
 <b>ТРАНСПОРТ</b>
</p>
<p>
	 Если предполагается проведение тренинга за пределами Москвы, то переезд бизнес-тренера по маршруту Москва - место проведения тренинга – Москва оплачивается принимающей стороной. Стоимость проезда включается в договор и оплачивается вместе с гонораром. Билеты на поезд или самолет бизнес-тренер приобретает сам. Если предполагается проезд на поезде, то категория вагона - СВ. Если предполагается перелет на самолете и срок перелета составляет не более двух часов, то класс перелета - «Эконом», если более - «Бизнес». В городе проведения тренинга принимающая сторона должна обеспечить трансфер аэропорт - гостиница – аэропорт, а также гостиница – место проведения тренинга – гостиница.
</p>
<p>
 <br>
</p>
<p>
 <b>ФИНАНСЫ</b>
</p>
<p>
	 Оплата производится безналичным путем. Организаторам предоставляется полный пакет подтверждающих документов. 
Бронирование дат проведения и окончательная фиксация за организатором производится после внесения предоплаты в размере 10% общей стоимости. Полная оплата организатором производится не позднее, чем за пять дней до начала мероприятия.
Дополнительная информация: mail@norca.ru +7(495) 776-82-34

</p>
<p>
 <b><br>
 </b>
</p>
<p>
 <b>ДОПОЛНИТЕЛЬНО</b>
</p>
<p>
</p>
<p>
	 Всем участникам тренинга выдаются сертификаты. Для этого организаторы должны за семь дней до начала тренинга выслать фамилии, имена и отчества участников.&nbsp;
</p>
<p>
	<br>
</p>