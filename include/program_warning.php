<p>
 <span style="font-family: Verdana; font-size: 10pt;">Внимание! </span>
</p>
<span style="font-family: Verdana; font-size: 10pt;"> </span>
<p style="text-align: justify;">
	<span style="font-family: Verdana; font-size: 10pt;">
	Данная программа тренинга продаж не является окончательной. </span>
</p>
<span style="font-family: Verdana; font-size: 10pt;"> </span>
<p style="text-align: justify;">
	<span style="font-family: Verdana; font-size: 10pt;"> </span>
</p>
<span style="font-family: Verdana; font-size: 10pt;"> </span>
<p style="text-align: justify;">
	<span style="font-family: Verdana; font-size: 10pt;">
	Все тренинги разрабатываются под реальности конкретного заказчика и ориентированы на конкретную компанию, ассортимент товара или услуг, который она продаёт, и могут включать в себя блоки темы из разных программ. При&nbsp;разработке программы тренинга&nbsp;учитывается менталитет потенциальных покупателей и экономическая среда рынка сбыта компании-заказчика. </span>
</p>
<span style="font-family: Verdana; font-size: 10pt;"> </span>
<p style="text-align: justify;">
	<span style="font-family: Verdana; font-size: 10pt;"> </span>
</p>
<span style="font-family: Verdana; font-size: 10pt;"> </span>
<p style="text-align: justify;">
	<span style="font-family: Verdana; font-size: 10pt;">
	В программе тренинга продаж используются примеры и тренировочные модели с конкретным товаром или услугой и с конкретными коммерческими ситуациями компании-заказчика. Прежде чем начинать тренинг продаж, проводится оценка и анализ потребностей компании-клиента в обучении и тестирование сотрудников до обучения и после. Затем на основе полученной информации с учётом специфики деятельности, товарного ассортимента и рынка сбыта разрабатывается программа тренинга продаж, полностью адаптированная под нужды и задачи компании клиента.</span>
</p>