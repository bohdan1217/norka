<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>

<section class="section-welcome section-welcome-otzivi">
    <div class="container">
        <h3 class="section-welcome-title-otzivi">Отзывы</h3>
        <span class="section-welcome-text-otzivi">Услуги бизнес-тренера по продажам</span>
        <a href="form">
            <button class="btn-otzivi btn-otzivi-heigth add-review">оставить отзывы</button>
        </a>
    </div>
</section>



<section class="section-content section-bg-grey">
        <div class="container">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
                "START_FROM" => "0",  // Номер пункта, начиная с которого будет построена навигационная цепочка
                "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                "SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
            ),
                false
            );?>


            <h2 class="section-title title-line text-center section-indents-smd">Отзывы</h2>

            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "reviews",
                array(
                    "COMPONENT_TEMPLATE" => "reviews",
                    "IBLOCK_TYPE" => "services",
                    "IBLOCK_ID" => "37",
                    "NEWS_COUNT" => "6",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "ID",
                        1 => "CODE",
                        2 => "XML_ID",
                        3 => "NAME",
                        4 => "TAGS",
                        5 => "SORT",
                        6 => "PREVIEW_TEXT",
                        7 => "PREVIEW_PICTURE",
                        8 => "DETAIL_TEXT",
                        9 => "DETAIL_PICTURE",
                        10 => "DATE_ACTIVE_FROM",
                        11 => "ACTIVE_FROM",
                        12 => "DATE_ACTIVE_TO",
                        13 => "ACTIVE_TO",
                        14 => "SHOW_COUNTER",
                        15 => "SHOW_COUNTER_START",
                        16 => "IBLOCK_TYPE_ID",
                        17 => "IBLOCK_ID",
                        18 => "IBLOCK_CODE",
                        19 => "IBLOCK_NAME",
                        20 => "IBLOCK_EXTERNAL_ID",
                        21 => "DATE_CREATE",
                        22 => "CREATED_BY",
                        23 => "CREATED_USER_NAME",
                        24 => "TIMESTAMP_X",
                        25 => "MODIFIED_BY",
                        26 => "USER_NAME",
                        27 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "EMAIL",
                        1 => "CITY",
                        2 => "NAME",
                        3 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "0",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y"
                ),
                false
            );?>

            <?
            CModule::IncludeModule('iblock');
            $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CITY", "PROPERTY_VIDEO", "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", 'PROPERTY_NAME', 'PREVIEW_TEXT'); //"ID", "NAME", "PREVIEW_TEXT"
            $arFilter = Array("IBLOCK_ID"=>37, "ACTIVE"=>"Y", 'SECTION_ID' => 1);
            $res = CIBlockElement::GetList(Array('data_adtive_from'=>'desc'), $arFilter, false, Array("nPageSize"=>12), $arSelect);
            $total = $res -> SelectedRowsCount();
            if($total){
                ?>
            <h2 class="section-title title-line text-center section-indents-mdl">Видео отзывы</h2>
            <ul class="row comments-video">
                <?
                $key = 0;
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    $video_src = CFile::GetPath($arFields['PROPERTY_VIDEO_VALUE']);
                    $key++;
                    ?>
                    <li class="col-md-6" id="video_<?=$arFields['PROPERTY_VIDEO_VALUE']?>">
                        <div class="comments-video-wrap">
                            <video id="my-video_<?=$key?>" class="video-js" controls preload="auto" width="540" height="344"
                                   poster="" data-setup="{}">
                                <source src="<?=$video_src?>" type='video/mp4'>
                                <source src="MY_VIDEO.webm" type='video/webm'>
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                </p>
                            </video>
                        </div>
                        <div class="comments-info">
                            <a href="#text_<?=$arFields['PROPERTY_VIDEO_VALUE']?>" class="">
                                <h4 class="comments-info-author"><?=$arFields['PROPERTY_NAME_VALUE']?></h4>
                                <?if ($arFields['PROPERTY_CITY_VALUE']) {?>
                                    <span class="comments-info-city">г. <?=$arFields['PROPERTY_CITY_VALUE']?></span>
                                <?}?>
                            </a>
                        </div>
                    </li>
                <?}?>
            </ul>
            <?}?>


            <div id="form" class="margin-top">
                <h4 class="newss-post-title section-indents-am5">Добавить отзыв</h4>
                <form action="<?=SITE_TEMPLATE_PATH?>/ajax/reviews.php" method="post" class="form-comments reviews_form" enctype="multipart/form-data">
                    <ul class="row">
                        <?$city = get_city_by_ip($_SERVER["REMOTE_ADDR"]);?>
                        <input type="hidden" name="city" value="<?=$city?>">
                        <li class="col-md-6 form-comments-name-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/name.png">
                            <input type="text" placeholder="Ваше имя" name="name" class="form-name-label form-comments-name d-block" required/>
                            <label id="name-error" class="count_name margin_top color_red_count"></label>
                        </li>


                        <li class="col-md-6 form-comments-email-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/mail.png">
                            <input type="email" placeholder="Ваш Е-mail" name="email" class="form-name-label form-comments-email d-block" required>
                            <label id="email-error" class="count_email margin_top color_red_count"></label>
                        </li>

                        <li class="col-md-12 form-comments-comment-wrap-blog">
                            <img src="<?=SITE_TEMPLATE_PATH?>/src/img/pen.png">
                            <textarea placeholder="Напишите отзыв" name="comment" class="form-comments-comment d-block review-text-comments" rows="5"></textarea>
                        </li>

                        <li class="col-md-6 form-comments-email-wrap-blog">
                            <div class="video-li">
                                <img src="<?=SITE_TEMPLATE_PATH?>/src/img/video-camera.png">
                                <label id="val" for="fileUpload">Загрузить видео</label>
                                <input type="file" placeholder="Видео" id="fileUpload" name="file" class="form-comments-email d-block video-input" accept="video/mp4" data-max-size="125829120">
                            </div>
                        </li>

                        <li class="counter_comments">Осталось символов: <span class="counter_number_comments">500</span></li>

                        <li class="col-md-12 justify-content-sm-center">
                            <button class="btn-submit btn_send_review">Отправить</button>
                        </li>
                    </ul>
                </form>
            </div>


        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>