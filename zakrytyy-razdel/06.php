<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Пятый урок");
$APPLICATION->SetTitle("06");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Пятый урок:</b>
		</p>
		<p>
 <b><br>
 </b>
		</p>
		<p>
			 - Как использовать восемь советов для проведения эффективной презентации<br>
			 - Как работает Алгоритм ОВИР ©<br>
			 - Как использовать технику СВ<br>
			 - Как пользоваться алгоритмом ТАНКИ ©
		</p>
		<p>
 <br>
		</p>
		 Продолжительность урока 12 минут.&nbsp;<br>
 <br>
		 <iframe width="640" height="360" src="https://www.youtube.com/embed/ImF69gvoa6g" frameborder="0" allowfullscreen="">
		</iframe> <br>
 <span style="border: 0px; color: #38434e; background-color: #ffffff;"><br>
		Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email:</span><a href="mailto:trener@norca.ru" style="border: 0px; background-color: #ffffff;">trener@norca.ru</a><span style="border: 0px; color: #38434e; background-color: #ffffff;">&nbsp;</span><br>
 <br>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>