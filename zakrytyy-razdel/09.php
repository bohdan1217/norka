<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Восьмой урок");
$APPLICATION->SetTitle("09");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Восьмой урок:</b>
		</p>
		<p>
 <u><br>
 </u>
		</p>
		<p>
			 - Как воспринимать возражения<br>
			 - Какие существуют типы возражений<br>
			 - Как использовать техники выявления ложного и истинного возражения<br>
			 - Как реагировать на возражения<br>
			 - Как использовать пяти шаговаю технику присоединения к возражению ИСИДА ©<br>
			 - Как работает метод ДДП ©
		</p>
		<br>
		Продолжительность урока 18 минут.<br>
 <br>
		 <iframe width="640" height="360" src="https://www.youtube.com/embed/f3ntii_NGY0" frameborder="0" allowfullscreen="">
		</iframe> <br>
 <br>
 <span style="border: 0px; color: #38434e; background-color: #ffffff;">Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email:</span><a href="mailto:trener@norca.ru" style="border: 0px; background-color: #ffffff;">trener@norca.ru</a><br>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>