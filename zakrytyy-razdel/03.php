<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Второй урок");
$APPLICATION->SetTitle("03");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Второй урок:</b>
		</p>
		<p>
			<u><br>
			</u>
		</p>
		<p>
			 - Как подготовиться к встрече<br>
			 - Как вступать в первичный контакт<br>
			 - Как использовать методику ПИКАП ©<br>
			 - Как поддерживать долгосрочные отношения с людьми
		</p>
 <br>
		 &nbsp; &nbsp; Продолжительность урока 19 минут.<br>
 <br>
		 <iframe width="640" height="360" src="https://www.youtube.com/embed/5bzv9FkGIZk" frameborder="0" allowfullscreen="">
		</iframe> <br>
 <br>
 <span style="color: #38434e; background-color: #ffffff;">Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email:</span><a href="mailto:trener@norca.ru" style="border: 0px; background-color: #ffffff;">trener@norca.ru</a><span style="color: #38434e; background-color: #ffffff;">&nbsp;</span><br>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>