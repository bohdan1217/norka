<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Третий урок");
$APPLICATION->SetTitle("04");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Третий урок:</b>
		</p>
		<p>
 <u><br>
 </u>
		</p>
		<p>
			 - Что такое техника вопросов<br>
			 - Какие основные преимущества метода вопросов<br>
			 - Как использовать правила постановки вопросов<br>
			 - Как использовать водные фразы<br>
			 - Какие существуют типы вопросов<br>
			 - Как использовать опросную систему ОПОРА ©
		</p>
 <br>
		 Продолжительность урока 20 минут<br>
 <br>
		 <iframe width="640" height="360" src="https://www.youtube.com/embed/mbwQLvDjVDQ" frameborder="0" allowfullscreen="">
		</iframe> <br>
 <br>
 <span style="border: 0px; color: #38434e; background-color: #ffffff;">Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email:</span><a href="mailto:trener@norca.ru" style="border: 0px; background-color: #ffffff;">trener@norca.ru</a><span style="border: 0px; color: #38434e; background-color: #ffffff;">&nbsp;</span><br>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>