<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Первый урок");
$APPLICATION->SetTitle("02");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Первый урок:</b>
		</p>
		<p>
			<b><br>
			</b>
		</p>
		<p>
			 - Как разрабатывать стратегии успеха<br>
			 - Какие есть факторы успеха<br>
			 - Что такое философия Kaizen<br>
			 - Как использовать инструменты самосовершенствования<br>
			 - Как готовиться к контакту с клиентом<br>
			 - Как использовать методику подготовки к встрече, алгоритм СПАС ©
		</p>
		<p>
 <br>
		</p>
		<p>
			 Продолжительность урока 14 минут.&nbsp;
		</p>
		<p>
 <br>
		</p>
		<p>
			 <iframe width="640" height="360" src="https://www.youtube.com/embed/W_8RBVfj2e4" frameborder="0" allowfullscreen="">
			</iframe> <br>
		</p>
		<p>
 <br>
		</p>
		<p>
			 Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email: <a href="mailto:trener@norca.ru">trener@norca.ru</a>&nbsp;<br>
		</p>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>