<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Четвертый урок");
$APPLICATION->SetTitle("05");
?><div class="content no_padding_ex pad_right">
	<div class="tips_i">
		<p>
 <b>Четвертый урок:</b>
		</p>
		<p>
 <u><br>
 </u>
		</p>
		<p>
			 - Что даёт профессиональное слушание<br>
			 - Какие есть типы слушателей<br>
			 - Какие существуют виды слушания<br>
			 - Что такое профессиональное слушание<br>
			 - Что такое правила профессионального слушательского поведения<br>
			 - Как использовать на практике стили профессионального слушания
		</p>
		<p>
			<br>
		</p>
		<p>
			 Продолжительность урока 26 минут.&nbsp;
		</p>
		<p>
 <br>
		</p>
		<p>
			 <iframe width="640" height="360" src="https://www.youtube.com/embed/nnWP9EvI6SQ" frameborder="0" allowfullscreen="">
			</iframe> <br>
		</p>
		<p>
			<span style="color: #38434e; background-color: #ffffff;"><br>
			</span>
		</p>
		<p>
 <span style="color: #38434e; background-color: #ffffff;">Если после просмотра урока у вас будут возникать вопросы, или появятся мысли, которыми вы захотите со мной поделиться, то смело можете писать мне на email:</span><a href="mailto:trener@norca.ru" style="border: 0px; background-color: #ffffff;">trener@norca.ru</a><br>
		</p>
	</div>
	<div class="right_col_pr">
		 <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/right_col.php', array( ), array()); ?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>