<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "бизнес тренер, тренинги по продажам, бесплатные тренинги по продажам, трненинги, советы по продажам,");
$APPLICATION->SetPageProperty("keywords", "бизнес тренер по продажам, Москва, Россия, тренер продаж, бизнес консультант, обучение, тренинги по продажам, тренинги, советы по продажам, заказать обучение у бизнес тренера");
$APPLICATION->SetPageProperty("description", "Лучший бизнес тренер по продажам - бизнес-консультант Дмитрий Норка. Бизнес-тренинги по продажам в Москве, корпоративные и открытые тренинги. Обучение экспертным продажам");
$APPLICATION->SetPageProperty("title", "Бизнес тренер по продажам Дмитрий Норка - Москва");
$APPLICATION->SetTitle("Тренер по продажам в Москве - Дмитрий Норка – обучение продажам в Москве и по всей России");
?>


<section class="section-welcome-about">
    <div class="container">
        <ul class="row">
            <li class="col-md-9 col-lg-7">
                <h3 class="section-welcome-title-about">Дмитрий Норка</h3>
                <span class="section-welcome-title-about-after">Бизнес-тренер по продажам</span>
                <p class="section-welcome-text-about">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => SITE_TEMPLATE_PATH."/includes/about_author.php"
                        )
                    );?>
                </p>
                <div class="d-flex flex-wrap justify-content-md-start justify-content-sm-center align-items-center section-indents-mdl">
                    <button class="btn-welcome" type="button" data-toggle="modal" data-target="#training">заказать тренинг</button>
                    <button class="btn-welcome" type="button" data-toggle="modal" data-target="#question">Задать вопрос автору</button>
                </div>
            </li>
        </ul>
    </div>
</section>


<section class="section-content section-bg-grey">
    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
            "START_FROM" => "0",  // Номер пункта, начиная с которого будет построена навигационная цепочка
            "PATH" => "", // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
            "SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
        ),
            false
        );?>
        <div class="video-about video-big-about">
            <video id="my-video" class="video-js vjs-default-skin vjs-16-9" controls preload="auto" width="1110" height="587"
                   poster="<?=SITE_TEMPLATE_PATH?>/src/img/about/wrapper.png" data-setup="{}">
                <source src="<?=SITE_DIR?>upload/norca.mp4" type='video/mp4'>
                <source src="MY_VIDEO.webm" type='video/webm'>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
            </video>
        </div>
    </div>
</section>




<section class="about-section-number">
    <div class="container">
        <h2 class="section-title-about section-title section-title-white title-line text-center section-indents-mdz">Мои цифры</h2>
        <ul>
            <?
            CModule::IncludeModule('iblock');
            $arSelect = Array("ID", "NAME", "PREVIEW_TEXT");
            $arFilter = Array("IBLOCK_ID"=>30, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                ?>
                <li class="col equalheight">
                    <div class="col-wrap">
                        <h5 class="text-center col-number-title"><span><?=$arFields["PREVIEW_TEXT"]?></span></h5>
                        <span class="d-block text-center col-number-info text-padding"><?=$arFields["NAME"]?></span>
                    </div>
                </li>
            <?}?>
        </ul>
        <div class="clearfix"></div>
    </div>
</section>

<?
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");
$arFilter = Array("IBLOCK_ID"=>39, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>8), $arSelect);
$total = $res -> SelectedRowsCount();
if($total){
?>
<section class="section-content-news section-bg-grey">
    <div class="container">
        <h2 class="section-title title-line text-center section-indents-mdz4">Мои достижения</h2>
        <ul class="row about-achievements-list section-indents-mdl8">
            <?
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                ?>
                <li class="col-md-6 col-lg-3">
                    <div>
                        <?if (!empty($arFields['PREVIEW_PICTURE'])) {?>
                            <?php $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>255, 'height' => 220), BX_RESIZE_IMAGE_EXACT , true);  ?>
                            <div class="about-achievements-wrap section-bg-white">
                                <img src="<?=$photo['src']?>" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>"/>
                            </div>
                        <?} else {?>
                            <div class="about-achievements-wrap section-bg-white">
                                <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>" style="height: 220px"/>
                            </div>
                        <?}?>
                        <p class="text-center about-achievements-title"><?=$arFields['NAME']?></p>
                    </div>
                </li>
            <?}?>
        </ul>
        <?}?>



        <ul class="row news-list">
            <?
            CModule::IncludeModule('iblock');
            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", 'DETAIL_PAGE_URL', 'DATE_ACTIVE_FROM');
            $arFilter = Array("IBLOCK_ID"=>38,"ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array('date_active_from'=>'desc'), $arFilter, false, Array("nPageSize"=>4), $arSelect);
            $total = $res -> SelectedRowsCount();
            if($total){
                ?>
                <li class="col-lg-6 col-md-12">
                    <div class="section-bg-white news-list-wrap">
                        <h3 class="news-list-title">Будущие события</h3>
                        <ul>
                            <?
                            while($ob = $res->GetNextElement())
                            {
                                $arFields = $ob->GetFields();
                                ?>
                                <li class="news-post">
                                    <div class="d-flex">
                                        <?if (!empty($arFields['PREVIEW_PICTURE'])) {?>
                                            <?php $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>110, 'height' => 83), BX_RESIZE_IMAGE_EXACT , true);  ?>
                                            <div class="news-img-wrap">
                                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                    <img src="<?=$photo['src']?>" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>"  class="news-img"/>
                                                </a>
                                            </div>
                                        <?} else {?>
                                            <div class="news-img-wrap">
                                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>" class="news-img" style="height: 83px"/>
                                                </a>
                                            </div>
                                        <?}?>
                                        <div class="news-info">
                                            <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                <h4 class="news-info-title"><?=$arFields["NAME"]?></h4>
                                            </a>
                                            <span class="d-block news-info-date"><?=date("d.m.Y", strtotime($arFields["DATE_ACTIVE_FROM"]));?></span>
                                        </div>
                                    </div>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                </li>
            <?}?>



            <?
            CModule::IncludeModule('iblock');
            $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", 'DETAIL_PAGE_URL', 'DATE_ACTIVE_FROM');
            $arFilter = Array("IBLOCK_ID"=>25, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array('date_active_from'=>'desc'), $arFilter, false, Array("nPageSize"=>4), $arSelect);
            $total = $res -> SelectedRowsCount();
            if($total){
                ?>
                <li class="col-lg-6 col-md-12">
                    <div class="section-bg-white news-list-wrap">
                        <h3 class="news-list-title">Последнее из блога</h3>
                        <ul>
                            <?
                            while($ob = $res->GetNextElement())
                            {
                                $arFields = $ob->GetFields();
                                ?>
                                <li class="news-post">
                                    <div class="d-flex">
                                        <?if (!empty($arFields['PREVIEW_PICTURE'])) {?>
                                            <?php $photo = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>110, 'height' => 83), BX_RESIZE_IMAGE_EXACT , true);  ?>
                                            <div class="news-img-wrap">
                                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                    <img src="<?=$photo['src']?>" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>"  class="news-img"/>
                                                </a>
                                            </div>
                                        <?} else {?>
                                            <div class="news-img-wrap">
                                                <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/src/img/no-photo.jpg" alt="<?=$arFields["NAME"]?>" title="<?=$arFields["NAME"]?>" class="news-img" style="height: 83px"/>
                                                </a>
                                            </div>
                                        <?}?>
                                        <div class="news-info">
                                            <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                                <h4 class="news-info-title"><?=$arFields["NAME"]?></h4>
                                            </a>
                                            <span class="d-block news-info-date"><?=date("d.m.Y", strtotime($arFields["DATE_ACTIVE_FROM"]));?></span>
                                        </div>
                                    </div>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                </li>
            <?}?>
        </ul>
    </div>
</section>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
