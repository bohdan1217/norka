<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Not Found");
?>
<div class="background_color_404">
    <div class="">
        <div class="error-text1">
            <p class="h1-404">404</p>
        </div>
        <div class="error-text2">
            <p class="text-align-center color-white">Запрашиваемая страница не найдена</p>
        </div>
        <div class="error-text3">
            <p class="text-align-center color-white">Для перехода на предыдущую страницу жмите на кнопку</p>
        </div>
        <div class="schedule-nb text-align-center">
            <a href="/">
                <button class="btn-events">ВЕРНУТЬСЯ НА САЙТ</button>
            </a>
        </div>

    </div>
</div>
